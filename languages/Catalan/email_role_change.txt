Hola {user_name},

T'informem que el teu compte al {forum_name} ha estat modificat. 

El teu nou rol: {role_name}.

Pots revisar el canvi de rol en aquesta URL:

{forum_url}