<?php
/*
* Copyright 2003 - 2007 Mark O'Sullivan
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
* Contact Mark O'Sullivan at mark [at] lussumo [dot] com
*
* Description: Catalan language dictionary
* Author: Jaume Llorens 
* Contact: info [at] jllorens [dot] net
* Based on Alberto Tellez (alberto [at] 2mdc [dot] com) Spanish traslation: Castellano 1.0
* 
* !!!!!DO NOT ALTER THIS FILE!!!!!
* If you need to make changes to these definitions, add the new definitions to
* your conf/language.php file. Definitions made there will override these.
*/

// Defineix the xml:lang attribute for the html tag
$Context->Dictionary['XMLLang'] = 'ca_ES';

// Defineix els termes del diccionari en Catal&agrave;
$Context->Dictionary['NoDiscussionsNotSignedIn'] = 'No pots participar en les converses perqu&egrave; no est&agrave;s registrat.';
$Context->Dictionary['SelectDiscussionCategory'] = 'Selecciona una categoria per aquesta conversa.';
$Context->Dictionary['WhisperYourCommentsTo'] = 'Comenta en privat a <small>(opcional)</small>';
$Context->Dictionary['And'] = 'i';
$Context->Dictionary['Or'] = 'o';
$Context->Dictionary['ClickHereToContinueToDiscussions'] = 'Continuar a les converses';
$Context->Dictionary['ClickHereToContinueToCategories'] = 'Continuar a les categories';
$Context->Dictionary['ReviewNewApplicants'] = 'Revisar les sol&middot;licituds d\'ingr&eacute;s';
$Context->Dictionary['New'] = 'nous';
$Context->Dictionary['NewCaps'] = 'Nous';
$Context->Dictionary['Username'] = 'Usuari';
$Context->Dictionary['Password'] = 'Contrasenya';
$Context->Dictionary['RememberMe'] = 'Recorda\'m';
$Context->Dictionary['ForgotYourPassword'] = 'He oblidat la contrasenya?';
$Context->Dictionary['Proceed'] = 'Continuar';
$Context->Dictionary['ErrorTitle'] = 'S\'han detectat alguns problemes';
$Context->Dictionary['RealName'] = 'Nom Real';
$Context->Dictionary['Email'] = 'Email';
$Context->Dictionary['Style'] = 'Estil';
$Context->Dictionary['AccountCreated'] = 'Compte Creat';
$Context->Dictionary['LastActive'] = '&Uacute;ltim Acc&eacute;s';
$Context->Dictionary['VisitCount'] = 'Comptador de Visites';
$Context->Dictionary['DiscussionsStarted'] = 'Converses Comen&ccedil;ades';
$Context->Dictionary['CommentsAdded'] = 'Comentaris Afegits';
$Context->Dictionary['LastKnownIp'] = '&Uacute;ltima Ip Coneguda';
$Context->Dictionary['PermissionError'] = 'No tens permisos per aquesta tasca.';
$Context->Dictionary['ChangePersonalInfo'] = 'Informaci&oacute; Personal';
$Context->Dictionary['DefineYourAccountProfile'] = 'Defineix el teu perfil';
$Context->Dictionary['YourUsername'] = 'El teu nom d\'usuari';
$Context->Dictionary['YourUsernameNotes'] = 'El teu nom d\'usuari apareixer&agrave; al costat de les teves converses i comentaris.';
$Context->Dictionary['YourFirstNameNotes'] = 'Aquest hauria de ser el teu nom real. Nom&eacute;s ser&agrave; visible des de la p&agrave;gina del compte.';
$Context->Dictionary['YourLastNameNotes'] = 'Aquest hauria de ser el teu cognom real.  Nom&eacute;s ser&agrave; visible des de la p&agrave;gina del compte.';
$Context->Dictionary['YourEmailAddressNotes'] = ' Has de donar una adre&ccedil;a d\'e-mail v&agrave;lida per si mai has de recuperar la teva contrasenya (el formulari per recuperar la constrasenya funciona per correu elctr&ograve;nic).';
$Context->Dictionary['CheckForVisibleEmail'] = 'Fer visible la meva adre&ccedil;a d\'e-mail als altres membres';
$Context->Dictionary['AccountPictureNotes'] = 'Pots escriure una URL v&agrave;lida a una imatge aqu&iacute;, com ara: <strong>http://www.elmeuweb.com/imatgedelcompte.jpg</strong>
	<br />La teva imatge de compte apareixer&agrave; a la teva p&agrave;gina de compte. Es centrar&agrave; autom&agrave;ticament i es retallar&agrave; a 280 p&iacute;xels d\'ample per 200 d\'alt.';
$Context->Dictionary['IconNotes'] = 'Pots escriure una URL v&agrave;lida a una imatge aqu&iacute;, com ara: <strong>http://www.elmeuweb.com/iconacompte.jpg</strong>
	<br />La icona apareixer&agrave; al costat del teu nom en els comentaris i converses i a la p&agrave;gina del teu compte. La icona es centrar&agrave; i retallar&agrave; autom&agrave;ticament a 32 p&iacute;xels d\'ample per 32 d\'alt.';
$Context->Dictionary['AddCustomInformation'] = 'Afegir informaci&oacute; personalitzada';
$Context->Dictionary['AddCustomInformationNotes'] = 'Amb aquests camps pots afegir informaci&oacute; personalitzada al teu perfil de compte, com ara parelles d\'etiquetes/valor. (p.ex.: <i>"Aniversari"</i> i <i>"1 de gener"</i>, o <i>"Grup preferit"</i> i <i>"Els Pets"</i>). Els valors que es precedeixin per un protocol del tipus http://, mailto:, ftp://, aim:, etc es convertiran autom&agrave;ticament en un enlla&ccedil;. Pots afegir tantes combinacions etiquetes/valor com vulguis.';
$Context->Dictionary['Label'] = 'Etiqueta';
$Context->Dictionary['Value'] = 'Valor';
$Context->Dictionary['AddLabelValuePair'] = 'Afegir una nova parella d\'etiqueta/valor';
$Context->Dictionary['Save'] = 'Guardar';
$Context->Dictionary['Cancel'] = 'Cancel&middot;lar i tornar enrere';
$Context->Dictionary['YourOldPasswordNotes'] = 'La contrasenya que fas servir actualment per entrar en aquest f&ograve;rum.';
$Context->Dictionary['YourNewPasswordNotes'] = 'No facis servir dates de naixement, n&uacute;meros PIN de targes de cr&egrave;dit, n&uacute;meros de tel&egrave;fon o qualsevol altra dada que es pugui esbrinar f&agrave;cilment <strong>i SI US PLAU no utilitzis aqu&iacute; la mateix contrasenya que fas servir en altres webs.</strong>';
$Context->Dictionary['Required'] = '(obligatori)';
$Context->Dictionary['YourNewPasswordAgain'] = 'Confirmar Nova Contrasenya';
$Context->Dictionary['YourNewPasswordAgainNotes'] = 'Torna a escriure la contrasenya per assegurar que no hi hagi cap error.';
$Context->Dictionary['ForumFunctionality'] = 'Prefer&egrave;ncies del F&ograve;rum';
$Context->Dictionary['ForumFunctionalityNotes'] = 'Els canvis que facis en aquest formulari s&oacute;n immediats. No cal pr&eacute;mer cap bot&oacute; per enviar els canvis.';
$Context->Dictionary['ControlPanel'] = 'Panell de Control';
$Context->Dictionary['CommentsForm'] = 'Formulari de Comentaris';
$Context->Dictionary['ShowFormatTypeSelector'] = 'Mostrar el selector de format de comentari a l\'afegir un comentari';
$Context->Dictionary['NewUsers'] = 'Nous Usuaris';
$Context->Dictionary['NewApplicantNotifications'] = 'Rebre avisos per correu electr&ograve;nic quan es registrin nous usuaris';
$Context->Dictionary['AssignToRole'] = 'Escollir un Nou Rol';
$Context->Dictionary['AssignToRoleNotes'] = 'El canvi de rol ser&agrave; efectiu immediatament. Si l\'usuari &eacute;s canviat a un rol que no t&eacute; acc&eacute;s al registre d\'usuari, es desregistrar&agrave; a la pr&ograve;xima p&agrave;gina.';
$Context->Dictionary['RoleChangeInfo'] = 'Notes sobre el canvi de rol';
$Context->Dictionary['RoleChangeInfoNotes'] = 'Si us plau proporciona algun comentari sobre aquest canvi de rol. Aquest ser&agrave; visible per tots els usuaris en l\'hist&ograve;ric de rols d\'aquest usuari.';
$Context->Dictionary['AboutMembership'] = '<h2>Qu&egrave; vol dir ser membre</h2>
	<p>Aquesta <strong>sol&middot;licitud per ser membre</strong> no et d&oacute;na acc&eacute;s instantani al f&ograve;rum. Totes les sol&middot;licituds s&oacute;n revisadas per un administrador abans de ser aprovades.</p>
	<p>Si us plau no entris informaci&oacute; inv&agrave;lida o incorrecta en aquest formulari si vols tenir acc&eacute;s al f&ograve;rum.</p>
	<p>Tota la informaci&oacute; que entris en aquest formulari ser&agrave; tractada de forma estrictament confidencial.</p>';
$Context->Dictionary['BackToSignInForm'] = 'Tornar al formulari d\'entrada';
$Context->Dictionary['MembershipApplicationForm'] = 'Formulari de sol&middot;licitud d\'ingr&eacute;s';
$Context->Dictionary['AllFieldsRequired'] = '**Tots els camps s&oacute;n obligatoris';
$Context->Dictionary['IHaveReadAndAgreeTo'] = 'He llegit i accepto //1';
$Context->Dictionary['TermsOfService'] = 'les condicions d\'&uacute;s del servei';
$Context->Dictionary['CommentHiddenOnXByY'] = 'Ocult //1 per //2';
$Context->Dictionary['ToYou'] = ' per tu;';
$Context->Dictionary['ToYourself'] = ' per tu mateix';
$Context->Dictionary['ToX'] = ' per //1';
$Context->Dictionary['Edited'] = 'editat';
$Context->Dictionary['edit'] = 'editar';
$Context->Dictionary['Edit'] = 'Editar';
$Context->Dictionary['Show'] = 'mostrar';
$Context->Dictionary['Hide'] = 'ocultar';
$Context->Dictionary['WhisperBack'] = 'Contestar en privat';
$Context->Dictionary['AddYourComments'] = 'Afegeix els teus comentaris';
$Context->Dictionary['TopOfPage'] = 'Amunt';
$Context->Dictionary['BackToDiscussions'] = 'Tornar al llistat de converses';
$Context->Dictionary['SignOutSuccessful'] = 'Has sortit amb &egrave;xit';
$Context->Dictionary['SignInAgain'] = 'Clica per tornar a entrar';
$Context->Dictionary['RequestProcessed'] = 'S\'ha processat la teva petici&oacute;';
$Context->Dictionary['MessageSentToXContainingPasswordInstructions'] = 'S\'ha enviat un missatge a <strong>//1</strong> amb instruccions per canviar la contrasenya.';
$Context->Dictionary['AboutYourPassword'] = 'Sobre la teva contrasenya';
$Context->Dictionary['AboutYourPasswordRequestNotes'] = '<strong>Aquest formulari no canvia la teva contrasenya</strong>. Omple\'l i rebr&agrave;s al teu correu les instruccions per poder-te\'n assignar una de nova.';
$Context->Dictionary['PasswordResetRequestForm'] = 'Reinicialitzar contrasenya.';
$Context->Dictionary['PasswordResetRequestFormNotes'] = 'Escriu el teu nom d\'usuari per reinicialitzar la teva contrasenya.';
$Context->Dictionary['SendRequest'] = 'Enviar';
$Context->Dictionary['PasswordReset'] = 'La teva contrasenya ha estat canviada';
$Context->Dictionary['SignInNow'] = 'Clica aqu&iacute; per entrar';
$Context->Dictionary['AboutYourPasswordNotes'] = 'A l\'escollir una contrasenya no tri&iuml;s dates d\'aniversari, n&uacute;meros PIN de targes de cr&egrave;dit, n&uacute;meros de tel&egrave;fon o qualsevol dada que pugui ser f&agrave;cilment endevinada <strong>I SI US PLAU, no facis servir la mataeixa que a altres p&agrave;gines web.</strong>';
$Context->Dictionary['PasswordResetForm'] = 'Canvi de contrasenya';
$Context->Dictionary['ChooseANewPassword'] = 'Escull una nova contrasenya i reescriu-la a sota.';
$Context->Dictionary['NewPassword'] = 'Contrasenya';
$Context->Dictionary['ConfirmPassword'] = 'Repeteix-la';
$Context->Dictionary['AllCategories'] = 'Totes les categories';
$Context->Dictionary['DateLastActive'] = 'Data d\'&uacute;ltima activitat';
$Context->Dictionary['Topics'] = 'Temes';
$Context->Dictionary['Comments'] = 'Comentaris';
$Context->Dictionary['Users'] = 'Usuaris';
$Context->Dictionary['AllRoles'] = 'Tots els rols';
$Context->Dictionary['Advanced'] = 'Avan&ccedil;ada';
$Context->Dictionary['ChooseSearchType'] = 'Cerca:';
$Context->Dictionary['DiscussionTopicSearch'] = 'Cerca de temes de conversa';
$Context->Dictionary['FindDiscussionsContaining'] = 'Cercar temes de conversa que continguin';
$Context->Dictionary['InTheCategory'] = 'dins aquesta categoria';
$Context->Dictionary['WhereTheAuthorWas'] = 'd\'aquest autor';
$Context->Dictionary['Search'] = 'Cercar';
$Context->Dictionary['DiscussionCommentSearch'] = 'Cerca en comentaris';
$Context->Dictionary['FindCommentsContaining'] = 'Cercar comentaris que continguin';
$Context->Dictionary['UserAccountSearch'] = 'Cerca d\'usuaris';
$Context->Dictionary['FindUserAccountsContaining'] = 'Cercar usuaris que continguin';
$Context->Dictionary['InTheRole'] = 'en el rol';
$Context->Dictionary['SortResultsBy'] = 'ordenar resultats per';
$Context->Dictionary['NoResultsFound'] = 'No s\'han trobat resultats';
$Context->Dictionary['DiscussionsCreated'] = 'Converses creades';
$Context->Dictionary['AdministrativeOptions'] = 'Opcions';
$Context->Dictionary['ApplicationSettings'] = 'Ajustaments de l\'Aplicaci&oacute;';
$Context->Dictionary['ManageExtensions'] = 'Extensions';
$Context->Dictionary['RoleManagement'] = 'Permisos i Rols';
$Context->Dictionary['CategoryManagement'] = 'Categories';
$Context->Dictionary['MembershipApplicants'] = 'Sol&middot;licituds d\'ingr&eacute;s';
$Context->Dictionary['GlobalApplicationSettings'] = 'Ajustaments de l\'Aplicaci&oacute;';
$Context->Dictionary['GlobalApplicationSettingsNotes'] = 'COMPTE amb els canvis que facis en aquesta p&agrave;gina. Introduir informaci&oacute; err&ograve;nia aqu&iacute; pot causar problemes en el funcionament del f&ograve;rum i la necessitat d\'editar manualment els fitxers per reparar el problema.';
$Context->Dictionary['AboutSettings'] = 'Sobre els ajustaments';
$Context->Dictionary['AboutSettingsNotes'] = "<p class=\"Description\">En aquesta secci&oacute; pots manipular tots els ajustaments possibles d'aquesta instal&middot;laci&oacute; de Vanilla. A sota hi trobar&agrave;s un llistat dels elements del men&uacute; i les seves funcions. Hi trobar&agrave;s tots aquells elements que els permisos del teu perfil permetin mostrar:</p>
	<dl><dt>Ajustaments de l'Aplicaci&oacute;</dt>
	<dd>Aquesta &eacute;s la p&agrave;gina principal de configuraci&oacute; de Vanilla. Aqu&iacute;  podr&agrave;s canviar el text del Titular, canviar els ajustaments de spam, definir els ajustaments de les cookies i canviar ajustaments b&agrave;sics com els missatges privats, categories, etc.</dd>
	<dt>Actualitzacions i Recordatoris</dt>
	<dd>Configura la freq&uuml;&egrave;ncia dels avisos d'actualitzaci&oacute;. Visita Lussumo per obtenir les darreres actualitzacions de Vanilla.</dd>
	<dt>Rols i Permisos</dt>
	<dd>Afegeix, edita i organitza rols i permisos.</dd>
	<dt>Ajustaments de Registre</dt>
	<dd>Defineix com es gestionen els nous membres: quin rol se'ls assigna, si necessiten l'aprovaci&oacute; de l'administrador, etc.</dd>
	<dt>Categories</dt>
	<dd>Afegeix, edita i organitza les categories.</dd>
	<dt>Extensions</dt>
	<dd>Les extensions afegeixen noves funcions a Vanilla. Usa aquest men&uacute; per activar les extensions i trobar-ne de noves a Lussumo.</dd>
	<dt>Temes i Estils</dt>
	<dd>Canvia els temes (plantilles xhtml) sobre els que es construeix Vanilla, canvia l'estil per defecte (css i imatges) i aplica'l a tots els usuaris del sistema.</dd>
	<dt>Idiomes</dt>
	<dd>Canvia el diccionari d'idioma que utilitzar&agrave; Vanilla.</dd>
	<dt>Sol&middot;licituds d'Ingr&eacute;s</dt>
	<dd>Vanilla no t&eacute; una \"llista de membres\" com altres f&ograve;rums. En en seu lloc fa servir un cercador per trobar i gestionar els membres. Si cal aprovaci&oacute; pr&egrave;via per a l'ingr&eacute;s, aquest enlla&ccedil; fa una cerca filtrada dels membres pendents d'aprovaci&oacute;.</dd>
	<dt>Altres opcions</dt>
	<dd>Depenent dels permisos del teu rol i les extenxions actives, pot haver-hi m&eacute;s  o menys elements en aquest men&uacute;. Benvingut a la m&agrave;gia de les extensions de Vanilla!</dd>
</dl>";
$Context->Dictionary['HiddenInformation'] = 'Informaci&oacute; oculta';
$Context->Dictionary['DisplayHiddenDiscussions'] = 'Mostrar converses ocultes';
$Context->Dictionary['DisplayHiddenComments'] = 'Mostrar comentaris ocults';
$Context->Dictionary['Choose'] = 'Escull...';
$Context->Dictionary['GetCategoryToEdit'] = '1. Selecciona la categoria que vulguis editar';
$Context->Dictionary['Categories'] = 'Categories';
$Context->Dictionary['ModifyCategoryDefinition'] = '2. Modifica la definici&oacute; de la categoria';
$Context->Dictionary['DefineNewCategory'] = 'Defineix la nova categoria';
$Context->Dictionary['CategoryName'] = 'Nom de la categoria';
$Context->Dictionary['CategoryNameNotes'] = 'El nom de categoria es mostrar&agrave; en l\'index i p&agrave;gina de les converses. No est&agrave; perm&egrave;s l\'Html.';
$Context->Dictionary['CategoryDescription'] = 'Descripci&oacute; de la Categoria';
$Context->Dictionary['CategoryDescriptionNotes'] = 'El valor que entris es mostrar&agrave; en la p&agrave;gina de categories. No est&agrave; perm&egrave;s l\'Html.';
$Context->Dictionary['RolesInCategory'] = 'Rols permesos per participar en aquesta categoria';
$Context->Dictionary['SelectCategoryToRemove'] = '1. Selecciona la categoria que vulguis eliminar';
$Context->Dictionary['SelectReplacementCategory'] = '2. Selecciona una categoria de reempla&ccedil;ament';
$Context->Dictionary['ReplacementCategory'] = 'Categoria de reempla&ccedil;ament';
$Context->Dictionary['ReplacementCategoryNotes'] = 'Quan s\'elimina una categoria del sistema, les converses que contenia queden orfes. La categoria de reempla&ccedil;ament ser&agrave; la que s\'assignar&agrave; a totes les converses que estaven assignades a la categoria eliminada.';
$Context->Dictionary['Remove'] = 'Eliminar';
$Context->Dictionary['CreateNewCategory'] = 'Crear una nova categoria';
$Context->Dictionary['CategoryRemoved'] = 'La categoria ha estat eliminada.';
$Context->Dictionary['CategorySaved'] = 'Els teus canvis s\'han desat amb &egrave;xit.';
$Context->Dictionary['NewCategorySaved'] = 'La categoria s\'ha creat amb &egrave;xit.';
$Context->Dictionary['SelectRoleToEdit'] = '1. Selecciona el rol que vulguis editar';
$Context->Dictionary['Roles'] = 'Rols';
$Context->Dictionary['ModifyRoleDefinition'] = '2. Modificar la definici&oacute; del rol';
$Context->Dictionary['DefineNewRole'] = 'Defineix el nou rol';
$Context->Dictionary['RoleName'] = 'Nom del Rol';
$Context->Dictionary['RoleNameNotes'] = 'El nom de rol es mostrar&agrave; a la p&agrave;gina d\'usuari, al costat del seu nom.  No est&agrave; perm&egrave;s l\'Html.';
$Context->Dictionary['RoleIcon'] = 'Icona del rol';
$Context->Dictionary['RoleIconNotes'] = "Pots introduir qualsevol RUTA v&agrave;lida a una imatge aqu&iacute;, per exemple: <strong>http://www.elmeuweb.com/lamevaicona.jpg</strong>
	<br />La icona de rol substituir&agrave; la icona d'usuari en tots els comentaris i en la p&agrave;gina del compte. Si no hi entres cap valor, es mantindr&agrave; la icona que el mateix usuari s'hagi assignat, si &eacute;s el cas.";
$Context->Dictionary['RoleTagline'] = 'Etiqueta de Rol';
$Context->Dictionary['RoleTaglineNotes'] = "L'etiqueta de rol es mostrar&agrave; en en la p&agrave;gina de compte d'usuari, sota el nom. Si no hi entres cap valor, l'etiqueta simplement no apareixer&agrave; a la p&agrave;gina d'usuari.";
$Context->Dictionary['RoleAbilities'] = 'Permisos de Rol';
$Context->Dictionary['RoleAbilitiesNotes'] = 'Selecciona els permisos que vulguis assignar a aquest rol.';
$Context->Dictionary['RoleRemoved'] = 'El rol ha estat eliminat.';
$Context->Dictionary['RoleSaved'] = 'Els teus canvis s\han desat amb &egrave;xit.';
$Context->Dictionary['NewRoleSaved'] = 'El rol s\'ha creat amb &egrave;xit.';
$Context->Dictionary['StartANewDiscussion'] = 'Encetar una nova conversa';
$Context->Dictionary['SelectRoleToRemove'] = '1. Selecciona el rol que vulguis eliminar';
$Context->Dictionary['SelectReplacementRole'] = '2. Selecciona un rol de reempla&ccedil;ament';
$Context->Dictionary['ReplacementRole'] = 'Rol de Reempla&ccedil;ament';
$Context->Dictionary['ReplacementRoleNotes'] = 'Quan s\'elimina un rol del sistema, els usuaris que el tinguessin assignat deixen de tenir permisos. El rol de reempla&ccedil;ament ser&agrave; el que s\'assignar&agrave; a tots els usuaris que tinguin assignat el rol que vols eliminar.';
$Context->Dictionary['CreateANewRole'] = 'Crear un nou rol';
$Context->Dictionary['Extensions'] = 'Extensions';
$Context->Dictionary['YouAreSignedIn'] = 'Est&agrave;s registrat';
$Context->Dictionary['BottomOfPage'] = 'Avall';
$Context->Dictionary['NotSignedIn'] = 'Per participar cal estar registrat'; // No est&agrave;s registrat';
$Context->Dictionary['SignIn'] = 'Registrar-se';
$Context->Dictionary['Discussions'] = 'Converses';
$Context->Dictionary['Settings'] = 'Ajustaments';
$Context->Dictionary['Account'] = 'Compte';
$Context->Dictionary['AllDiscussions'] = 'Totes les converses';
$Context->Dictionary['Category'] = 'Categoria';
$Context->Dictionary['StartedBy'] = 'Oberta per';
$Context->Dictionary['LastCommentBy'] = 'Darrer comentari de';
$Context->Dictionary['PageDetailsMessage'] = '//1 a //2';
$Context->Dictionary['PageDetailsMessageFull'] = '//1 a //2 de //3';
$Context->Dictionary['SearchResultsMessage'] = 'Resultats //1 de //2 per //3';
$Context->Dictionary['NoSearchResultsMessage'] = 'No \'ha trobat cap resultat';
$Context->Dictionary['Previous'] = 'Anterior';
$Context->Dictionary['Next'] = 'Seg&uuml;ent';
$Context->Dictionary['WrittenBy'] = 'Escrit per';
$Context->Dictionary['Added'] = 'Afegit';
$Context->Dictionary['MyAccount'] = 'El meu Compte';
$Context->Dictionary['ApplyForMembership'] = 'Sol&middot;licitar ingr&eacute;s';
$Context->Dictionary['SignOut'] = 'Sortir';
$Context->Dictionary['ResetYourPassword'] = 'Restaura la teva contrasenya';
$Context->Dictionary['AdministrativeSettings'] = 'Ajustaments d\'Administraci&oacute;';
$Context->Dictionary['TermsOfServiceBody'] = "<h1>Termes del Servei</h1>
<h2>Please carefully review the following rules, policies, and disclaimers.</h2>

<p>Considering the real-time nature of this community, it is impossible for us to review messages or confirm the validity of information posted.
We do not actively monitor the contents of and are not responsible for any content posted.
We do not vouch for or warrant the accuracy, completeness or usefulness of any message, and are not responsible for the contents of any data posted by members. 
The messages express the views of the author of the message, not necessarily the views of this community or any entity associated with this community.
Any user who feels that a posted message is objectionable is encouraged to contact us immediately by email.
We have the ability to remove objectionable messages and we will make every effort to do so, within a reasonable time frame, if we determine that removal is necessary.
This is a manual process, however, so please realize that we may not be able to remove or edit particular messages immediately.</p>

<p>You agree, through your use of this service, that you will not use this community to post any material which is knowingly false and/or defamatory, inaccurate, abusive, vulgar, hateful, harassing, obscene, profane, sexually oriented, threatening, invasive of a person's privacy, or otherwise violative of any law.
You agree not to post any copyrighted material unless the copyright is owned by you.</p>

<p>Although this community does not and cannot review the messages posted and is not responsible for the content of any of these messages, we at this community reserve the right to delete any message for any or no reason at all.
You remain solely responsible for the content of your messages, and you agree to indemnify and hold harmless this community, Lussumo (the makers of the discussion software), and their agents with respect to any claim based upon transmission of your message(s).</p>

<p>We at this community also reserve the right to reveal your identity (or whatever information we know about you) in the event of a complaint or legal action arising from any message posted by you.
We log all internet protocol addresses accessing this web site.</p>

<p>Please note that advertisements, chain letters, pyramid schemes, and solicitations are inappropriate on this community.</p>

<p><strong>We reserve the right to terminate any membership for any reason or no reason at all.</strong></p>";
$Context->Dictionary['EmailAddress'] = 'E-mail'; 
$Context->Dictionary['PasswordAgain'] = 'Repetir contrasenya';
$Context->Dictionary['SignedInAsX'] = 'Registrat com a //1';
$Context->Dictionary['AccountOptions'] = 'Opcions del Compte';
$Context->Dictionary['ChangeYourPersonalInformation'] = 'Informaci&oacute; Personal';
$Context->Dictionary['ChangeYourPassword'] = 'Canviar Contrasenya';
$Context->Dictionary['ChangeForumFunctionality'] = 'Preferencies del F&ograve;rum';
$Context->Dictionary['YourFirstName'] = 'El teu nom';
$Context->Dictionary['YourLastName'] = 'Els teus cognoms';
$Context->Dictionary['YourEmailAddress'] = 'La teva adre&ccedil;a de correu electr&ograve;nic';
$Context->Dictionary['AccountPicture'] = 'Imatge del compte';
$Context->Dictionary['Icon'] = 'Icona';
$Context->Dictionary['MakeRealNameVisible'] = 'Mostrar el meu nom real als altres membres';
$Context->Dictionary['YourOldPassword'] = 'La teva contrasenya vella';
$Context->Dictionary['YourNewPassword'] = 'La teva contrasenya nova';
$Context->Dictionary['DiscussionTopic'] = 'Assumpte de la Discusi&oacute;';
$Context->Dictionary['EmailLower'] = 'email';
$Context->Dictionary['UsernameLower'] = 'nom d\'usuari';
$Context->Dictionary['PasswordLower'] = 'contrasenya';
$Context->Dictionary['NewPasswordLower'] = 'nova contrasenya';
$Context->Dictionary['RoleNameLower'] = 'nom del rol';
$Context->Dictionary['DiscussionTopicLower'] = 'assumpte de la conversa';
$Context->Dictionary['CommentsLower'] = 'comentaris';
$Context->Dictionary['CategoryNameLower'] = 'nom de categoria';
$Context->Dictionary['Options'] = 'Opcions';
$Context->Dictionary['BlockCategory'] = 'Bloquejar categoria';
$Context->Dictionary['UnblockCategory'] = 'Desbloquejar categoria';
$Context->Dictionary['BookmarkThisDiscussion'] = 'Afegir aquesta conversa a favorits';
$Context->Dictionary['UnbookmarkThisDiscussion'] = 'Eliminar aquesta conversa dels favorits';
$Context->Dictionary['HideConfirm'] = "N\'est&agrave;s segur de voler ocultar aquest comentari?";
$Context->Dictionary['ShowConfirm'] = "N\'est&agrave;s segur de voler mostrar aquest comentari?";
$Context->Dictionary['BookmarkText'] = 'Afegir a favorits';
$Context->Dictionary['ConfirmHideDiscussion'] = "N\'est&agrave;s segur de voler ocultar aquesta conversa?";
$Context->Dictionary['ConfirmUnhideDiscussion'] = "N\'est&agrave;s segur de voler mostrar aquesta conversa?";
$Context->Dictionary['ConfirmCloseDiscussion'] = "N\'est&agrave;s segur de voler tancar aquesta conversa?";
$Context->Dictionary['ConfirmReopenDiscussion'] = "N\'est&agrave;s segur de voler reobrir aquesta conversa?";
$Context->Dictionary['ConfirmSticky'] = "N\'est&agrave;s segur de voler convertir aquesta conversa en popular?";
$Context->Dictionary['ConfirmUnsticky'] = "N\'est&agrave;s segur de voler convertir aquesta conversa en no popular?";
$Context->Dictionary['ChangePersonalInformation'] = 'Canviar informaci&oacute; personal';
$Context->Dictionary['ApplicantOptions'] = 'Opcions de la Sol&middot;licitud';
$Context->Dictionary['ApproveForMembership'] = 'Aprovar la Sol&middot;licitud';
$Context->Dictionary['DeclineForMembership'] = 'Denegar la Sol&middot;licitud';
$Context->Dictionary['ChangeRole'] = 'Canviar Rol';
$Context->Dictionary['NewApplicantSearch'] = 'Cerca de Noves Sol&middot;licituds';
$Context->Dictionary['BigInput'] = 'entrada gran';
$Context->Dictionary['SmallInput'] = 'entrada petita';
$Context->Dictionary['EditYourDiscussionTopic'] = 'Editar l\'assumpte de la conversa';
$Context->Dictionary['EditYourComments'] = 'Editar els teus comentaris';
$Context->Dictionary['FormatCommentsAs'] = 'Formatar els comentaris com ';
$Context->Dictionary['SaveYourChanges'] = 'Desar els Canvis';
$Context->Dictionary['Text'] = 'Texto';
$Context->Dictionary['EnterYourDiscussionTopic'] = 'Escriu l\'assumpte de la conversa';
$Context->Dictionary['EnterYourComments'] = 'Escriu els teus comentaris';
$Context->Dictionary['StartYourDiscussion'] = 'Enceta una conversa';
$Context->Dictionary['ShowAll'] = 'Mostrar-ho tot';
$Context->Dictionary['DiscussionIndex'] = 'Opcions de la Llista de Converses';
$Context->Dictionary['JumpToLastReadComment'] = 'Saltar al darrer comentari quan es faci clic en l\'assumpte de la conversa';
$Context->Dictionary['NoDiscussionsFound'] = 'No s\'ha trobat cap conversa';
$Context->Dictionary['RegistrationManagement'] = 'Ajustaments del Registre';
$Context->Dictionary['NewMemberRole'] = 'Rol de nou membre';
$Context->Dictionary['NewMemberRoleNotes'] = 'Quan els nous usuaris sol&middot;liciten l\'ingr&eacute;s, aquest &eacute;s el rol que se\'ls assigna. Si aquest rol t&eacute; perm&iacute;s d\'entrada, se li donar&agrave; acc&eacute;s immediat al f&ograve;rum.';
$Context->Dictionary['RegistrationChangesSaved'] = 'Els canvis en el proc&eacute;s de registre s\'han desat amb &egrave;xit ';
$Context->Dictionary['ClickHereToContinue'] = 'Clica aqu&iacute; per continuar';
$Context->Dictionary['RegistrationAccepted'] = 'Registre acceptat.';
$Context->Dictionary['RegistrationPendingApproval'] = 'Registre pendent de l\'aprovaci&oacute; de l\'administrador.';
$Context->Dictionary['Applicant'] = 'Sol.licitant';
$Context->Dictionary['ThankYouForInterest'] = 'Gr&agrave;cies pel teu inter&egrave;s!';
$Context->Dictionary['ApplicationWillBeReviewed'] = 'La teva sol&middot;licitud d\'ingr&eacute;s ser&agrave; revisada per un administrador. Si &eacute;s acceptada, rebr&agrave;s l\'av&iacute;s al teu correu electr&ograve;nic.';
$Context->Dictionary['ApplicationComplete'] = 'Sol&middot;licitud completa!';
$Context->Dictionary['AccountChangeNotification'] = ': Canvis en el compte d\'usuari';
$Context->Dictionary['PasswordResetRequest'] = ': Sol.licitud de canvi de contrasenya';
$Context->Dictionary['LanguageManagement'] = 'Idiomes';
$Context->Dictionary['LanguageChangesSaved'] = 'L\'idioma s\'ha modificat amb &egrave;xit.';
$Context->Dictionary['ChangeLanguage'] = 'Escull un idioma';
$Context->Dictionary['ChangeLanguageNotes'] = 'Si el teu idioma no surt a la llista, pots mirar de <a href="http://lussumo.com/addons/">descarregar-lo als darrers idiomes de Lussumo</a>.';
$Context->Dictionary['CloseThisDiscussion'] = 'Tancar aquesta conversa';
$Context->Dictionary['ReOpenThisDiscussion'] = 'Reobrir aquesta conversa';
$Context->Dictionary['MakeThisDiscussionUnsticky'] = 'Convertir aquesta conversa en no popular';
$Context->Dictionary['MakeThisDiscussionSticky'] = 'Convertir aquesta conversa en popular';
$Context->Dictionary['HideThisDiscussion'] = 'Ocultar aquesta conversa';
$Context->Dictionary['UnhideThisDiscussion'] = 'Mostrar aquesta conversa';

// Warnings
$Context->Dictionary['ErrOpenDirectoryExtensions'] = 'No es pot obrir el directori d\'extensions. Assegura\'t que el PHP t&eacute; acc&eacute;s de lectura al directori //1 .';
$Context->Dictionary['ErrOpenDirectoryThemes'] = 'No es pot obrir el directori de temes.  Assegura\'t que el PHP t&eacute; acc&eacute;s de lectura al directori //1 .';
$Context->Dictionary['ErrOpenDirectoryStyles'] = 'No es pot obrir el directori d\'estils.  Assegura\'t que el PHP t&eacute; acc&eacute;s de lectura al directori //1 .';
$Context->Dictionary['ErrReadExtensionDefinition'] = 'S\'ha produ&iuml;t un error a l\'intentar llegir la definici&oacute; d\'extensions des de';
$Context->Dictionary['ErrReadFileExtensions'] = 'No es pot llegir el fitxer d\'extensions:';
$Context->Dictionary['ErrOpenFile'] = 'No s\'ha pogut obrir el fitxer.  Assegura\'t que el PHP t&eacute; acc&eacute;s de lectura al fitxer //1 .';
$Context->Dictionary['ErrWriteFile'] = 'No s\'ha pogut escriure al fitxer.';
$Context->Dictionary['ErrEmailSubject'] = 'Has d\'escriure un assumpte.';
$Context->Dictionary['ErrEmailRecipient'] = 'Has d\'escriure al menys un destinatari.';
$Context->Dictionary['ErrEmailFrom'] = 'Has d\'escriure l\'e-mail del remitent.';
$Context->Dictionary['ErrEmailBody'] = 'Has d\'escriure el cos del missatge de correu.';
$Context->Dictionary['ErrCategoryNotFound'] = 'No s\'ha pogut trobar la categoria que demanes.';
$Context->Dictionary['ErrCategoryReplacement'] = 'Has d\'escollir una categoria de reempla&ccedil;ament.';
$Context->Dictionary['ErrCommentNotFound'] = 'No s\'ha pogut trobar el comentari que demanes.';
$Context->Dictionary['ErrDiscussionID'] = 'No s\'ha proporcionat cap identificador de conversa.';
$Context->Dictionary['ErrCommentID'] = 'No s\'ha proporcionat cap identificador de comentari.';
$Context->Dictionary['ErrPermissionComments'] = 'No tens perm&iacute;s per administrar comentaris.';
$Context->Dictionary['ErrWhisperInvalid'] = 'No s\'ha trobat el detinatari del missatge privat.';
$Context->Dictionary['ErrDiscussionNotFound'] = 'No s\'ha pogut trobar la conversa que demanes.';
$Context->Dictionary['ErrSelectCategory'] = 'Has de seleccionar una categoria per aquesta conversa.';
$Context->Dictionary['ErrPermissionEditComments'] = 'No pots pas editar els comentaris d\'un altre usuari.';
$Context->Dictionary['ErrPermissionDiscussionEdit'] = 'No s\'ha pas modificat la conversa perqu&egrave; o no existeix, o no tens permisos administratius en aquesta categoria.';
$Context->Dictionary['ErrRoleNotFound'] = 'No s\'ha pogut trobar el rol que demanes.';
$Context->Dictionary['ErrPermissionInsufficient'] = 'No tens pas prous privilegis per fer aquesta sol&middot;licitud.';
$Context->Dictionary['ErrSearchNotFound'] = 'No s\ha pogut trobar el que cerques.';
$Context->Dictionary['ErrSearchLabel'] = 'Has de proporcionar una etiqueta per la teva cerca. Llavors podr&agrave;s fer clic damunt l\'etiqueta per comen&ccedil;ar la cerca.';
$Context->Dictionary['ErrRoleNotes'] = 'Has de proporcionar notes sobre el canvi de rol.';
$Context->Dictionary['ErrOldPasswordBad'] = 'La contrasenya vella que has entrat &eacute;s incorrecte.';
$Context->Dictionary['ErrNewPasswordMatchBad'] = 'La confirmaci&oacute; de la nova contrasenya no coincideix.';
$Context->Dictionary['ErrPasswordsMatchBad'] = 'Les contrasenyes que has entrat no coincideixen.';
$Context->Dictionary['ErrAgreeTOS'] = 'Has d\'acceptar els termes del servei.';
$Context->Dictionary['ErrUsernameTaken'] = 'Aquest nom d\'usuari el fa servir un altre membre.';
$Context->Dictionary['ErrUserNotFound'] = 'No s\'ha pogut trobar l\'usuari que busques.';
$Context->Dictionary['ErrRemoveUserStyle'] = 'No s\'ha pogut eliminar l\'usuari perqu&egrave; &eacute;s l\'autor d\'un estil.';
$Context->Dictionary['ErrRemoveUserComments'] = 'No s\'ha pogut eliminar l\'usuari perqu&egrave; ha escrit comentaris en alguna conversa.';
$Context->Dictionary['ErrRemoveUserDiscussions'] = 'No s\'ha pogut eliminar l\'usuari perqu&egrave; ha intervingut en alguna conversa.';
$Context->Dictionary['ErrInvalidUsername'] = 'Aquest nom d\'usuari no &eacute;s v&agrave;lid.';
$Context->Dictionary['ErrInvalidPassword'] = 'La contrasenya no &eacute;s v&agrave;lida.';
$Context->Dictionary['ErrAccountNotFound'] = 'No s\'ha pogut trobar cap compte amb aquest nom d\'usuari.';
$Context->Dictionary['ErrPasswordRequired'] = 'Has d\'entrar una nova contrasenya.';
$Context->Dictionary['ErrUserID'] = 'No has entrat l\'identificador d\'usuari.';
$Context->Dictionary['ErrPermissionUserSettings'] = 'No tens pas permisos per canviar els ajustaments d\'aquest usuari.';
$Context->Dictionary['ErrSpamComments'] = 'Has enviat //1 comentaris en //2 segons. Hem activat un filtre de spam al teu compte. Has d\'esperar almenys  //3 segons per enviar-ne un altre.';
$Context->Dictionary['ErrSpamDiscussions'] = 'Has enviat //1 converses en //2 segons. Hem activat un filtre de spam al teu compte. Has d\'esperar almenys //3 segons per comen&ccedil;ar una nova conversa.';
$Context->Dictionary['ErrUserCombination'] = 'No s\'ha trobat aquesta combinaci&oacute; de nom d\'usuari i contrasenya.';
$Context->Dictionary['ErrNoLogin'] = 'No tens perm&iacute;s per entrar a la p&agrave;gina.';
$Context->Dictionary['ErrPasswordResetRequest'] = 'hi ha hagut un error en la validaci&oacute; del canvi de contrasenya. Assegura\'t de copiar tota la url del teu correu.';
$Context->Dictionary['ErrSignInToDiscuss'] = 'No pots participar en les converses sense estar registrat.';
$Context->Dictionary['ErrPermissionCommentEdit'] = 'No tens prou permisos per editar el comentari seleccionat.';
$Context->Dictionary['ErrRequiredInput'] = 'Has d\'entrar algun valor per a l\'entrada //1.';
$Context->Dictionary['ErrInputLength'] = '//1 &eacute;s //2 car&agrave;cters massa llarg.';
$Context->Dictionary['ErrImproperFormat'] = 'No has entrat un valor amb format correcte per a ';
$Context->Dictionary['ErrOpenDirectoryLanguages'] = 'No s\ha pogut obrir el directori d\'idiomes. Assegura\'t que el PHP t&eacute; acc&eacute;s de lectura al directori //1.';
$Context->Dictionary['ErrPermissionAddComments'] = 'No tens permisos per afegir comentaris a les converses.';
$Context->Dictionary['ErrPermissionStartDiscussions'] = 'No tens permisos per encetar una conversa.';

$Context->Dictionary['Warning'] = 'Atenci&oacute;!';
$Context->Dictionary['ApplicationTitles'] = 'Nom del F&ograve;rum';
$Context->Dictionary['ApplicationTitle'] = 'T&iacute;tol de l\'Aplicaci&oacute;';
$Context->Dictionary['BannerTitle'] = 'T&iacute;tol del F&ograve;rum';
$Context->Dictionary['ApplicationTitlesNotes'] = 'El t&iacute;tol de l\'aplicaci&oacute; &eacute;s la que apareix a la barra de t&iacute;tol del tu navegador. El t&iacute;tol del f&ograve;rum es mostra damunt les pestanyes del men&uacute;. El t&iacute;tol del f&ograve;rum admet HTML.';
$Context->Dictionary['CountsTitle'] = 'Llistats de converses i Panell de Control';
$Context->Dictionary['DiscussionsPerPage'] = 'Converses per p&agrave;gina';
$Context->Dictionary['CommentsPerPage'] = 'Comentaris per p&agrave;gina';
$Context->Dictionary['SearchResultsPerPage'] = 'Resultats de cerca per p&agrave;gina';
$Context->Dictionary['MaxBookmarksInPanel'] = 'M&agrave;x. favorits en el panell';
$Context->Dictionary['MaxPrivateInPanel'] = 'M&agrave;x. privats en el panell';
$Context->Dictionary['MaxBrowsingHistoryInPanel'] = 'M&agrave;x. hist&ograve;ric en el panell';
$Context->Dictionary['MaxDiscussionsInPanel'] = 'M&agrave;x. converses pr&ograve;pies en el panell';
$Context->Dictionary['MaxSavedSearchesInPanel'] = 'M&agrave;x. cerques desades en el panell';
$Context->Dictionary['CountsNotes'] = 'Els valors escollits aqu&iacute; limiten el m&agrave;xim de converses o comentaris mostrats a la lista de converses, p&agrave;gina de comentaris i panell de control.';
$Context->Dictionary['SpamProtectionTitle'] = 'Protecci&oacute; de Spam';
$Context->Dictionary['MaxCommentLength'] = 'M&agrave;x. car&agrave;cters en els comentaris';
$Context->Dictionary['MaxCommentLengthNotes'] = 'Encara que la base de dades pugui transferir i emmagatzemar tanta informaci&oacute; como permeti la mem&ograve;ria del servidor, &eacute;s una bona idea mantenir una longitud m&agrave;xima raonable en els comentaris.';
$Context->Dictionary['XDiscussionsYSecondsZFreeze'] = 'Els membres no poden enviar m&eacute;s de //1 converses en //2 segons o el seu compte es congelar&agrave; durant //3 segons.';
$Context->Dictionary['XCommentsYSecondsZFreeze'] = 'Els membres no poden enviar m&eacute;s de //1 comentaris en //2 segons o el seu compte es congelar&agrave; durant //3 segons.';
$Context->Dictionary['LogAllIps'] = 'Registrar i monitoritzar totes les adreces IP';
$Context->Dictionary['SupportContactTitle'] = 'Contacte de Suport del F&ograve;rum';
$Context->Dictionary['SupportName'] = 'Nom del contacte de Suport';
$Context->Dictionary['SupportEmail'] = 'E-mail del contacte de suport';
$Context->Dictionary['SupportContactNotes'] = 'Tots els correus que s\'envi&iuml;n des del sistema tindran aquest nom i aquesta adre&ccedil;a electr&ograve;nica.';
$Context->Dictionary['DiscussionLabelsTitle'] = 'Etiquetes de Discusi&oacute;';
$Context->Dictionary['LabelPrefix'] = 'Prefix d\'etiqueta';
$Context->Dictionary['LabelSuffix'] = 'Sufix d\'etiqueta';
$Context->Dictionary['WhisperLabel'] = 'Etiqueta Privat';
$Context->Dictionary['StickyLabel'] = 'Etiqueta Popular';
$Context->Dictionary['SinkLabel'] = 'Etiqueta Enfonsar';
$Context->Dictionary['ClosedLabel'] = 'Etiqueta Tancat';
$Context->Dictionary['HiddenLabel'] = 'Etiqueta Ocult';
$Context->Dictionary['BookmarkedLabel'] = 'Etiqueta Favorit';
$Context->Dictionary['WebPathToVanilla'] = 'Ruta Web a Vanilla';
$Context->Dictionary['CookieDomain'] = 'Domini de Cookies';
$Context->Dictionary['WebPathNotes'] = 'La Ruta Web a Vanilla ha de ser la ruta completa tal i com l\'escriuries al navegador web. Per exemple: http://www.elmeudomini.com/vanilla/';
$Context->Dictionary['CookieSettingsNotes'] = 'El domini de cookies &eacute;s el que vols assignar a les cookies de Vanilla. T&iacute;picament, el domini de cookies ser&agrave; com ara www.elteudomini.com. Les cookies es poden definir en una ruta particular amb l\'ajustament "ruta de cookie". (PISTA: Si vols que les cookies de Vanilla afectin tots els subdominis del teu domini, fes servir ".elteudomini.com" com a domini de cookie)';
$Context->Dictionary['AllowNameChange'] = 'Permetre als usuaris canviar el seu nom d\'usuari';
$Context->Dictionary['AllowPublicBrowsing'] = 'Permetre als no membres navegar pel f&ograve;rum';
$Context->Dictionary['UseCategories'] = 'Categoritzar les converses';
$Context->Dictionary['DiscussionLabelsNotes'] = 'Les etiquetes de conversa es mostren davant l\'assumpte de la conversa en la p&agrave;gina principal de converses. El prefix i sufix d\'etiqueta es mostren a banda i banda de l\'etiqueta de conversa. Si l\'etiqueta de conversa est&agrave; buida el prefix i el sufix no es mostraran.';
$Context->Dictionary['ForumOptions'] = 'Opcions del F&ograve;rum';
$Context->Dictionary['GlobalApplicationChangesSaved'] = 'S\'han desat els canvis amb &egrave;xit';
$Context->Dictionary['ApprovedMemberRole'] = 'Rol de Membres Aprovats';
$Context->Dictionary['ApprovedMemberRoleNotes'] = 'Quan un administrador aprova una sol&middot;licitud d\'ingr&eacute;s (si &eacute;s que s\'escau), aquest &eacute;s el rol que se li assignar&agrave;.';
$Context->Dictionary['NewMemberWelcomeAboard'] = 'Ja tens perm&iacute;s d\acc&eacute;s. Benvingut a bord!';
$Context->Dictionary['RoleCategoryNotes'] = 'Selecciona les categories a les que aquest rol tindr&agrave; acc&eacute;s';
$Context->Dictionary['DebugTitle'] = 'Eliminaci&oacute; d\'errors de Vanilla';
$Context->Dictionary['DebugDescription'] = 'Si tens prou permisos permisos, pots activar la modalitat de funcionament d\'eliminaci&oacute; d\'errors de Vanilla i mostrar totes les consultes d\'una p&agrave;gina despr&eacute;s d\'executar-se. Ser&agrave;s la &uacute;nica persona que veur&agrave; aquestes dades. Fes servir aquesta p&agrave;gina  per activar i desactivar la modalitat d\'eliminaci&oacute; d\'errors.';
$Context->Dictionary['CurrentApplicationMode'] = 'Vanilla est&agrave; en aquests moments en la modalitat de funcionament: ';
$Context->Dictionary['DEBUG'] = 'Eliminaci&oacute; d\'errors';
$Context->Dictionary['RELEASE'] = 'Publicaci&oacute;';
$Context->Dictionary['SwitchApplicationMode'] = 'Fes clic aqu&iacute; per canviar la modalitat de funcionament de l\'aplicaci&oacute;';
$Context->Dictionary['BackToApplication'] = 'Fes clic aqu&iacute; para tornar a Vanilla';
$Context->Dictionary['ErrReadFileSettings'] = 'Hi ha hagut un error al provar de llegir els ajustaments des del fitxer de configuraci&oacute;: ';
$Context->Dictionary['CookiePath'] = 'Ruta de Cookie';
$Context->Dictionary['Wait'] = 'Espera';
$Context->Dictionary['OldPostDateFormatCode'] = 'j M Y';
$Context->Dictionary['XDayAgo'] = 'hace //1 dia';
$Context->Dictionary['XDaysAgo'] = 'hace //1 dies';
$Context->Dictionary['XHourAgo'] = 'hace //1 hora';
$Context->Dictionary['XHoursAgo'] = 'hace //1 hores';
$Context->Dictionary['XMinuteAgo'] = 'hace //1 minut';
$Context->Dictionary['XMinutesAgo'] = 'hace //1 minuts';
$Context->Dictionary['XSecondAgo'] = 'hace //1 segon';
$Context->Dictionary['XSecondsAgo'] = 'hace //1 segons';
$Context->Dictionary['nothing'] = 'res';
$Context->Dictionary['EnableWhispers'] = 'Activar missatges privats';
$Context->Dictionary['ExtensionFormNotes'] = 'Les extensions es fan servir per afegir noves funcions a Vanilla. M&eacute;s avall hi trobar&agrave;s llistades totes les extensions que estan instal&middot;lades. Per activar una extensi&oacute;, prem la casella de verificaci&oacute; al costat del nom de l\'extensi&oacute;. <a href="http://lussumo.com/addons/">M&eacute;s extensions de Lussumo Vanilla</a>.';
$Context->Dictionary['EnabledExtensions'] = 'Extensions Activades';
$Context->Dictionary['DisabledExtensions'] = 'Extensions Desactivadas';
$Context->Dictionary['ErrExtensionNotFound'] = 'No s\ha trobat l\'extensi&oacute; requerida.';
$Context->Dictionary['UpdatesAndReminders'] = 'Actualitzacions i Recordatoris';
$Context->Dictionary['UpdateCheck'] = 'Comprovar actualitzacions';
$Context->Dictionary['UpdateCheckNotes'] = 'Vanilla s\'actualitza constantment, a l\'anar-se solucionant els problemes que van sorgint i a l\'afegir-s\'hi (o eliminar) funcions desenvolupades per la comunitat. Per assegurar-te que tens la instal&middot;laci&oacute; de Vanilla al dia, &eacute;s important comprovar les actualitzacions regularment.';
$Context->Dictionary['CheckForUpdates'] = 'Comprovar actualitzacions ara';
$Context->Dictionary['ErrUpdateCheckFailure'] = 'S\'ha produ&iuml;t un error a l\'obtenir informaci&oacute; de Lussumo en relaci&oacute; a la darrera actualitzaci&oacute; de Vanilla. Si us plau, torna a provar-ho m&eacute;s tard.';
$Context->Dictionary['PleaseUpdateYourInstallation'] = '<strong>ATENCI&oacute;:</strong> La teva instal&middot;laci&oacute; de Vanilla &eacute;s //1, per&ograve; <span class="Highlight">la darrera versi&oacute; disponible a Vanilla &eacute;s //2</span>. Si us plau, actualitza la teva instal&middot;laci&oacute; immediatament descarregant la darrera versi&oacute; des de <a href="http://getvanilla.com">http://getvanilla.com</a>.';
$Context->Dictionary['YourInstallationIsUpToDate'] = 'Tens la instal&middot;laci&oacute; de Vanilla al dia. Ves-ho comprovant de tant en tant!';
$Context->Dictionary['ErrPermissionHideDiscussions'] = 'No tens permisos per ocultar converses.';
$Context->Dictionary['ErrPermissionCloseDiscussions'] = 'No tens permisos per tancar converses.';
$Context->Dictionary['ErrPermissionStickDiscussions'] = 'No tens permisos per convertir una conversa en popular.';
$Context->Dictionary['CategoryReorderNotes'] = 'Arrossega i deixa anar les categories per reordenar-les. Es desar&agrave; el nou ordre autom&agrave;ticament. Si una categoria apareix en gris &eacute;s que est&aacute; bloquejada pel teu rol.';
$Context->Dictionary['RoleReorderNotes'] = 'Arrossega i deixa anar els rols per reordenar-los. Es desar&agrave; el nou ordre ser&agrave; autom&agrave;ticament. El rol <i>Sense registrar</i> &eacute;s un rol especial que s\'aplica als usuaris que naveguen pel f&ograve;rum sense estar registrats i no es pot borrar.';
$Context->Dictionary['PERMISSION_CHECK_FOR_UPDATES'] = 'Comprovar actualitzacions';
$Context->Dictionary['PERMISSION_SIGN_IN'] = 'Pot registrar-se';
$Context->Dictionary['PERMISSION_ADD_COMMENTS'] = 'Afegir comentaris';
$Context->Dictionary['PERMISSION_ADD_COMMENTS_TO_CLOSED_DISCUSSION'] = 'Afegir comentaris a converses tancades';
$Context->Dictionary['PERMISSION_START_DISCUSSION'] = 'Encetar converses';
$Context->Dictionary['PERMISSION_HTML_ALLOWED'] = 'HTML i Imatges permeses';
$Context->Dictionary['PERMISSION_IP_ADDRESSES_VISIBLE'] = 'Adreces IP visibles';
$Context->Dictionary['PERMISSION_APPROVE_APPLICANTS'] = 'Aprovar Sol&middot;licituds d\'Ingr&eacute;s';
$Context->Dictionary['PERMISSION_MANAGE_REGISTRATION'] = 'Configuraci&oacute; de registre';
$Context->Dictionary['PERMISSION_EDIT_USERS'] = 'Editar qualsevol usuari';
$Context->Dictionary['PERMISSION_CHANGE_USER_ROLE'] = 'Canviar els rols d\'usuari';
$Context->Dictionary['PERMISSION_SORT_ROLES'] = 'Ordenar rols';
$Context->Dictionary['PERMISSION_ADD_ROLES'] = 'Afegir nous rols';
$Context->Dictionary['PERMISSION_EDIT_ROLES'] = 'Editar rols existents';
$Context->Dictionary['PERMISSION_REMOVE_ROLES'] = 'Eliminar rols existentes';
$Context->Dictionary['PERMISSION_STICK_DISCUSSIONS'] = 'Convertir converses en populars';
$Context->Dictionary['PERMISSION_HIDE_DISCUSSIONS'] = 'Ocultar converses';
$Context->Dictionary['PERMISSION_CLOSE_DISCUSSIONS'] = 'Tancar converses';
$Context->Dictionary['PERMISSION_EDIT_DISCUSSIONS'] = 'Editar qualsevol conversa';
$Context->Dictionary['PERMISSION_HIDE_COMMENTS'] = 'Ocultar comentaris';
$Context->Dictionary['PERMISSION_EDIT_COMMENTS'] = 'Editar qualsevol comentari';
$Context->Dictionary['PERMISSION_ADD_CATEGORIES'] = 'Afegir categories';
$Context->Dictionary['PERMISSION_EDIT_CATEGORIES'] = 'Editar categories';
$Context->Dictionary['PERMISSION_REMOVE_CATEGORIES'] = 'Eliminar categories';
$Context->Dictionary['PERMISSION_SORT_CATEGORIES'] = 'Ordenar categories';
$Context->Dictionary['PERMISSION_VIEW_HIDDEN_DISCUSSIONS'] = 'Mostrar categories ocultes';
$Context->Dictionary['PERMISSION_VIEW_HIDDEN_COMMENTS'] = 'Mostrar comentaris ocults';
$Context->Dictionary['PERMISSION_VIEW_ALL_WHISPERS'] = 'Mostrar tots els missatges privats';
$Context->Dictionary['PERMISSION_CHANGE_APPLICATION_SETTINGS'] = 'Canviar els ajustaments de l\'aplicaci&oacute;';
$Context->Dictionary['PERMISSION_MANAGE_EXTENSIONS'] = 'Extensions';
$Context->Dictionary['PERMISSION_MANAGE_LANGUAGE'] = 'Canviar idioma';
$Context->Dictionary['PERMISSION_MANAGE_STYLES'] = 'Administrar estils';
$Context->Dictionary['PERMISSION_MANAGE_THEMES'] = 'Administrar temes';
$Context->Dictionary['PERMISSION_RECEIVE_APPLICATION_NOTIFICATION'] = 'Notificaci&oacute; de noves sol&middot;licituds per correu electr&ograve;nic';
$Context->Dictionary['PERMISSION_ALLOW_DEBUG_INFO'] = 'Veure informaci&oacute; d\'eliminaci&oacute; d\'errors';
$Context->Dictionary['PERMISSION_DATABASE_CLEANUP'] = 'Permetre l\'&uacute;s de l\'extensi&oacute; de neteja';
$Context->Dictionary['PERMISSION_ADD_ADDONS'] = 'Pot afegir extensions';
$Context->Dictionary['NoEnabledExtensions'] = 'No hi ha cap extensi&oacute; activada.';
$Context->Dictionary['NoDisabledExtensions'] = 'No hi ha extensions desactivades.';
$Context->Dictionary['NA'] = 'n/a';
$Context->Dictionary['AboutExtensionPage'] = '<strong>La p&agrave;gina d\'extensions</strong><br />Aquesta p&agrave;gina la poden utilitzar els creadors d\'extensions per programar p&agrave;ginas d\'extensions personalitzades a Vanilla. Est&agrave;s veient la p&agrave;gina per defecte i probablement hagis arribat aqu&iacute; per accident o per l\'error d\'alguna extensi&oacute;.;.';
$Context->Dictionary['NewApplicant'] = 'Nova sol.licitud d\'alta';
$Context->Dictionary['PERMISSION_SINK_DISCUSSIONS'] = 'Enfonsar converses';
$Context->Dictionary['MakeThisDiscussionSink'] = 'Enfonsar aquesta conversa';
$Context->Dictionary['MakeThisDiscussionUnSink'] = 'Fer surar aquesta conversa';
$Context->Dictionary['ConfirmUnSink'] = "N\'est&agrave;s segur de voler fer surar aquesta conversa?";
$Context->Dictionary['ConfirmSink'] = "N\'est&agrave;s segur de voler enfonsar aquesta conversa?";
$Context->Dictionary['ErrPermissionSinkDiscussions'] = 'No tens permisos per enfonsar converses';
$Context->Dictionary['YourCommentsWillBeWhisperedToX'] = 'Els teus comentaris es faran en privat a  //1';
$Context->Dictionary['SMTPHost'] = 'Host SMTP';
$Context->Dictionary['SMTPUser'] = 'Usuari SMTP';
$Context->Dictionary['SMTPPassword'] = 'Contrasenya SMTP';
$Context->Dictionary['SMTPSettingsNotes'] = 'Per defecte, Vanilla far&agrave; servir el servidor de correu que estigui instal&middot;lat en el servidor. Si, per alguna ra&oacute;, vols fer servir un altre servidor de correu SMTP, pots configurar-lo amb aquestes tres opcions. Si no vols utilitzar un servidor SMTP, deixa aquests camps en blanc.';
$Context->Dictionary['PagelistNextText'] = 'Seg&uuml;ent';
$Context->Dictionary['PagelistPreviousText'] = 'Anterior';
$Context->Dictionary['EmailSettings'] = 'Configuraci&oacute; del Correu Electr&ograve;nic';
$Context->Dictionary['UpdateReminders'] = 'Recordatoris d\'Actualitzaci&oacute;';
$Context->Dictionary['UpdateReminderNotes'] = 'Vanilla es pot configurar perqu&egrave; et recordi la comprovaci&oacute; d\'actualitzacions. Qualsevol membre que tingui permisos para comprovar actualitzacions veur&agrave; aquest recordatori, que apareixer&agrave; damunt la llista de converses.';
$Context->Dictionary['ReminderLabel'] = 'Comprovar actualitzacions';
$Context->Dictionary['Never'] = 'Mai';
$Context->Dictionary['Weekly'] = 'Setmanalment';
$Context->Dictionary['Monthly'] = 'Mensualment';
$Context->Dictionary['Quarterly'] = 'Trimestralment (3 mesos)';
$Context->Dictionary['ReminderChangesSaved'] = 'S\'han desat els teus ajustaments de recordatori amb &egrave;xit.';
$Context->Dictionary['NeverCheckedForUpdates'] = 'Encara no has comprovat si hi ha actualitzacions de Vanilla.';
$Context->Dictionary['XDaysSinceUpdateCheck'] = 'Han passat //1 dies des de la darrera vegada que vas comprovar si hi havia actualitzacions de Vanilla.';
$Context->Dictionary['CheckForUpdatesNow'] = 'Fes clic aqu&iacute; per comprovar ara si hi ha actualitzacions';
$Context->Dictionary['ManageThemeAndStyle'] = 'Temes i Estils';
$Context->Dictionary['ThemeChangesSaved'] = 'S\'han destat els canvis amb &egrave;xit';
$Context->Dictionary['ThemeAndStyleNotes'] =  'Els Temes i Estils serveixen per canviar l\'estructura i aparen&ccedil;a de Vanilla respectivament. Per a m&eacute;s temes i estils, o per aprendre a fer els teus, <a href="http://lussumo.com/addons/">visita el directori d\'afegits de Vanilla</a>.';
$Context->Dictionary['ThemeLabel'] = 'Temes disponibles en la teva instal"laci&oacute; de Vanilla';
$Context->Dictionary['StyleLabel'] = 'Estils disponibles per al tema seleccionat';
$Context->Dictionary['ApplyStyleToAllUsers'] = 'Aplicar aquest estil a tots els usuaris';
$Context->Dictionary['ThemeAndStyleManagement'] = 'Temes i Estils';
$Context->Dictionary['Check'] = 'Marcar: ';
$Context->Dictionary['All'] = 'Tots';
$Context->Dictionary['None'] = 'Cap';
$Context->Dictionary['Simple'] = 'Simple';
$Context->Dictionary['ErrorFopen'] = 'Hi ha hagut un error a l\'intentar obtenir informaci&oacute; d\'un origen de dades extern (\\1).';
$Context->Dictionary['ErrorFromPHP'] = ' Aquest &eacute;s el missatge de PHP: \\1';
$Context->Dictionary['InvalidHostName'] = 'El nom del host introdu&iuml;t no &eacute;s v&agrave;lid: \\1';
$Context->Dictionary['WelcomeToVanillaGetSomeAddons'] = '<strong>Benvingut a Vanilla!</strong>
<br />Te n\'adonar&agrave;s ben aviat que &eacute;s molt... vainilla. Potser l\'hauries d\'amanir amb algun <a href="http://lussumo.com/addons/">afegit</a>.';
$Context->Dictionary['RemoveThisNotice'] = 'Eliminar aquest missatge';
$Context->Dictionary['OtherSettings'] = 'Altres ajustaments';
$Context->Dictionary['ChangesSaved'] = 'S\'han desat els teus canvis amb &egrave;xit';

// Added for Vanilla 1.1 on 2007-02-20
$Context->Dictionary['ErrPostBackKeyInvalid'] = 'Hi ha hagut un problema a l\'autenticar la informaci&oacute; del teu missatge.';
$Context->Dictionary['ErrPostBackActionInvalid'] = 'La informaci&oacute; del teu missatge no ha estat definida adequadament.';
// Moved from settings.php
$Context->Dictionary['TextWhispered'] = 'Privat';
$Context->Dictionary['TextSticky'] = 'Popular';
$Context->Dictionary['TextClosed'] = 'Tancat';
$Context->Dictionary['TextHidden'] = 'Eliminat';
$Context->Dictionary['TextSink'] = 'Enfonsat';
$Context->Dictionary['TextBookmarked'] = 'Marcat';
$Context->Dictionary['TextPrefix'] = '[';
$Context->Dictionary['TextSuffix'] = ']';
// Added for new update checker
$Context->Dictionary['CheckingForUpdates'] = 'Revisant actualitzacions...';
$Context->Dictionary['ApplicationStatusGood'] = 'Vanilla est&agrave; actualizat.';
$Context->Dictionary['ExtensionStatusGood'] = 'Aquesta extensi&oacute; est&agrave; actualitzada.';
$Context->Dictionary['ExtensionStatusUnknown'] = 'No s\'ha trobat l\'extensii&oacute;. <a href="http://lussumo.com/docs/doku.php?id=vanilla:administrators:updatecheck">Esbrina per qu&egrave;</a>';
$Context->Dictionary['NewVersionAvailable'] = 'La versi&oacute; \\1 est&agrave; disponible. <a href="\\2">Descarregar-la</a>';
// Altered for new applicant management screen
$Context->Dictionary['ApproveForMembership'] = 'Aprovar';
$Context->Dictionary['DeclineForMembership'] = 'Denegar';
$Context->Dictionary['ApplicantsNotes'] = 'Utilitza aquest formulari per aprovar o declinar noves sol&middot;licituds de membres.';
$Context->Dictionary['NoApplicants'] = 'No ni ha sol&middot;licituds per revisar...';
// Added for ajax/sortroles.php
$Context->Dictionary['ErrPermissionSortRoles'] = 'No tens permisos per canviar els rols';
$Context->Dictionary['ErrPermissionSortCategories'] = 'No tens permisos per canviar les categories';


/* Please do not remove or alter this definition */
$Context->Dictionary['PanelFooter'] = '<p id="AboutVanilla"><a href="http://getvanilla.com">Vanilla '.APPLICATION_VERSION.'</a> &eacute;s un producte de <a href="http://lussumo.com">Lussumo</a>. Per a m&eacute;s informaci&oacute;: <a href="http://lussumo.com/docs">Documentaci&oacute;</a>, <a href="http://lussumo.com/community">Suport</a>.</p>';

?>