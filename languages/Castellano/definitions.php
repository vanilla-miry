<?php
/*
* Copyright 2003 - 2005 Mark O'Sullivan
* Copyright 2010 Miriam Ruiz <miriam@debian.org>
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
* Contact Mark O'Sullivan at mark [at] lussumo [dot] com
*
* Description: Spanish (Spain) language dictionary
* Author: Alberto Tellez <alberto@2mdc.com>, Andres Rassol, Miriam Ruiz <miriam@debian.org>
* 
* !!!!!DO NOT ALTER THIS FILE!!!!!
* If you need to make changes to these definitions, add the new definitions to
* your conf/language.php file. Definitions made there will override these.
*/

// Define the xml:lang attribute for the html tag
$Context->Dictionary['XMLLang'] = 'es-es';

// Define all dictionary codes in English
$Context->Dictionary['NoDiscussionsNotSignedIn'] = 'No puedes participar en las conversaciones hasta que no te registres.';
$Context->Dictionary['SelectDiscussionCategory'] = 'Selecciona la categor&iacute;a para esta conversaci&oacute;n.';
$Context->Dictionary['WhisperYourCommentsTo'] = 'Susurra tus comentarios a <small>(opcional)</small>';
$Context->Dictionary['And'] = 'y';
$Context->Dictionary['Or'] = 'o';
$Context->Dictionary['ClickHereToContinueToDiscussions'] = 'Continuar a las conversaciones';
$Context->Dictionary['ClickHereToContinueToCategories'] = 'Continuar a las categor&iacute;as';
$Context->Dictionary['ReviewNewApplicants'] = 'Revisar las solicitudes de ingreso';
$Context->Dictionary['New'] = 'nuevos';
$Context->Dictionary['NewCaps'] = 'Nuevos';
$Context->Dictionary['Username'] = 'Nombre';
$Context->Dictionary['Password'] = 'Contrase&ntilde;a';
$Context->Dictionary['RememberMe'] = 'Recordarme';
$Context->Dictionary['ForgotYourPassword'] = 'Olvidaste tu contrase&ntilde;a?';
$Context->Dictionary['Proceed'] = 'Continuar';
$Context->Dictionary['ErrorTitle'] = 'Se encontraron algunos problemas';
$Context->Dictionary['RealName'] = 'Nombre real';
$Context->Dictionary['Email'] = 'Correo electr&oacute;nico';
$Context->Dictionary['Style'] = 'Estilo';
$Context->Dictionary['AccountCreated'] = 'Cuenta creada';
$Context->Dictionary['LastActive'] = '&Uacute;ltima modificaci&oacute;n';
$Context->Dictionary['VisitCount'] = 'Contador de visitas';
$Context->Dictionary['DiscussionsStarted'] = 'Convers. iniciadas';
$Context->Dictionary['CommentsAdded'] = 'Comentarios';
$Context->Dictionary['LastKnownIp'] = '&Uacute;ltima IP Conocida';
$Context->Dictionary['PermissionError'] = 'No tienes permisos para realizar la tarea solicitada.';
$Context->Dictionary['ChangePersonalInfo'] = 'Informaci&oacute;n Personal';
$Context->Dictionary['DefineYourAccountProfile'] = 'Define tu perfil';
$Context->Dictionary['YourUsername'] = 'Identificador';
$Context->Dictionary['YourUsernameNotes'] = 'Tu identificador aparecer&aacute; al lado de tus conversaciones y comentarios.';
$Context->Dictionary['YourFirstNameNotes'] = 'Aqu&iacute; deber&iacute;a ir tu nombre real. S&oacute;lo ser&aacute; visible desde tu perfil.';
$Context->Dictionary['YourLastNameNotes'] = 'Aqu&iacute; deber&iacute;an ir tus apellidos reales.  S&oacute;lo ser&aacute; visible desde tu perfil.';
$Context->Dictionary['YourEmailAddressNotes'] = 'Debes proporcionar una direcci&oacute;n de correo v&aacute;lida para recuperar tu contrase&ntilde;a en caso de p&eacute;rdida u olvido (el formulario de recuperaci&oacute;n de contrase&ntilde;a funciona por email).';
$Context->Dictionary['CheckForVisibleEmail'] = 'Hacer visible mi direcci&oacute;n de correo a otros miembros';
$Context->Dictionary['AccountPictureNotes'] = 'Puedes introducir cualquier URL v&aacute;lida a una imagen aqu&iacute;, como: <strong>http://www.miwebsite.com/imagencuenta.jpg</strong>
	<br />Tu imagen aparecer&aacute; en tu perfil, y ser&aacute; autom&aacute;ticamente centrada y recortada a 280 p&iacute;xels de ancho y 200 de alto.';
$Context->Dictionary['IconNotes'] = 'Puedes introducir cualquier URL v&aacute;lida a una imagen aqu&iacute;, como: <strong>http://www.miwebsite.com/iconocuenta.jpg</strong>
	<br />Tu icono aparacer&aacute; al lado de tu identificador en los comentarios de las conversaciones y en tu perfil. Tu icono ser&aacute; autom&aacute;ticamente centrado y recortado a 32 pixels de ancho y 32 de alto.';
$Context->Dictionary['AddCustomInformation'] = 'A&ntilde;adir informaci&oacute;n personalizada';
$Context->Dictionary['AddCustomInformationNotes'] = 'Usando los campos siguientes, puedes a&ntilde;adir informaci&oacute;n personalizada a tu perfil de cuenta, como parejas de etiqueta/valor. (ej. <i>"Cumplea&ntilde;os"</i> y <i>"18 de Julio"</i>, o <i>"Grupo Favorito"</i> y <i>"La Oreja de Van Gogh"</i>). Los valores que vayan precedidos de un protocolo como http://, mailto:, ftp://, aim:, etc ser&aacute;n linkados autom&aacute;ticamente. Puedes a&ntilde;adir tantas combinaciones de etiqueta/valor como desees.';
$Context->Dictionary['Label'] = 'Etiqueta';
$Context->Dictionary['Value'] = 'Valor';
$Context->Dictionary['AddLabelValuePair'] = 'A&ntilde;adir otra pareja de etiqueta/valor';
$Context->Dictionary['Save'] = 'Guardar';
$Context->Dictionary['Cancel'] = 'Cancelar y volver atr&aacute;s';
$Context->Dictionary['YourOldPasswordNotes'] = 'La contrase&ntilde;a que usas actualmente para entrar en este foro de conversaci&oacute;n.';
$Context->Dictionary['YourNewPasswordNotes'] = 'No uses fechas de nacimiento, n&uacute;mero PIN de tu tarjeta de cr&eacute;dito, n&uacute;mero de tel&eacute;fono o cualquier cosa que sea f&aacute;cilmente adivinada <strong>y POR FAVOR no uses la misma contrase&ntilde;a aqu&iacute; que la que usas en otros sitios web.</strong>';
$Context->Dictionary['Required'] = '(obligatorio)';
$Context->Dictionary['YourNewPasswordAgain'] = 'Confirmar Nueva Contrase&ntilde;a';
$Context->Dictionary['YourNewPasswordAgainNotes'] = 'Escribe de nuevo la contrase&ntilde;a para estar seguro que no has cometido ning&uacute;n fallo.';
$Context->Dictionary['ForumFunctionality'] = 'Preferencias del Foro';
$Context->Dictionary['ForumFunctionalityNotes'] = 'Los cambios efectuados en este formulario son efectivos de inmediato. No necesitas pulsar ning&uacute;n bot&oacute;n para enviar los cambios.';
$Context->Dictionary['ControlPanel'] = 'Panel de Control';
$Context->Dictionary['CommentsForm'] = 'Formulario de Comentarios';
$Context->Dictionary['ShowFormatTypeSelector'] = 'Mostrar el selector de formato de comentario al a&ntilde;adir un comentario';
$Context->Dictionary['NewUsers'] = 'Nuevas altas';
$Context->Dictionary['NewApplicantNotifications'] = 'Recibir notificaciones por email cuando se efect&uacute;n nuevas altas';
$Context->Dictionary['AssignToRole'] = 'Elegir un Nuevo Rol';
$Context->Dictionary['AssignToRoleNotes'] = 'El cambio de rol ser&aacute; efectivo inmediatamente. Si el nuevo rol asignado no permite registrarse, se producir&aacute; una eliminaci&oacute;n del registro en la siguiente p&aacute;gina que visite.';
$Context->Dictionary['RoleChangeInfo'] = 'Notas sobre el cambio de rol';
$Context->Dictionary['RoleChangeInfoNotes'] = 'Por favor proporcione alg&uacute;n comentario acerca de este cambio de rol. &Eacute;ste ser&aacute; visible p&uacute;blicamente en el hist&oacute;rico de roles de esta persona.';
$Context->Dictionary['AboutMembership'] = '<h2>Acerca de ser miembro</h2>
	<p>Esta <strong>solicitud de ser miembro</strong> no le da acceso instant&aacute;neo al foro. Todas las solicitudes son revisadas por un administrador antes de ser aprobadas.</p>
	<p>Por favor no introduzca informaci&oacute;n inv&aacute;lida o incorrecta en este formulario o seguramente no obtendr&aacute; acceso al foro.</p>
	<p>Toda la informaci&oacute;n introducida en este formulario ser&aacute; tratada de forma estrictamente confidencial.</p>';
$Context->Dictionary['BackToSignInForm'] = 'Volver al formulario de entrada';
$Context->Dictionary['MembershipApplicationForm'] = 'Formulario de solicitud de ingreso';
$Context->Dictionary['AllFieldsRequired'] = '**Todos los campos son obligatorios';
$Context->Dictionary['IHaveReadAndAgreeTo'] = 'He le&iacute;do y acepto los //1';
$Context->Dictionary['TermsOfService'] = 'Condiciones de uso';
$Context->Dictionary['CommentHiddenOnXByY'] = 'Comentario eliminado el //1 por //2';
$Context->Dictionary['ToYou'] = ' para t&iacute;';
$Context->Dictionary['ToYourself'] = ' para t&iacute; mismo/a';
$Context->Dictionary['ToX'] = ' para //1';
$Context->Dictionary['Edited'] = 'editado';
$Context->Dictionary['edit'] = 'editar';
$Context->Dictionary['Edit'] = 'Editar';
$Context->Dictionary['Show'] = 'recuperar';
$Context->Dictionary['Hide'] = 'eliminar';
$Context->Dictionary['WhisperBack'] = 'Contestar en susurros';
$Context->Dictionary['AddYourComments'] = 'A&ntilde;ade tus comentarios';
$Context->Dictionary['TopOfPage'] = 'Arriba';
$Context->Dictionary['BackToDiscussions'] = 'Volver a conversaciones';
$Context->Dictionary['SignOutSuccessful'] = 'Has salido con &eacute;xito';
$Context->Dictionary['SignInAgain'] = 'Click para volver a entrar';
$Context->Dictionary['RequestProcessed'] = 'Tu petici&oacute;n ha sido procesada';
$Context->Dictionary['MessageSentToXContainingPasswordInstructions'] = 'Se ha enviado un mensaje a <strong>//1</strong> con instrucciones para cambiar la contrase&ntilde;a.';
$Context->Dictionary['AboutYourPassword'] = 'Acerca de tu contrase&ntilde;a';
$Context->Dictionary['AboutYourPasswordRequestNotes'] = '<strong>Este formulario no cambiar&aacute; tu contrase&ntilde;a.</strong> Al enviar este formulario se te enviar&aacute;n instrucciones de c&oacute;mo cambiar tu contrase&ntilde;a.';
$Context->Dictionary['PasswordResetRequestForm'] = 'Formulario de petici&oacute;n de cambio de contrase&ntilde;a.';
$Context->Dictionary['PasswordResetRequestFormNotes'] = 'Introduce tu identificador para poder cambiar la contrase&ntilde;a.';
$Context->Dictionary['SendRequest'] = 'Mandar Petici&oacute;n';
$Context->Dictionary['PasswordReset'] = 'Tu contrase&ntilde;a ha sido cambiada con &eacute;xito';
$Context->Dictionary['SignInNow'] = 'Click aqu&iacute; para entrar';
$Context->Dictionary['AboutYourPasswordNotes'] = 'Cuando elijas una nueva contrase&ntilde;a, no uses fechas de cumplea&ntilde;os, n&uacute;meros PIN de tarjetas de cr&eacute;dito, n&uacute;meros de tel&eacute;fono o cualquier cosa que sea f&aacute;cilmente adivinada.<strong>Y POR FAVOR, no use la misma contrase&ntilde;a que en otras p&aacute;ginas web</strong>.';
$Context->Dictionary['PasswordResetForm'] = 'Formulario de cambio de contrase&ntilde;a';
$Context->Dictionary['ChooseANewPassword'] = 'Elige una nueva contrase&ntilde;a y escr&iacute;belo debajo.';
$Context->Dictionary['NewPassword'] = 'Nueva contrase&ntilde;a';
$Context->Dictionary['ConfirmPassword'] = 'De nuevo';
$Context->Dictionary['AllCategories'] = 'Todas las categor&iacute;as';
$Context->Dictionary['DateLastActive'] = 'Fecha de &uacute;ltima actividad';
$Context->Dictionary['Topics'] = 'Temas';
$Context->Dictionary['Comments'] = 'Comentarios';
$Context->Dictionary['Users'] = 'Personas';
$Context->Dictionary['AllRoles'] = 'Todos los roles';
$Context->Dictionary['Advanced'] = 'Avanzada';
$Context->Dictionary['ChooseSearchType'] = 'B&uacute;squeda:';
$Context->Dictionary['DiscussionTopicSearch'] = 'B&uacute;squeda de temas de conversaci&oacute;n';
$Context->Dictionary['FindDiscussionsContaining'] = 'Buscar temas de conversaci&oacute;n que contengan';
$Context->Dictionary['InTheCategory'] = 'en esta categor&iacute;a';
$Context->Dictionary['WhereTheAuthorWas'] = 'cuyo autor o autora sea';
$Context->Dictionary['Search'] = 'Buscar';
$Context->Dictionary['DiscussionCommentSearch'] = 'B&uacute;squeda en comentarios';
$Context->Dictionary['FindCommentsContaining'] = 'Buscar comentarios que contengan';
$Context->Dictionary['UserAccountSearch'] = 'B&uacute;squeda de personas';
$Context->Dictionary['FindUserAccountsContaining'] = 'Buscar cuentas que contengan';
$Context->Dictionary['InTheRole'] = 'en el rol';
$Context->Dictionary['SortResultsBy'] = 'ordenar resultados por';
$Context->Dictionary['NoResultsFound'] = 'No se encontraron resultados';
$Context->Dictionary['DiscussionsCreated'] = 'Conversaciones creadas';
$Context->Dictionary['AdministrativeOptions'] = 'Opciones';
$Context->Dictionary['ApplicationSettings'] = 'Generales';
$Context->Dictionary['ManageExtensions'] = 'Extensiones';
$Context->Dictionary['RoleManagement'] = 'Roles y permisos';
$Context->Dictionary['CategoryManagement'] = 'Categor&iacute;as';
$Context->Dictionary['MembershipApplicants'] = 'Solicitudes de ingreso';
$Context->Dictionary['GlobalApplicationSettings'] = 'Configuraci&oacute;n general de la aplicaci&oacute;n';
$Context->Dictionary['GlobalApplicationSettingsNotes'] = '<b>Ten cuidado</b> con los cambios que realices en esta p&aacute;gina. Introducir informaci&oacute;n err&oacute;nea aqu&iacute; puede causar que el foro deje de funcionar y sea necesario modificar manualmente los archivos de configuraci&oacute;n para reparar el problema.';
$Context->Dictionary['AboutSettings'] = 'Acerca de la configuraci&oacute;n';
$Context->Dictionary['AboutSettingsNotes'] = "<p class=\"Description\">En esta secci&oacute;n puedes modificar todas las opciones de configuraci&oacute;n de esta instalaci&oacute;n de Vanilla. Debajo se encuentra una lista de los elementos del men&uacute; y sus funciones. Dependiendo de los permisos que tengas, se ver&aacute;n o no todos los elementos listados:</p>
	<dl><dt>Configuraci&oacute;n de la Aplicaci&oacute;n</dt>
	<dd>Esta es la p&aacute;gina principal de configuraci&oacute;n de Vanilla. Aqu&iacute; podr&aacute;s cambiar el t&iacute;tulo, los ajustes respecto al spam, la configuraci&oacute;n de las cookies y cambiar par&aacute;metros b&aacute;sicos como los susurros, categor&iacute;as, etc.</dd>
	<dt>Actualizaciones</dt>
	<dd>Configure la frecuencia de recordatorios de actualizaci&oacute;n. Visite Lussumo para obtener las &uacute;ltimas actualizaciones de Vanilla.</dd>
	<dt>Roles y permisos</dt>
	<dd>A&ntilde;ade, edita y organiza roles y permisos.</dd>
	<dt>Registro</dt>
	<dd>Define c&oacute;mo se manejan los nuevos miembros: qu&eacute; rol se les asigna, si necesitan aprobaci&oacute;n del administrador, etc.</dd>
	<dt>Categor&iacute;as</dt>
	<dd>A&ntilde;ade, edita y organiza las categor&iacute;as.</dd>
	<dt>Extensiones</dt>
	<dd>Las extensiones a&ntilde;aden nuevas funciones a Vanilla. Usa este men&uacute; para activar las extensiones y encontrar nuevas extensiones en Lussumo.</dd>
	<dt>Temas y estilos</dt>
	<dd>Cambia los temas (plantillas XHTML) sobre los que se construye Vanilla, cambia el estilo por defecto (css e im&aacute;genes) y apl&iacute;calo a todas las cuentas del sistema.</dd>
	<dt>Idiomas</dt>
	<dd>Cambia el diccionario de idioma que usa Vanilla.</dd>
	<dt>Solicitudes de Ingreso</dt>
	<dd>Vanilla no tiene una \"lista de miembros\" como otros foros. En vez de eso, Vanilla usa la b&uacute;squeda para encontrar y gestionar los miembros. Si se requiere aprobaci&oacute;n para el ingreso, este enlace realiza una b&uacute;squeda filtrada de los miembros pendientes de aprobaci&oacute;n.</dd>
	<dt>Otras opciones</dt>
	<dd>Dependiendo de los permisos de tu rol y las extensiones activas, puede haber m&aacute;s elementos en este men&uacute;. Vanilla le da la bienvenida a la magia de las extensiones!</dd>
</dl>";
$Context->Dictionary['HiddenInformation'] = 'Informaci&oacute;n eliminada';
$Context->Dictionary['DisplayHiddenDiscussions'] = 'Mostrar conversaciones eliminadas';
$Context->Dictionary['DisplayHiddenComments'] = 'Mostrar comentarios eliminados';
$Context->Dictionary['Choose'] = 'Elige...';
$Context->Dictionary['GetCategoryToEdit'] = '1. Selecciona la categor&iacute;a que deseas editar';
$Context->Dictionary['Categories'] = 'Categor&iacute;as';
$Context->Dictionary['ModifyCategoryDefinition'] = '2. Modifica la definici&oacute;n de la categor&iacute;a';
$Context->Dictionary['DefineNewCategory'] = 'Define la nueva categor&iacute;a';
$Context->Dictionary['CategoryName'] = 'Nombre de la categor&iacute;a';
$Context->Dictionary['CategoryNameNotes'] = 'El nombre de categor&iacute;a ser&aacute; visible en el &iacute;ndice de conversaciones y en la p&aacute;gina de conversaciones. No est&aacute; permitido el HTML.';
$Context->Dictionary['CategoryDescription'] = 'Descripci&oacute;n de Categor&iacute;a';
$Context->Dictionary['CategoryDescriptionNotes'] = 'El valor introducido aqu&iacute; ser&aacute; visible en la p&aacute;gina de categor&iacute;as. No est&aacute; permitido el HTML.';
$Context->Dictionary['RolesInCategory'] = 'Roles permitidos para participar en esta categor&iacute;a';
$Context->Dictionary['SelectCategoryToRemove'] = '1. Selecciona la categor&iacute;a que deseas eliminar';
$Context->Dictionary['SelectReplacementCategory'] = '2. Selecciona una categor&iacute;a de reemplazo';
$Context->Dictionary['ReplacementCategory'] = 'Categor&iacute;a de reemplazo';
$Context->Dictionary['ReplacementCategoryNotes'] = 'Cuando se elimina una categor&iacute;a del sistema, las conversaciones que se encontraban en ella quedan hu&eacute;rfanas. La categor&iacute;a de reemplazo ser&aacute;a asignada a todas las conversaciones que estaban asignadas a la categor&iacute;a eliminada.';
$Context->Dictionary['Remove'] = 'Eliminar';
$Context->Dictionary['CreateNewCategory'] = 'Crear una nueva categor&iacute;a';
$Context->Dictionary['CategoryRemoved'] = 'La categor&iacute;a ha sido eliminada.';
$Context->Dictionary['CategorySaved'] = 'Tus cambios fueron guardados con &eacute;xito.';
$Context->Dictionary['NewCategorySaved'] = 'La categor&iacute;a fu&eacute; creada con &eacute;xito.';
$Context->Dictionary['SelectRoleToEdit'] = '1. Selecciona el rol que quieres editar';
$Context->Dictionary['Roles'] = 'Roles';
$Context->Dictionary['ModifyRoleDefinition'] = '2. Modificar la definici&oacute;n del rol';
$Context->Dictionary['DefineNewRole'] = 'Define el nuevo rol';
$Context->Dictionary['RoleName'] = 'Nombre del Rol';
$Context->Dictionary['RoleNameNotes'] = "El nombre de rol ser&aacute; visible en perfil, al lado del nombre de cada persona.  No est&aacute; permitido el HTML.";
$Context->Dictionary['RoleIcon'] = 'Icono de rol';
$Context->Dictionary['RoleIconNotes'] = "Puedes introducir cualquier RUTA v&aacute;lida a una imagen aqu&iacute;, por ejemplo: <strong>http://www.miwebsite.com/miicono.jpg</strong>
	<br />El icono de rol reemplazar&aacute; al icono personal en todos los comentarios y en tu perfil. Si no proporcionas un valor a este campo, el icono definido por cada persona se mantendr&aacute;, si existe.";
$Context->Dictionary['RoleTagline'] = 'Etiqueta de Rol';
$Context->Dictionary['RoleTaglineNotes'] = "La etiqueta de rol aparecer&aacute; en el perfil de cada persona, debajo del nombre. Si no proporcionas un valor a este campo, la etiqueta simplemente no aparecer&aacute; en el perfil personal.";
$Context->Dictionary['RoleAbilities'] = 'Permisos de Rol';
$Context->Dictionary['RoleAbilitiesNotes'] = 'Selecciona los permisos que deseas que tenga este rol.';
$Context->Dictionary['RoleRemoved'] = 'El rol ha sido eliminado.';
$Context->Dictionary['RoleSaved'] = 'Tus cambios han sido guardados con &eacute;xito.';
$Context->Dictionary['NewRoleSaved'] = 'El rol ha sido creado con &eacute;xito.';
$Context->Dictionary['StartANewDiscussion'] = 'Nueva conversaci&oacute;n';
$Context->Dictionary['SelectRoleToRemove'] = '1. Selecciona el rol que deseas elminar';
$Context->Dictionary['SelectReplacementRole'] = '2. Seleciona un rol de reemplazo';
$Context->Dictionary['ReplacementRole'] = 'Rol de Reemplazo';
$Context->Dictionary['ReplacementRoleNotes'] = 'Cuando se elimina un rol del sistema, quienes tengan ese rol asignado dejan de tener permisos. El rol de reemplazo ser&aacute; asignado a cualquiera que tenga asignado el rol que deseas eliminar.';
$Context->Dictionary['CreateANewRole'] = 'Crear un nuevo rol';
$Context->Dictionary['Extensions'] = 'Extensiones';
$Context->Dictionary['YouAreSignedIn'] = 'Te has identificado';
$Context->Dictionary['BottomOfPage'] = 'Abajo';
$Context->Dictionary['NotSignedIn'] = 'No te has identificado';
$Context->Dictionary['SignIn'] = 'Identificaci&oacute;n';
$Context->Dictionary['Discussions'] = 'Conversaciones';
$Context->Dictionary['Settings'] = 'Configuraci&oacute;n';
$Context->Dictionary['Account'] = 'Mi Perfil';
$Context->Dictionary['AllDiscussions'] = 'Todas las conversaciones';
$Context->Dictionary['Category'] = 'Categor&iacute;a';
$Context->Dictionary['StartedBy'] = 'Abierta por';
$Context->Dictionary['LastCommentBy'] = '&Uacute;ltimo comentario de';
$Context->Dictionary['PageDetailsMessage'] = '//1 a //2';
$Context->Dictionary['PageDetailsMessageFull'] = '//1 a //2 de //3';
$Context->Dictionary['SearchResultsMessage'] = 'Resultados //1 de //2 para //3';
$Context->Dictionary['NoSearchResultsMessage'] = 'No se encontraron resultados';
$Context->Dictionary['Previous'] = 'Anterior';
$Context->Dictionary['Next'] = 'Siguiente';
$Context->Dictionary['WrittenBy'] = 'Escrito por';
$Context->Dictionary['Added'] = 'A&ntilde;adido';
$Context->Dictionary['MyAccount'] = 'Mi Cuenta';
$Context->Dictionary['ApplyForMembership'] = 'Solicita el ingreso';
$Context->Dictionary['SignOut'] = 'Salir';
$Context->Dictionary['ResetYourPassword'] = 'Reset your password';
$Context->Dictionary['AdministrativeSettings'] = 'Configuraci&oacute;n de administraci&oacute;n';
$Context->Dictionary['TermsOfServiceBody'] = "<h1>Condiciones de uso</h1>
<h2>Please carefully review the following rules, policies, and disclaimers.</h2>

<p>Considering the real-time nature of this community, it is impossible for us to review messages or confirm the validity of information posted.
We do not actively monitor the contents of and are not responsible for any content posted.
We do not vouch for or warrant the accuracy, completeness or usefulness of any message, and are not responsible for the contents of any data posted by members. 
The messages express the views of the author of the message, not necessarily the views of this community or any entity associated with this community.
Any user who feels that a posted message is objectionable is encouraged to contact us immediately by email.
We have the ability to remove objectionable messages and we will make every effort to do so, within a reasonable time frame, if we determine that removal is necessary.
This is a manual process, however, so please realize that we may not be able to remove or edit particular messages immediately.</p>

<p>You agree, through your use of this service, that you will not use this community to post any material which is knowingly false and/or defamatory, inaccurate, abusive, vulgar, hateful, harassing, obscene, profane, sexually oriented, threatening, invasive of a person's privacy, or otherwise violative of any law.
You agree not to post any copyrighted material unless the copyright is owned by you.</p>

<p>Although this community does not and cannot review the messages posted and is not responsible for the content of any of these messages, we at this community reserve the right to delete any message for any or no reason at all.
You remain solely responsible for the content of your messages, and you agree to indemnify and hold harmless this community, Lussumo (the makers of the discussion software), and their agents with respect to any claim based upon transmission of your message(s).</p>

<p>We at this community also reserve the right to reveal your identity (or whatever information we know about you) in the event of a complaint or legal action arising from any message posted by you.
We log all internet protocol addresses accessing this web site.</p>

<p>Please note that advertisements, chain letters, pyramid schemes, and solicitations are inappropriate on this community.</p>

<p><strong>We reserve the right to terminate any membership for any reason or no reason at all.</strong></p>";
$Context->Dictionary['EmailAddress'] = 'Direcci&oacute;n de Email';
$Context->Dictionary['PasswordAgain'] = 'Repetir Contrase&ntilde;a';
$Context->Dictionary['SignedInAsX'] = 'Hola, //1';
$Context->Dictionary['AccountOptions'] = 'Opciones de Cuenta';
$Context->Dictionary['ChangeYourPersonalInformation'] = 'Informaci&oacute;n Personal';
$Context->Dictionary['ChangeYourPassword'] = 'Cambiar Contrase&ntilde;a';
$Context->Dictionary['ChangeForumFunctionality'] = 'Preferencias del Foro';
$Context->Dictionary['YourFirstName'] = 'Nombre';
$Context->Dictionary['YourLastName'] = 'Apellidos';
$Context->Dictionary['YourEmailAddress'] = 'Direcci&oacute;n de correo electr&oacute;nico';
$Context->Dictionary['AccountPicture'] = 'Foto de la Cuenta';
$Context->Dictionary['Icon'] = 'Icono';
$Context->Dictionary['MakeRealNameVisible'] = 'Hacer mi nombre real visible a otros miembros';
$Context->Dictionary['YourOldPassword'] = 'Tu contras&ntilde;a antigua';
$Context->Dictionary['YourNewPassword'] = 'Tu contras&ntilde;a nueva';
$Context->Dictionary['DiscussionTopic'] = 'Tema de conversaci&oacute;n';
$Context->Dictionary['EmailLower'] = 'email';
$Context->Dictionary['UsernameLower'] = 'nombre';
$Context->Dictionary['PasswordLower'] = 'contras&ntilde;a';
$Context->Dictionary['NewPasswordLower'] = 'nueva contras&ntilde;a';
$Context->Dictionary['RoleNameLower'] = 'nombre del rol';
$Context->Dictionary['DiscussionTopicLower'] = 'tema de conversaci&oacute;n';
$Context->Dictionary['CommentsLower'] = 'comentarios';
$Context->Dictionary['CategoryNameLower'] = 'nombre de categor&iacute;a';
$Context->Dictionary['Options'] = 'Opciones';
$Context->Dictionary['BlockCategory'] = 'Bloquear categor&iacute;a';
$Context->Dictionary['UnblockCategory'] = 'Desbloquear categor&iacute;a';
$Context->Dictionary['BookmarkThisDiscussion'] = 'A&ntilde;adir esta conversaci&oacute;n a favoritos';
$Context->Dictionary['UnbookmarkThisDiscussion'] = 'Eliminar esta conversaci&oacute;n de favoritos';
$Context->Dictionary['HideConfirm'] = '&iquest;De verdad quieres eliminar este comentario?';
$Context->Dictionary['ShowConfirm'] = '&iquest;De verdad quieres recuperar este comentario?';
$Context->Dictionary['BookmarkText'] = 'A&ntilde;adir a favoritos';
$Context->Dictionary['MoveText'] = 'Mover esta conversaci&oacute;n';
$Context->Dictionary['SelectCategoryToMoveTo'] = 'Selecciona categoría';
$Context->Dictionary['ConfirmMoveDiscussion'] = '&iquest;De verdad quieres mover esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmHideDiscussion'] = '&iquest;De verdad quieres eliminar esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmUnhideDiscussion'] = '&iquest;De verdad quieres recuperar esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmCloseDiscussion'] = '&iquest;De verdad quieres cerrar esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmReopenDiscussion'] = '&iquest;De verdad quieres reabrir esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmSticky'] = '&iquest;De verdad quieres etiquetar esta conversaci&oacute;n como Fija?';
$Context->Dictionary['ConfirmUnsticky'] = '&iquest;De verdad quieres eliminar la etiqueta de Fija de esta conversaci&oacute;n?';
$Context->Dictionary['ChangePersonalInformation'] = 'Cambiar informaci&oacute;n personal';
$Context->Dictionary['ApplicantOptions'] = 'Opciones de la Solicitud';
$Context->Dictionary['ApproveForMembership'] = 'Aprobar la Solicitud';
$Context->Dictionary['DeclineForMembership'] = 'Rechazar la Solicitud';
$Context->Dictionary['ChangeRole'] = 'Cambiar Rol';
$Context->Dictionary['NewApplicantSearch'] = 'B&uacute;squeda de Nuevas Solicitudes';
$Context->Dictionary['BigInput'] = 'entrada grande';
$Context->Dictionary['SmallInput'] = 'entrada peque&ntilde;a';
$Context->Dictionary['EditYourDiscussionTopic'] = 'Editar el tema de la conversaci&oacute;n';
$Context->Dictionary['EditYourComments'] = 'Editar tus comentarios';
$Context->Dictionary['FormatCommentsAs'] = 'Formatear los comentarios como ';
$Context->Dictionary['SaveYourChanges'] = 'Guardar Cambios';
$Context->Dictionary['Text'] = 'Texto';
$Context->Dictionary['EnterYourDiscussionTopic'] = 'Introduce el tema de la conversaci&oacute;n';
$Context->Dictionary['EnterYourComments'] = 'Introduce tus comentarios';
$Context->Dictionary['StartYourDiscussion'] = 'Empieza tu conversaci&oacute;n';
$Context->Dictionary['ShowAll'] = 'Mostrar todos';
$Context->Dictionary['DiscussionIndex'] = 'Opciones de Lista de conversaciones';
$Context->Dictionary['JumpToLastReadComment'] = 'Saltar al &uacute;ltimo comentario cuando se haga click en el tema de la conversaci&oacute;n';
$Context->Dictionary['NoDiscussionsFound'] = 'No se encontraron conversaciones';
$Context->Dictionary['RegistrationManagement'] = 'Registro';
$Context->Dictionary['NewMemberRole'] = 'Rol asociado a nuevas solicitudes de alta';
$Context->Dictionary['NewMemberRoleNotes'] = 'Cuando alguien solicita darse de alta en el foro, este es el rol que se le asigna. Si ese rol tiene los permisos adecuados, podr&iacute; tener acceso inmediatamente.';
$Context->Dictionary['RegistrationChangesSaved'] = 'Tus cambios al proceso de registro han sido guardados con &eacute;xito.';
$Context->Dictionary['ClickHereToContinue'] = 'Click aqu&iacute; para continuar';
$Context->Dictionary['RegistrationAccepted'] = 'Registro aceptado.';
$Context->Dictionary['RegistrationPendingApproval'] = 'Registro pendiente de aprobraci&oacute;n por el administrador.';
$Context->Dictionary['Applicant'] = 'Solicitante';
$Context->Dictionary['ThankYouForInterest'] = 'Gracias por tu inter&eacute;s!';
$Context->Dictionary['ApplicationWillBeReviewed'] = 'Es posible que tu solicitud de ingreso tenga que ser revisada manualmente. Si es aceptada, se te informar&aacute; mediante correo electr&oacute;nico.';
$Context->Dictionary['ApplicationComplete'] = '&iexcl;Solicitud completa!';
$Context->Dictionary['AccountChangeNotification'] = '- Notificación de Cambio de Cuenta';
$Context->Dictionary['PasswordResetRequest'] = 'Solicitud de Cambio de Contrase&ntilde;a';
$Context->Dictionary['LanguageManagement'] = 'Idiomas';
$Context->Dictionary['LanguageChangesSaved'] = 'El idioma ha sido cambiado con &eacute;xito.';
$Context->Dictionary['ChangeLanguage'] = 'Elige un idioma';
$Context->Dictionary['ChangeLanguageNotes'] = 'Si tu idioma no aparece aqu&iacute;, puedes <a href="http://vanillaforums.org/addons">descargar otros idiomas</a>.';
$Context->Dictionary['CloseThisDiscussion'] = 'Cerrar esta conversaci&oacute;n';
$Context->Dictionary['ReOpenThisDiscussion'] = 'Reabrir esta conversaci&oacute;n';
$Context->Dictionary['MakeThisDiscussionUnsticky'] = 'Liberar esta conversaci&oacute;n';
$Context->Dictionary['MakeThisDiscussionSticky'] = 'Fijar esta conversaci&oacute;n';
$Context->Dictionary['HideThisDiscussion'] = 'Eliminar esta conversaci&oacute;n';
$Context->Dictionary['UnhideThisDiscussion'] = 'Recuperar esta conversaci&oacute;n';
$Context->Dictionary['HideDiscussionLC'] = 'eliminar esta conversaci&oacute;n';
$Context->Dictionary['UnhideDiscussionLC'] = 'recuperar esta conversaci&oacute;n';

// Warnings
$Context->Dictionary['ErrOpenDirectoryExtensions'] = 'No se puede abrir el directorio de extensiones. Aseg&uacute;rate que PHP tiene acceso de lectura al directorio //1 .';
$Context->Dictionary['ErrOpenDirectoryThemes'] = 'No se puede abrir el directorio de temas. Aseg&uacute;rate que PHP tiene acceso de lectura al directorio //1 .';
$Context->Dictionary['ErrOpenDirectoryStyles'] = 'No se puede abrir el directorio de estilos. Aseg&uacute;rate que PHP tiene acceso de lectura al directorio //1 .';
$Context->Dictionary['ErrReadExtensionDefinition'] = 'Ocurri&oacute; un error al intentar leer la definicion de extensi&oacute;n desde';
$Context->Dictionary['ErrReadFileExtensions'] = 'No se puede leer el fichero de extensiones:';
$Context->Dictionary['ErrOpenFile'] = 'El fichero no se pudo abrir. Aseg&uacute;rate que PHP tiene acceso de lectura al fichero //1 .';
$Context->Dictionary['ErrWriteFile'] = 'El fichero no se pudo escribir.';
$Context->Dictionary['ErrEmailSubject'] = 'Debes escribir un tema.';
$Context->Dictionary['ErrEmailRecipient'] = 'Debes escribir al menos un destinatario.';
$Context->Dictionary['ErrEmailFrom'] = 'Debes definir el email del remitente.';
$Context->Dictionary['ErrEmailBody'] = 'Debes escribir el cuerpo del email.';
$Context->Dictionary['ErrCategoryNotFound'] = 'La categor&iacute;a pedida no se pudo encontrar.';
$Context->Dictionary['ErrCategoryReplacement'] = 'Debes escoger una categor&iacute;a de reemplazo.';
$Context->Dictionary['ErrCommentNotFound'] = 'El comentario pedido no se pudo encontrar.';
$Context->Dictionary['ErrDiscussionID'] = 'No se proporcion&oacute; un identificador de conversaci&oacute;n.';
$Context->Dictionary['ErrCommentID'] = 'No se proporcion&oacute; un identificador de comentario.';
$Context->Dictionary['ErrPermissionComments'] = 'No tienes permiso para administrar comentarios.';
$Context->Dictionary['ErrWhisperInvalid'] = 'No se ha podico encontrar a la persona destinataria del susurro.';
$Context->Dictionary['ErrDiscussionNotFound'] = 'La conversaci&oacute;n solicitada no se pudo encontrar.';
$Context->Dictionary['ErrSelectCategory'] = 'Debes seleccionar una categor&iacute;a para esta conversaci&oacute;n.';
$Context->Dictionary['ErrPermissionEditComments'] = "No puedes editar los comentarios de otra persona.";
$Context->Dictionary['ErrPermissionDiscussionEdit'] = 'La conversaci&oacute;n no se modific&oacute; porque, o bien no existe, o no tienes permisos administrativos en esta categor&iacute;a.';
$Context->Dictionary['ErrRoleNotFound'] = 'El rol pedido no se pudo encontrar.';
$Context->Dictionary['ErrPermissionInsufficient'] = 'No tienes suficientes privilegios para realizar esta solicitud.';
$Context->Dictionary['ErrSearchNotFound'] = 'La b&uacute;squeda solicitada no pudo encontrarse.';
$Context->Dictionary['ErrSearchLabel'] = 'Debes proporcionar una etiqueta para tu b&uacute;squeda. podr&aacute;s entonces hacer click en la etiqueta de b&uacute;squeda para realizarla.';
$Context->Dictionary['ErrRoleNotes'] = 'Debes proporcionar notas acerca del cambio de rol.';
$Context->Dictionary['ErrOldPasswordBad'] = 'La contrase&ntilde;a antigua introducida es incorrecta.';
$Context->Dictionary['ErrNewPasswordMatchBad'] = 'La confirmaci&oacute;n de la nueva contrase&ntilde;a no coincide.';
$Context->Dictionary['ErrPasswordsMatchBad'] = 'Las contrase&ntilde;s introducidas no coinciden.';
$Context->Dictionary['ErrAgreeTOS'] = 'Debes aceptar los t&eacute;rminos del servicio.';
$Context->Dictionary['ErrUsernameTaken'] = 'Ese nombre ya est&aacute; en uso por otra persona.';
$Context->Dictionary['ErrUserNotFound'] = 'No se ha podido encontrar a la persona requerida.';
$Context->Dictionary['ErrRemoveUserStyle'] = 'No se ha podido eliminar a esa persona porque ha creado alg&uacute;n estilo.';
$Context->Dictionary['ErrRemoveUserComments'] = 'No se ha podido eliminar a esa persona porque ha escrito comentarios en alguna conversaci&oacute;n.';
$Context->Dictionary['ErrRemoveUserDiscussions'] = 'No se ha podido eliminar a esa persona porque ha creado alguna conversaci&oacute;n.';
$Context->Dictionary['ErrInvalidUsername'] = 'El nombre no es v&aacute;lido.';
$Context->Dictionary['ErrInvalidPassword'] = 'La contrase&ntilde;a no es v&aacute;lida.';
$Context->Dictionary['ErrAccountNotFound'] = 'No se ha podido encontrar una cuenta con el nombre introducido.';
$Context->Dictionary['ErrPasswordRequired'] = 'Debes introducir una nueva contrase&ntilde;a.';
$Context->Dictionary['ErrUserID'] = 'No se ha introducido un identificador.';
$Context->Dictionary['ErrPermissionUserSettings'] = 'No tienes permisos para cambiar la configuraci&oacute;n de esta persona.';
$Context->Dictionary['ErrSpamComments'] = 'Has enviado //1 comentarios en //2 segundos. Se ha activado un filtro de spam en tu cuenta. Debes esperar por lo menos //3 segundos para enviar otro.';
$Context->Dictionary['ErrSpamDiscussions'] = 'Has enviado //1 conversaciones en //2 segundos. Se ha activado un filtro de spam en tu cuenta. Debes esperar por lo menos //3 segundos para empezar otra conversaci&oacute;n.';
$Context->Dictionary['ErrUserCombination'] = 'La combinaci&oacute;n de nombre y contrase&ntilde;a no pudo ser encontrada.';
$Context->Dictionary['ErrNoLogin'] = 'No tienes permiso para entrar en la p&aacute;gina.';
$Context->Dictionary['ErrPasswordResetRequest'] = 'hubo un error en la validaci&oacute;n de cambio de contrase&ntilde;a. Aseg&uacute;rate de copiar toda la url de tu email.';
$Context->Dictionary['ErrSignInToDiscuss'] = 'No puedes participar en las conversaciones porque no te has identificado.';
$Context->Dictionary['ErrPermissionCommentEdit'] = 'No tienes suficientes permisos para editar el comentario seleccionado.';
$Context->Dictionary['ErrRequiredInput'] = 'Debes introducir un valor para la entrada //1.';
$Context->Dictionary['ErrInputLength'] = '//1 es //2 caracteres demasiado larg@.';
$Context->Dictionary['ErrImproperFormat'] = 'No has introducido un valor con el formato correcto para ';
$Context->Dictionary['ErrOpenDirectoryLanguages'] = 'No se pudo abrir el directorio de idiomas. Aseg&uacute;rate que PHP tiene acceso de lectura para el directorio //1.';
$Context->Dictionary['ErrPermissionAddComments'] = 'No tienes permisos para a&ntilde;adir comentarios a las conversaciones.';
$Context->Dictionary['ErrPermissionStartDiscussions'] = 'No tienes permisos para empezar conversaciones.';

$Context->Dictionary['Warning'] = 'Atenci&oacute;n!';
$Context->Dictionary['ApplicationTitles'] = 'Identificaci&oacute;n del foro';
$Context->Dictionary['ApplicationTitle'] = 'Nombre de la Aplicaci&oacute;n';
$Context->Dictionary['BannerTitle'] = 'Nombre del Foro';
$Context->Dictionary['ApplicationTitlesNotes'] = 'El nombre de la aplicaci&oacute;n aparece en la barra de t&iacute;tulo de tu navegador. El nombre del foro aparece encima de las pesta&ntilde;as del men&uacute;. El nombre del foro admite HTML.';
$Context->Dictionary['CountsTitle'] = 'Listados de conversaciones y Panel de Control';
$Context->Dictionary['DiscussionsPerPage'] = 'Conversaciones por p&aacute;gina';
$Context->Dictionary['CommentsPerPage'] = 'Comentarios por p&aacute;gina';
$Context->Dictionary['SearchResultsPerPage'] = 'Resultados de b&uacute;squeda por p&aacute;gina';
$Context->Dictionary['MaxBookmarksInPanel'] = 'M&aacute;x. favoritos en el panel';
$Context->Dictionary['MaxPrivateInPanel'] = 'M&aacute;x. privados en el panel';
$Context->Dictionary['MaxBrowsingHistoryInPanel'] = 'M&aacute;x. hist&oacute;rico en panel';
$Context->Dictionary['MaxDiscussionsInPanel'] = 'M&aacute;x. conversaciones propias en panel';
$Context->Dictionary['MaxSavedSearchesInPanel'] = 'M&aacute;x. b&uacute;squedas guardadas en panel';
$Context->Dictionary['CountsNotes'] = 'Los valores escogidos aqu&iacute; limitan el m&aacute;ximo de conversaciones o comentarios mostrados en la lista de conversaciones, p&aacute;gina de comentarios y en el panel de control.';
$Context->Dictionary['SpamProtectionTitle'] = 'Protecci&oacute;n de Spam';
$Context->Dictionary['MaxCommentLength'] = 'M&aacute;x. caracteres en los comentarios';
$Context->Dictionary['MaxCommentLengthNotes'] = "Aunque la base de datos puede transferir y almacenar tanta informaci&oacute;n como permita la memoria del servidor, es una buena idea mantener una longitud m&aacute;xima de los comentarios razonable.";
$Context->Dictionary['XDiscussionsYSecondsZFreeze'] = 'Los miembros no pueden enviar m&aacute;s de //1 conversaciones en //2 segundos o su cuenta ser&aacute; congelada por //3 segundos.';
$Context->Dictionary['XCommentsYSecondsZFreeze'] = 'Los miembros no pueden enviar m&aacute;s de //1 comentarios en //2 segundos o su cuenta ser&aacute; congelada por //3 segundos.';
$Context->Dictionary['LogAllIps'] = 'Registrar &amp; monitorizar todas las direcciones IP';
$Context->Dictionary['SupportContactTitle'] = 'Contacto de Soporte del Foro';
$Context->Dictionary['SupportName'] = 'Nombre de Soporte';
$Context->Dictionary['SupportEmail'] = 'Direcci&oacute;n de email de soporte';
$Context->Dictionary['SupportContactNotes'] = 'Todos los emails que se env&iacute;en desde el sistema tendr&aacute;n este nombre y direcci&oacute;n de email.';
$Context->Dictionary['DiscussionLabelsTitle'] = 'Etiquetas de conversaci&oacute;n';
$Context->Dictionary['LabelPrefix'] = 'Prefijo de etiqueta';
$Context->Dictionary['LabelSuffix'] = 'Sufijo de etiqueta';
$Context->Dictionary['WhisperLabel'] = 'Etiqueta Privado';
$Context->Dictionary['StickyLabel'] = 'Etiqueta Fija';
$Context->Dictionary['SinkLabel'] = 'Etiqueta Hundir';
$Context->Dictionary['ClosedLabel'] = 'Etiqueta Cerrado';
$Context->Dictionary['HiddenLabel'] = 'Etiqueta Eliminada';
$Context->Dictionary['BookmarkedLabel'] = 'Etiqueta Favorito';
$Context->Dictionary['WebPathToVanilla'] = 'Ruta Web a Vanilla';
$Context->Dictionary['CookieDomain'] = 'Dominio de Cookies';
$Context->Dictionary['WebPathNotes'] = 'La Ruta Web a Vanilla debe ser la ruta completa tal y como la escribir&iacute;as en un navegador web. Por ejemplo: http://www.tudominio.com/vanilla/';
$Context->Dictionary['CookieSettingsNotes'] = 'El dominio de cookies es el que quieres asignar a las cookies de Vanilla. T&iacute;picamente, el dominio de cookies ser&aacute; algo as&iacute; como www.tudominio.com. Las cookies se pueden definir en una ruta particular en tu website usando el ajuste "ruta de cookie". (PISTA: Si quieres que las cookies de Vanilla afecten a todos los subdominios de tu dominio, usa ".tudominio.com" como dominio de cookie)';
$Context->Dictionary['AllowNameChange'] = 'Permitir a todo el mundo cambiar su propio nombre';
$Context->Dictionary['AllowPublicBrowsing'] = 'Permitir a los no miembros navegar el foro';
$Context->Dictionary['UseCategories'] = 'Categorizar las conversaciones';
$Context->Dictionary['DiscussionLabelsNotes'] = "Las etiquetas de conversaci&oacute;n aparecer&aacute;n delante del tema de la conversaci&oacute;n en la p&aacute;gina principal de conversaciones. El prefijo y sufijo de etiquetas aparecer&aacute;n a cada lado de la etiqueta de conversaci&oacute;n. Si la etiqueta de conversaci&oacute;n est&aacute; vac&iacute;a, el prefijo y sufijo no aparecer&aacute;n.";
$Context->Dictionary['ForumOptions'] = 'Opciones del Foro';
$Context->Dictionary['GlobalApplicationChangesSaved'] = 'Tus cambios se han guardado con &eacute;xito';
$Context->Dictionary['ApprovedMemberRole'] = 'Rol asociado a altas aprobadas';
$Context->Dictionary['ApprovedMemberRoleNotes'] = 'Cuando una solicitud de ingreso es aprobada por un administrador (si la aprobaci&oacute;n es necesaria), este es el rol que se le asignar&aacute;.';
$Context->Dictionary['NewMemberWelcomeAboard'] = 'Ya tienes permiso de acceso. Si&eacute;ntete como en tu casa.';
$Context->Dictionary['RoleCategoryNotes'] = 'Selecciona las categor&iacute;as a las que este rol tendr&aacute; acceso';
$Context->Dictionary['DebugTitle'] = 'Eliminaci&oacute;n de errores de Vanilla';
$Context->Dictionary['DebugDescription'] = 'Si tienes suficientes permisos, puedes activar el modo de eliminaci/oacute;n de errores de Vanilla y mostrar todas las consultas de una p&aacute;gina despu&eacute;s de la ejecuci&oacute;n de la p&aacute;gina. Ser&aacute;s la &uacute;nica persona que ver&aacute; estos datos. Usa esta p&aacute;gina para activar y desactivar el modo de eliminaci&oacute;n de errores.';
$Context->Dictionary['CurrentApplicationMode'] = 'Vanilla est&aacute; en estos momentos en el modo: ';
$Context->Dictionary['DEBUG'] = 'Eliminaci&oacute;n de errores';
$Context->Dictionary['RELEASE'] = 'Publicaci&oacute;n';
$Context->Dictionary['SwitchApplicationMode'] = 'Haga click aqu&iacute; para cambiar el modo de la aplicaci&oacute;n';
$Context->Dictionary['BackToApplication'] = 'Haga click aqu&iacute; para volver a Vanilla';
$Context->Dictionary['ErrReadFileSettings'] = 'Hubo un error al intentar leer el fichero de configuraci&oacute;n: ';
$Context->Dictionary['CookiePath'] = 'Ruta de Cookie';
$Context->Dictionary['Wait'] = 'Espera';
$Context->Dictionary['OldPostDateFormatCode'] = 'M jS Y';
$Context->Dictionary['XDayAgo'] = 'hace //1 dia';
$Context->Dictionary['XDaysAgo'] = 'hace //1 d&iacute;as';
$Context->Dictionary['XHourAgo'] = 'hace //1 hora';
$Context->Dictionary['XHoursAgo'] = 'hace //1 horas';
$Context->Dictionary['XMinuteAgo'] = 'hace //1 minuto';
$Context->Dictionary['XMinutesAgo'] = 'hace //1 minutos';
$Context->Dictionary['XSecondAgo'] = 'hace //1 segundo';
$Context->Dictionary['XSecondsAgo'] = 'hace //1 segundos';
$Context->Dictionary['nothing'] = 'nada';
$Context->Dictionary['EnableWhispers'] = 'Activar susurros';
$Context->Dictionary['ExtensionFormNotes'] = 'Las extensiones se usan para a&ntilde;adir nuevas funciones a Vanilla. M&aacute;s abajo est&aacute;n listadas todas las extensiones que est&aacute;n instaladas. Para activar una extensi&oacute;n, pincha en la casilla de verificaci&oacute;n al lado del nombre de la extensi&oacute;n. <a href="http://lussumo.com/addons/">M&aacute;s extensiones de Lussumo Vanilla</a>.';
$Context->Dictionary['EnabledExtensions'] = 'Extensiones Activadas';
$Context->Dictionary['DisabledExtensions'] = 'Extensiones Desactivadas';
$Context->Dictionary['ErrExtensionNotFound'] = 'La extensi&oacute;n requerida no se pudo encontrar.';
$Context->Dictionary['UpdatesAndReminders'] = 'Actualizaciones';
$Context->Dictionary['UpdateCheck'] = 'Comprobar actualizaciones';
$Context->Dictionary['UpdateCheckNotes'] = 'Vanilla est&aacute; siendo actualizado constantemente, al irse solucionando los problemas descubiertos y al a&ntilde;adirse (o eliminarse) funciones por la comunidad de desarrollo. Para asegurarte que tu instalaci&oacute;n de Vanilla esta al d&iacute;a y es segura, es importante comprobar las actualizaciones regularmente.';
$Context->Dictionary['CheckForUpdates'] = 'Comprobar actualizaciones ahora';
$Context->Dictionary['ErrUpdateCheckFailure'] = 'Hubo un error al obtener informaci&oacute;n de Lussumo acerca de la &uacute;ltima actualizaci&oacute;n de Vanilla. Por favor, int&eacute;ntelo de nuevo m&aacute;s tarde.';
$Context->Dictionary['PleaseUpdateYourInstallation'] = '<strong>ATENCI&oacute;N:</strong> Tu instalaci&oacute;n de Vanilla es //1, pero <span class="Highlight">la versi&oacute;n m&aacute;s reciente disponible de Vanilla es //2</span>. Por favor, actualiza tu instalaci&oacute;n inmediatamente descargando la &uacute;ltima versi&oacute;n desde <a href="http://getvanilla.com">http://getvanilla.com</a>.';
$Context->Dictionary['YourInstallationIsUpToDate'] = 'Tu instalaci&oacute;n de Vanilla est&aacute; al d&iacute;a. Por favor compru&eacute;balo de nuevo pronto!';
$Context->Dictionary['ErrPermissionHideDiscussions'] = 'No tienes permisos para eliminar conversaciones.';
$Context->Dictionary['ErrPermissionCloseDiscussions'] = 'No tienes permisos para cerrar conversaciones.';
$Context->Dictionary['ErrPermissionStickDiscussions'] = 'No tienes permisos para hacer una conversaci&oacute;n popular.';
$Context->Dictionary['CategoryReorderNotes'] = 'Arrastra y suelta las categor&iacute;as para reordenarlas. El nuevo orden ser&aacute; guardado autom&aacute;ticamente. Si una categor&iacute;a aparece en gris es que est&aacute; bloqueada para tu rol.';
$Context->Dictionary['RoleReorderNotes'] = 'Arrastra y suelta los roles para reordenarlos. El nuevo orden ser&aacute; guardado autom&aacute;ticamente. El rol <i>Unauthenticated</i> es un rol especial que se aplica a quienes que navegan el foro sin tener registro, y no puede borrarse';
$Context->Dictionary['PERMISSION_CHECK_FOR_UPDATES'] = 'Comprobar actualizaciones';
$Context->Dictionary['PERMISSION_SIGN_IN'] = 'Puede registrarse';
$Context->Dictionary['PERMISSION_ADD_COMMENTS'] = 'A&ntilde;adir comentarios';
$Context->Dictionary['PERMISSION_ADD_COMMENTS_TO_CLOSED_DISCUSSION'] = 'A&ntilde;adir comentarios a conversaciones cerradas';
$Context->Dictionary['PERMISSION_START_DISCUSSION'] = 'Empezar conversaciones';
$Context->Dictionary['PERMISSION_HTML_ALLOWED'] = 'HTML &amp; Im&aacute;genes permitidas';
$Context->Dictionary['PERMISSION_IP_ADDRESSES_VISIBLE'] = 'Direcciones IP visibles';
$Context->Dictionary['PERMISSION_APPROVE_APPLICANTS'] = 'Aprobar Solicitudes de Ingreso';
$Context->Dictionary['PERMISSION_MANAGE_REGISTRATION'] = 'Configuraci&oacute;n de registro';
$Context->Dictionary['PERMISSION_EDIT_USERS'] = 'Editar cualquier persona';
$Context->Dictionary['PERMISSION_CHANGE_USER_ROLE'] = 'Cambiar los roles';
$Context->Dictionary['PERMISSION_SORT_ROLES'] = 'Ordenar roles';
$Context->Dictionary['PERMISSION_ADD_ROLES'] = 'A&ntilde;adir roles nuevos';
$Context->Dictionary['PERMISSION_EDIT_ROLES'] = 'Editar roles existentes';
$Context->Dictionary['PERMISSION_REMOVE_ROLES'] = 'Eliminar roles existentes';
$Context->Dictionary['PERMISSION_STICK_DISCUSSIONS'] = 'Hacer conversaciones populares';
$Context->Dictionary['PERMISSION_HIDE_DISCUSSIONS'] = 'Eliminar conversaciones';
$Context->Dictionary['PERMISSION_CLOSE_DISCUSSIONS'] = 'Cerrar conversaciones';
$Context->Dictionary['PERMISSION_EDIT_DISCUSSIONS'] = 'Editar cualquier conversaci&oacute;n';
$Context->Dictionary['PERMISSION_MOVE_ANY_DISCUSSIONS'] = 'Mover cualquier conversaci&oacute;n';
$Context->Dictionary['PERMISSION_HIDE_COMMENTS'] = 'Eliminar comentarios';
$Context->Dictionary['PERMISSION_EDIT_COMMENTS'] = 'Editar cualquier comentario';
$Context->Dictionary['PERMISSION_ADD_CATEGORIES'] = 'A&ntilde;adir categor&iacute;as';
$Context->Dictionary['PERMISSION_EDIT_CATEGORIES'] = 'Editar categor&iacute;as';
$Context->Dictionary['PERMISSION_REMOVE_CATEGORIES'] = 'Eliminar categor&iacute;as';
$Context->Dictionary['PERMISSION_SORT_CATEGORIES'] = 'Ordenar categor&iacute;as';
$Context->Dictionary['PERMISSION_VIEW_HIDDEN_DISCUSSIONS'] = 'Mostrar categor&iacute;as eliminadas';
$Context->Dictionary['PERMISSION_VIEW_HIDDEN_COMMENTS'] = 'Mostrar comentarios eliminados';
$Context->Dictionary['PERMISSION_VIEW_ALL_WHISPERS'] = 'Mostrar todos los susurros';
$Context->Dictionary['PERMISSION_CHANGE_APPLICATION_SETTINGS'] = 'Cambiar configuraci&oacute;n de la aplicaci&oacute;n';
$Context->Dictionary['PERMISSION_MANAGE_EXTENSIONS'] = 'Extensiones';
$Context->Dictionary['PERMISSION_MANAGE_LANGUAGE'] = 'Cambiar idioma';
$Context->Dictionary['PERMISSION_MANAGE_STYLES'] = 'Administrar estilos';
$Context->Dictionary['PERMISSION_MANAGE_THEMES'] = 'Administrar temas';
$Context->Dictionary['PERMISSION_RECEIVE_APPLICATION_NOTIFICATION'] = '- Notificación por email de nuevas solicitudes';
$Context->Dictionary['PERMISSION_ALLOW_DEBUG_INFO'] = 'Ver informaci&oacute;n de eliminaci&oacute;n de errores';
$Context->Dictionary['PERMISSION_DATABASE_CLEANUP'] = 'Permitir usar la extensi&oacute;n de limpieza';
$Context->Dictionary['PERMISSION_ADD_ADDONS'] = 'Puede a&ntilde;adir extensiones';
$Context->Dictionary['NoEnabledExtensions'] = 'No hay ninguna extension activada.';
$Context->Dictionary['NoDisabledExtensions'] = 'No hay extensiones desactivadas.';
$Context->Dictionary['NA'] = 'n/a';
$Context->Dictionary['AboutExtensionPage'] = '<strong>La p&aacute;gina de extensiones</strong><br />esta p&aacute;gina puede es utilizada por los creadores de extensiones para programar p&aacute;ginas de extensiones personalizadas en Vanilla. est&aacute;s viendo la p&aacute;gina por defecto y probablemente llegaste aqu&iacute; por accidente o por un error en una extensi&oacute;n.';
$Context->Dictionary['NewApplicant'] = 'Nueva solicitud de ingreso';
$Context->Dictionary['PERMISSION_SINK_DISCUSSIONS'] = 'Hundir conversaciones';
$Context->Dictionary['MakeThisDiscussionSink'] = 'Hundir esta conversaci&oacute;n';
$Context->Dictionary['MakeThisDiscussionUnSink'] = 'Flotar esta conversaci&oacute;n';
$Context->Dictionary['ConfirmUnSink'] = '&iquest;De verdad quieres eliminar la etiqueta de Hundida de esta conversaci&oacute;n?';
$Context->Dictionary['ConfirmSink'] = '&iquest;De verdad quieres etiquetar esta conversaci&oacute;n como Hundida?';
$Context->Dictionary['ErrPermissionSinkDiscussions'] = 'No tienes permisos para hundir conversaciones';
$Context->Dictionary['YourCommentsWillBeWhisperedToX'] = 'Tus comentarios ser&aacute;n susurrados a  //1';
$Context->Dictionary['SMTPHost'] = 'Host SMTP';
$Context->Dictionary['SMTPUser'] = 'Identificador SMTP';
$Context->Dictionary['SMTPPassword'] = 'Contrase&ntilde;a SMTP';
$Context->Dictionary['SMTPSettingsNotes'] = 'Por defecto, Vanilla usar&aacute; el servidor de correo que est&eacute; instalado en el servidor donde reside Vanilla. Si, por alguna raz&oacute;n, deseas usar otro servidor de correo SMTP para enviar los correos salientes, puedes configurarlo con estas tres opciones. Si no quieres usar un servidor SMTP, deja estos campos en blanco.';
$Context->Dictionary['PagelistNextText'] = 'Siguiente';
$Context->Dictionary['PagelistPreviousText'] = 'Anterior';
$Context->Dictionary['EmailSettings'] = 'Configuraci&oacute;n de Email';
$Context->Dictionary['UpdateReminders'] = 'Recordatorios de Actualizaci&oacute;n';
$Context->Dictionary['UpdateReminderNotes'] = 'Somos olvidadizos as&iacute; que Vanilla se puede configurar para recordarte la comprobaci&oacute;n de actualizaciones. Cualquier miembro que tenga permisos para comprobar actualizaciones ver&aacute; este recordatorio, que aparecer&aacute; encima de la lista de conversaciones.';
$Context->Dictionary['ReminderLabel'] = 'Comprobar actualizaciones';
$Context->Dictionary['Never'] = 'Nunca';
$Context->Dictionary['Weekly'] = 'Semanalmente';
$Context->Dictionary['Monthly'] = 'Mensualmente';
$Context->Dictionary['Quarterly'] = 'Trimestralmente (3 meses)';
$Context->Dictionary['ReminderChangesSaved'] = 'Tu configuraci&oacute;n de recordatorio ha sido guardada con &eacute;xito.';
$Context->Dictionary['NeverCheckedForUpdates'] = 'Todav&iacute;a no has comprobado las actualizaciones de Vanilla.';
$Context->Dictionary['XDaysSinceUpdateCheck'] = 'Han pasado //1 d&iacute;as desde la &uacute;ltima vez que comprobaste las actualizaciones de Vanilla.';
$Context->Dictionary['CheckForUpdatesNow'] = 'Haz click aqu&iacute; para comprobar las actualizaciones ahora';
$Context->Dictionary['ManageThemeAndStyle'] = 'Temas y estilos';
$Context->Dictionary['ThemeChangesSaved'] = 'Tus cambios se han guardado con &eacute;xito';
$Context->Dictionary['ThemeAndStyleNotes'] =  "Los Temas y Estilos sirven para cambiar las estructura y apariencia de Vanilla respectivamente. Para m&aacute;s temas y estilos, o para aprender c&oacute;mo hacer los tuyos, <a href=\"http://lussumo.com/addons/\">visita el directorio de a&ntilde;adidos de Vanilla</a>.";
$Context->Dictionary['ThemeLabel'] = 'Temas disponibles en tu instalaci&oacute;n de Vanilla';
$Context->Dictionary['StyleLabel'] = 'Estilos disponibles para el tema seleccionado';
$Context->Dictionary['ApplyStyleToAllUsers'] = 'Aplicar este estilo a todo el mundo';
$Context->Dictionary['ThemeAndStyleManagement'] = 'Temas y Estilos';
$Context->Dictionary['Check'] = 'Marcar: ';
$Context->Dictionary['All'] = 'Todos';
$Context->Dictionary['None'] = 'Ninguno';
$Context->Dictionary['Simple'] = 'Simple';
$Context->Dictionary['ErrorFopen'] = "Hubo un error al intentar obtener informaci&oacute;n de un origen de datos externo (\\1).";
$Context->Dictionary['ErrorFromPHP'] = " Este es el mensaje de PHP: \\1";
$Context->Dictionary['InvalidHostName'] = 'El nombre de host introducido no es v&aacute;lido: \\1';
$Context->Dictionary['WelcomeToVanillaGetSomeAddons'] = '<strong>&iexcl;Vanilla te da la bienvenida!</strong>
<br />Como podr&aacute;s observar, el foro inicialmente es algo minimalista. Deber&iacute;as aderezarlo con alguna <a href="http://lussumo.com/addons/">extensi&oacute;n</a>.';
$Context->Dictionary['RemoveThisNotice'] = 'Eliminar este mensaje';
$Context->Dictionary['OtherSettings'] = 'Otros ajustes';
$Context->Dictionary['ChangesSaved'] = 'Tus cambios se guardaron con &eacute;xito';
$Context->Dictionary['DiscussionType'] = 'Tipo de conversaci&oacute;n';
// Added for Vanilla 1.1 on 2007-02-20
$Context->Dictionary['ErrPostBackKeyInvalid'] = 'There was a problem authenticating your post information.';
$Context->Dictionary['ErrPostBackActionInvalid'] = 'Your post information was not be defined properly.';
$Context->Dictionary['ErrPostBackKeySignOutInvalid'] = 'Can you please confirm you would like to sign out?';
// Moved from settings.php
$Context->Dictionary['TextWhispered'] = 'Privada';
$Context->Dictionary['TextSticky'] = 'Fija';
$Context->Dictionary['TextClosed'] = 'Cerrada';
$Context->Dictionary['TextHidden'] = 'Eliminada';
$Context->Dictionary['TextSink'] = 'Hundida';
$Context->Dictionary['TextBookmarked'] = 'Marcada';
$Context->Dictionary['TextPrefix'] = '[';
$Context->Dictionary['TextSuffix'] = ']';
// Added for new update checker
$Context->Dictionary['CheckingForUpdates'] = 'Buscando actualizaciones...';
$Context->Dictionary['ApplicationStatusGood'] = 'No hay actualizaciones disponibles para Vanilla.';
$Context->Dictionary['ExtensionStatusGood'] = 'No hay actualizaciones disponibles para esta extensi&oacute;n.';
$Context->Dictionary['ExtensionStatusUnknown'] = 'No ha sido posible encontrar esta extensi&oacute;n. <a href="http://lussumo.com/docs/doku.php?id=vanilla:administrators:updatecheck">Averigua por qu&eacute;</a>';
$Context->Dictionary['NewVersionAvailable'] = 'Est&aacute; disponible la versi&oacute;n \\1. <a href="\\2">Descargar</a>';
// Altered for new applicant management screen
$Context->Dictionary['ApproveForMembership'] = 'Aprobar';
$Context->Dictionary['DeclineForMembership'] = 'Rechazar';
$Context->Dictionary['ApplicantsNotes'] = 'Usa este formulario para aprobar o rechazar la peticiones de alta.';
$Context->Dictionary['NoApplicants'] = 'Ahora mismo no hay ninguna petici&oacute;n de alta pendiente de revisi&oacute;n.';
// Added for ajax/sortroles.php
$Context->Dictionary['ErrPermissionSortRoles'] = 'No tienes permisos para ordenar los roles';
$Context->Dictionary['ErrPermissionSortCategories'] = 'No tienes permisos para ordenar las categor&iacute;as';
//Added for Vanilla 1.1.5 on 2008-05-25
$Context->Dictionary['EditYourDiscussion'] = 'Modifica tu conversaci&oacute;n';

/* Please do not remove or alter this definition */
$Context->Dictionary['PanelFooter'] = '<p id="AboutVanilla"><a href="http://getvanilla.com">Vanilla '.APPLICATION_VERSION.'</a> es un producto de <a href="http://lussumo.com">Lussumo</a>. Puedes obtener m&aacute;s informaci&oacute;n en su <a href="http://vanillaforums.org">página web</a>.</p>';

?>