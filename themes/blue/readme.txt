===============================
THEME INSTALLATION INSTRUCTIONS
===============================

Your new theme files should be placed within their own folder in the themes
directory of Vanilla. The path to your new theme should be as follows:

/path/to/vanilla/themes/theme_name/

Once you have placed your theme in the correct place, you can apply the theme
using the **Themes & Styles** form on the settings tab.