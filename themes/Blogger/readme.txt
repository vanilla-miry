Blogger 1.5 By Sam El
http://www.ventswap.com

Thank you for downloading Blogger!
Please give me a credit and leave my link at the bottom alone! :)
And feel free to contact me at sjeeps@gmail.com with any comments or suggestions.

Thanks again!

Tested and looks good in:

FF 2.0.0.7
IE 7
Safari 3.0

Version History:
1.0 - First release
1.1 - Fixed css bug (gab between .Discussionoverview and .Topics when discussion overview extension is enabled)
1.2 - CSS validated with 0 errors and warnings, CSS fixes to accommodate some add-ons (thanks to jimw).
1.3 - Fixed IE7 gab at the end of the page.
1.4 - Updated for Vanilla 1.1.3, various css updates and fixes
1.5 - Updated for Vanilla 1.1.5a


Installation:
Just upload the theme to your /theme folder, go to Settings >>Themes & Styles >> Select Blogger >> check the "Apply this style to all users" box and you're done!

Note:

Open foot.php to edit the footer links.

Enjoy!