<?php
/*
* Copyright 2010 Miriam Ruiz
* Copyright 2003 Mark O'Sullivan
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
* Contact Mark O'Sullivan at mark [at] lussumo [dot] com
*
* Description: Terms of use for the vanilla forum - should be customized by Vanilla user
*/
include('appg/settings.php');
include('conf/settings.php');
include('library/Framework/Framework.Functions.php');

/* ------------------------------------------------------------------------ */

// PhpCache - a class for caching arbitrary data
// Copyright (C) 2005-2007, Edward Eliot
// http://www.ejeliot.com/blog/77

/*
      Software License Agreement (BSD License)

      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions are met:

         * Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.
         * Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.
         * Neither the name of Edward Eliot nor the names of its contributors 
           may be used to endorse or promote products derived from this software 
           without specific prior written permission of Edward Eliot.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS" AND ANY
      EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
      DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
      ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
      SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

define('HTTP_TIMEOUT', 3); // how long to wait for a connection before aborting, CURL only
define('MAX_HTTP_REQUEST_TIME', 5); // maximum time allowed for completing URL request before aborting, CURL only
define('HTTP_USERAGENT', 'SimpleRss');

class SimpleHttp {
	var $iConnectTimeout;
	var $iRequestTimeout;
	var $sUserAgent;

	function SimpleHttp($iConnectTimeout = HTTP_TIMEOUT, $iRequestTimeout = MAX_HTTP_REQUEST_TIME, $sUserAgent = HTTP_USERAGENT)
	{
		$this->iConnectTimeout = $iConnectTimeout;
		$this->iRequestTimeout = $iRequestTimeout;
	}

	function Get($sUrl) { // check for curl lib, use in preference to file_get_contents if available
		if (function_exists('curl_init')) {
			// initiate session
			$oCurl = curl_init($sUrl);
			// set options
			curl_setopt($oCurl, CURLOPT_CONNECTTIMEOUT, $this->iConnectTimeout);
			curl_setopt($oCurl, CURLOPT_TIMEOUT, $this->iRequestTimeout);
			curl_setopt($oCurl, CURLOPT_USERAGENT, $this->sUserAgent);
			curl_setopt($oCurl, CURLOPT_HEADER, false);
			curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, true);
			// request URL
			$sResult = curl_exec($oCurl);
			// close session
			curl_close($oCurl);
			return $sResult;
		} else {
			ini_set('user_agent', HTTP_USERAGENT);
			// fopen_wrappers need to be enabled for this to work
			// See http://www.php.net/manual/en/function.file-get-contents.php
			if ($sResult = @file_get_contents($sUrl)) {
				return $sResult;
			}
		}
		return false;
	}
}

define('CACHE_PATH', $Configuration['APPLICATION_PATH'] . '/cache/');

class SimpleCache {
	var $sCacheSubDir;
	var $sFile;
	var $sFileLock;
	var $iCacheTime; // In seconds
	var $oCacheObject;

	function SimpleCache($sType,$sKey, $iCacheTime = 300) {
		$this->sCacheSubDir = '/'.$sType.'/';
		$this->sFile = CACHE_PATH.$this->sCacheSubDir.md5($sKey).".cache";
		$this->sFileLock = "$this->sFile.lock";
		$iCacheTime >= 10 ? $this->iCacheTime = $iCacheTime : $this->iCacheTime = 10;
	}

	function Check() {
		if (file_exists($this->sFileLock)) return true;
		return (file_exists($this->sFile) && ($this->iCacheTime == -1 || time() - filemtime($this->sFile) <= $this->iCacheTime));
	}

	function Exists() {
		return (file_exists($this->sFile) || file_exists($this->sFileLock));
	}

	function Set($vContents) {
		if (!file_exists($this->sFileLock)) {
			if (file_exists($this->sFile)) {
				copy($this->sFile, $this->sFileLock);
			}
			$oFile = fopen($this->sFile, 'w');
			fwrite($oFile, serialize($vContents));
			fclose($oFile);
			if (file_exists($this->sFileLock)) {
				unlink($this->sFileLock);
			}
		return true;
		}
		return false;
	}

	function Get() {
		if (file_exists($this->sFileLock)) {
			return unserialize(file_get_contents($this->sFileLock));
		} else {
			return unserialize(file_get_contents($this->sFile));
		}
	}

	function ReValidate() {
		touch($this->sFile);
	}
}

/* ------------------------------------------------------------------------ */

class FauxContext { // Create a faux-context, 'cos we don't need all the extra overhead
	var $Dictionary;
	function FauxContext() {
		$this->Dictionary = array();
	}
	function GetDefinition($Code) {
		if (array_key_exists($Code, $this->Dictionary)) {
			return $this->Dictionary[$Code];
		} else {
			return $Code;
		}
	}
}
$Context = new FauxContext();

header ('Content-type: text/xml; charset='.$Configuration['CHARSET']);

// LANGUAGE DICTIONARY
include($Configuration['LANGUAGES_PATH'].$Configuration['LANGUAGE'].'/definitions.php');
//include($Configuration['APPLICATION_PATH'].'conf/language.php');

$What=strtolower(ForceIncomingString('What', null)); // 'Topics', 'Blog', 'Category', 'Discussion'
$Page=(int)1;
$Feed=strtoupper(ForceIncomingString('Feed', null));
if ($Feed != 'ATOM') $Feed = 'RSS2';
$FeedTitle=urldecode(ForceIncomingString('FeedTitle', null));

if (isset($_GET['Blog']) || isset($_GET['BlogSearch'])) { // Messages in the blog
	$params = 'PostBackAction=Search&Type=Comments&Page='.$Page.'&Feed='.$Feed.'&BlogSearch=1';
	if ($FeedTitle==NULL) $FeedTitle = 'Blog';
} else if (isset($_GET['CategoryID'])) { // Discussions in a category
	$params = 'PostBackAction=Search&Type=Topics&Page='.$Page.'&Feed='.$Feed.'&CategoryID='.ForceIncomingInt('CategoryID', null);
	if ($FeedTitle==NULL) $FeedTitle = 'Category';
} else if (isset($_GET['DiscussionID'])) { // Messages in a discussion
	$params = 'PostBackAction=Search&Type=Comments&Page='.$Page.'&Feed='.$Feed.'&DiscussionID='.ForceIncomingInt('DiscussionID', null);
	if ($FeedTitle==NULL) $FeedTitle = 'Discussion';
} else { // All the topics
	$params = 'PostBackAction=Search&Type=Topics&Page='.$Page.'&Feed='.$Feed;
	if ($FeedTitle==NULL) $FeedTitle = 'All topics';
}

$url = $Configuration['BASE_URL'].'/search.php?'.$params.'&FeedTitle='.urlencode($FeedTitle);

$cache = new SimpleCache('feed', $params, 600);
if ($cache->Check()) {
	echo($cache->Get());
} else {
	$http = new SimpleHttp();
	$data = $http->Get($url);
	$cache->Set($data);
	echo $data;
}
?>