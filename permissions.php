<?php

// See: http://snipplr.com/view/5350/recursive-chmod-in-php/
function recursive_chmod($path, $filePerm=0644, $dirPerm=0755)
{
	// Check if the path exists
	if(!file_exists($path)) {
		return(FALSE);
	}
	// See whether this is a file
	if(is_file($path)) {
		// Chmod the file with our given filepermissions
		echo "chmod $path ".sprintf("%o",$filePerm)." [FILE]</br>\n";
		chmod($path, $filePerm);
		// If this is a directory...
	} elseif(is_dir($path)) {
		// Then get an array of the contents
		$foldersAndFiles = scandir($path);
		// Remove "." and ".." from the list
		$entries = array_slice($foldersAndFiles, 2);
		// Parse every result...
		foreach($entries as $entry) {
			// And call this function again recursively, with the same permissions
			recursive_chmod($path."/".$entry, $filePerm, $dirPerm);
		}
		// When we are done with the contents of the directory, we chmod the directory itself
		echo "chmod $path ".sprintf("%o",$dirPerm)." [DIR]</br>\n";
		chmod($path, $dirPerm);
	}
	// Everything seemed to work out well, return TRUE
	return(TRUE);
}

$BASEDIR='/var/www/miriamruiz.es/threads';
chmod($BASEDIR.'/conf/database.php', 0666);
chmod($BASEDIR.'/conf/extensions.php', 0666);
chmod($BASEDIR.'/conf/settings.php', 0666);
recursive_chmod($BASEDIR.'/conf/language_cache', 0666, 0777);
recursive_chmod($BASEDIR.'/cache', 0666, 0777);
?>