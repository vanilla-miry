<?php
$Configuration['FT_ALLDISCUSSIONS_ALT'] = array(
	'RSS2' => $Configuration['BASE_URL'] . 'feed.php?Feed=RSS2',
	'ATOM' => $Configuration['BASE_URL'] . 'feed.php?Feed=ATOM',
);

$Configuration['FT_BLOGFEED_ALT'] = array(
	'RSS2' => $Configuration['BASE_URL'] . 'feed.php?Blog&Feed=RSS2',
	'ATOM' => $Configuration['BASE_URL'] . 'feed.php?Blog&Feed=ATOM',
);

$Configuration['FT_ALLFEEDS_ALT'] = array(
	'RSS2' => $Configuration['BASE_URL'] . 'feed.php?Feed=RSS2',
	'ATOM' => $Configuration['BASE_URL'] . 'feed.php?Feed=ATOM',
);
?>