<?php /* this is added by ExtensionLanguageLoader extension, please do not edit this block */
@include ($Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/LanguageLoader.php');

// Custom Language Definitions
	$Context->Dictionary[ "RSS2Feed" ]
		= "<img src='images/RSS.png' class='FeedIcon' /> RSS2";
	$Context->Dictionary[ "ATOMFeed" ]
		= "<img src='images/Atom.png' class='FeedIcon' /> Atom";

	$Context->Dictionary['PanelFooter'] = '<p id="AboutVanilla">Este foro ha sido desarrollado con <a href="http://getvanilla.com">Vanilla '.APPLICATION_VERSION.'</a>. Puedes descargarte el c&oacute;digo fuente en este <a href="http://repo.or.cz/w/vanilla-miry.git">repositorio git</a>.</p>';
?>