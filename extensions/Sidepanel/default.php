<?php
/*
Extension Name: Sidepanel
Extension Url: http://lussumo.com/addons
Description: Enables you to insert everything you want to appear in the sidepanel easily (based on Statcounter by JP Mitchell)
Version: 1.0
Author: Thomas Schranz
Author Url: http://suitcase.at
*/

if(in_array($Context->SelfUrl, array(
	"index.php",
	"categories.php",
	"discussions.php",
	"comments.php",
	"search.php",
	"post.php",
	"account.php",
	"extension.php",
//	"settings.php",
))){

$content = <<< ENDCODE

<a href="http://www.miriamruiz.es/weblog"><img src="images/banner.png" alt="Blog de Miriam Ruiz" /></a>

ENDCODE;

$Panel->AddString($content,0);
}
?>