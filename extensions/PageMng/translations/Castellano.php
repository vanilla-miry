<?php
// if(isset($Menu) || defined('PAGEMNG_ISAJAX'))

	$Context->Dictionary['PageMng_NoInputValue'] = 'You must enter a value for the Tab Name input.';
	$Context->Dictionary['PageMng_AlreadyCreated'] = 'A tab of that identifier has already been created.';
	$Context->Dictionary['PageMng_FileError'] = 'PAGEMANAGER ERROR: An error occured in attempting to save your tabs.  '.
		'Please check that your file permissions are correct and verify that PageManager::CustomPageFile (%s)'.
		' is a valid file name.';
	$Context->Dictionary['PageMng_NoRoles'] = 'PAGEMANAGER ERROR: No roles were found when attempting to build tab file.  If the current '.
		'page is not settings.php please go there now to properly create the file.  If so, then, well, something\'s wrong...';

// if($Context->SelfUrl == 'index.php')

	$Context->Dictionary['PageLinks'] = 'Page Links';

// if(($Context->SelfUrl == 'settings.php') && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS'))

	$Context->Dictionary['RoleTabsNotes'] = 'Select the tabs this Role can view, and, if applicable, access.';
	$Context->Dictionary['CreateANewPage'] = 'Create a New Page';
	$Context->Dictionary['PageManagement'] = 'P&aacute;ginas';
	$Context->Dictionary['ResyncTabs'] = 'Resync Tabs';
	$Context->Dictionary['ResyncTabsNotes'] = 'This will completely revert all system tabs back to the default, are you sure you wish to continue?';
	$Context->Dictionary['ResyncTabsSaved'] = 'All system tabs have been restored.';
	$Context->Dictionary['DefineNewPage'] = 'Define New Page';
	$Context->Dictionary['ModifyThePage'] = 'Modify the Page/Tab';
	$Context->Dictionary['SelectPage'] = 'Select Page/Tab to Edit';
	$Context->Dictionary['TabName'] = 'Tab Name';
	$Context->Dictionary['TabNameNotes'] = 'Name is the text which will appear on it.';
	$Context->Dictionary['TabIdentifier'] = 'Tab Identifier';
	$Context->Dictionary['TabIdentifierNotes'] = 'It is highly recommended that you leave the identifier field blank or how it originally was; it is only here for compatibility with other extensions.';
	$Context->Dictionary['TabAttributes'] = 'Tab Attributes';
	$Context->Dictionary['TabAttributesNotes'] = 'Extra HTML attributes for the tab anchor tag, such as the access key if Quick Keys is turned on (eg. accesskey="m"), or a title.';
	$Context->Dictionary['TabURL'] = 'Tab Url';
	$Context->Dictionary['TabURLNotes'] = 'The tab can either point to a URL or an HTML page.  If the URL is filled in in the above input, then it will be a hyperlink to that resource, if not, it will be a page with the below content. (note that urls must be complete, i.e. no relative urls)';
	$Context->Dictionary['PageHTML'] = 'Page HTML';
	$Context->Dictionary['PageHTMLNotes'] = 'PHP can also be included into the page HTML.';
	$Context->Dictionary['PageRoleNotes'] = 'The user roles which are allowed to view the tab and, if applicable, access the page it represents.  Please note, if a user, by default, cannot view a certain system tab, nothing in this will change that (eg. a guest will not be able to view the \'Settings\' tab just because it\'s set so here), and this will not prevent a user from accessing a link a hidden tab might point to.';
	$Context->Dictionary['PageReorderNotes'] = 'Drag and drop the pages below to reorder them.  Although their order will be saved automatically, you will need to refresh to see your changes.  Also note that tab order does not determine the default index page.';
	$Context->Dictionary['RoleTabs'] = 'Viewable Tabs/Pages';
	$Context->Dictionary['RoleTabsNotes'] = 'Select the tabs/custom pages the role is able to view/access';
	$Context->Dictionary['TabHidden'] = 'Tab Visibility';
	$Context->Dictionary['TabHiddenNotes'] = 'Whether or not a tab is displayed on the navigational bar';
	$Context->Dictionary['HiddenQ'] = 'Tab hidden from navigation';
?>