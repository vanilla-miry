<?php

	echo '<div id="Form" class="Account GlobalsForm">';

	if (ForceIncomingInt('Success', 0)) echo '<div id="Success">'.$this->Context->GetDefinition('ChangesSaved').'</div>';

	echo '<fieldset>

				<legend>'.$this->Context->GetDefinition("TPSettings").'</legend>'.$this->Get_Warnings().' '.$this->Get_PostBackForm('frmTPForm').'

				<ul>
					<li>
						<label for="txtConsumerKey">'.$this->Context->GetDefinition("consumer_key").'</label>
							<input type="text" name="TP_CONSUMER_KEY" id="txtConsumerKey"  value="'.$this->ConfigurationManager->GetSetting('TP_CONSUMER_KEY').'" />
						</li>
					<li>

					<li>
						<label for="txtConsumerSectet">'.$this->Context->GetDefinition("consumer_secret").'</label>
							<input type="text" name="TP_CONSUMER_SECRET" id="txtConsumerSectet"  value="'.$this->ConfigurationManager->GetSetting('TP_CONSUMER_SECRET').'" />
						</li>
					<li>

					<li>
						<label for="txtUserKey">'.$this->Context->GetDefinition("user_key").'</label>
							<input type="text" name="TP_USER_KEY" id="txtUserKey"  value="'.$this->ConfigurationManager->GetSetting('TP_USER_KEY').'" />
						</li>
					<li>


					<li>
						<label for="txtUserSecret">'.$this->Context->GetDefinition("user_secret").'</label>
							<input type="text" name="TP_USER_SECRET" id="txtUserSecret"  value="'.$this->ConfigurationManager->GetSetting('TP_USER_SECRET').'" />
						</li>
					<li>

				</ul>

				<div class="Submit">
						   <input type="submit" name="btnSave" value="'.$this->Context->GetDefinition('Save').'" class="Button SubmitButton" />
					 <a href="'.GetUrl($this->Context->Configuration, $this->Context->SelfUrl).'" class="CancelButton">'.$this->Context->GetDefinition('Cancel').'</a>
				</div>

			</form>
		</fieldset>
	</div>';

?>
