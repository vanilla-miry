<?php
	/*
	Extension Name: Tweet Posts
	Extension Url: http://lussumo.com/addons/
	Description: Tweets all new posts to Twitter
	Version: 0.1
	Author: John Croucher
	Author Url: http://www.sixlabrats.com/
	*/


	// Make sure we are being loaded from framework
	if (!defined('IN_VANILLA')) exit();


	include_once($Configuration['LIBRARY_PATH'] . 'Vanilla/Vanilla.Control.DiscussionForm.php');
	include ($Configuration['EXTENSIONS_PATH'].'TweetPosts/languages/english.php');
	include ($Configuration['EXTENSIONS_PATH'].'TweetPosts/PostBackControl.TweetPosts.php');

	include_once( dirname(__FILE__) . '/TwitterLib/EpiCurl.php' );
	include_once( dirname(__FILE__) . '/TwitterLib/EpiOAuth.php' );
	include_once( dirname(__FILE__) . '/TwitterLib/EpiTwitter.php' );


	// Make sure a place for our configuration settings exists
	if( !array_key_exists('TP_CONSUMER_KEY', $Configuration)) {AddConfigurationSetting($Context, 'TP_CONSUMER_KEY', '');}
	if( !array_key_exists('TP_CONSUMER_SECRET', $Configuration)) {AddConfigurationSetting($Context, 'TP_CONSUMER_SECRET', '');}
	if( !array_key_exists('TP_USER_KEY', $Configuration)) {AddConfigurationSetting($Context, 'TP_USER_KEY', '');}
	if( !array_key_exists('TP_USER_SECRET', $Configuration)) {AddConfigurationSetting($Context, 'TP_USER_SECRET', '');}


	// Setup the settings for Tweet Posts
	if	(($Context->SelfUrl == 'settings.php') && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS'))
	{
		$TPForm = $Context->ObjectFactory->NewContextObject($Context, 'TPForm');
		$Page->AddRenderControl($TPForm, $Configuration["CONTROL_POSITION_BODY_ITEM"]);

		$ExtensionOptions = $Context->GetDefinition('ExtensionOptions');

		$Panel->AddList($ExtensionOptions);
		$Panel->AddListItem($ExtensionOptions, $Context->GetDefinition('TweetPosts Management'), GetUrl($Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=TweetPosts'));

	}

	// Override the DiscussionForm functon passing it off to our custom function
	if (isset($Context))
		 $Context->ObjectFactory->SetReference('DiscussionForm', 'TweetPosts');

	class TweetPosts extends DiscussionForm 
	{

		// This function includes most of the code from DiscussionForm.
		// This should be changed if there is a better place to hook into
		
		public function TweetPosts(&$Context) 
		{
			$this->Name = 'DiscussionForm';
			$this->CommentFormAttributes = '';
			$this->DiscussionFormAttributes = '';
			$this->Constructor($Context);
			$this->FatalError = 0;
			$this->EditDiscussionID = 0;
			$this->CommentID = ForceIncomingInt('CommentID', 0);
			$this->DiscussionID = ForceIncomingInt('DiscussionID', 0);
			$this->DiscussionFormattedForDisplay = 0;
			$this->ValidActions = array('SaveDiscussion', 'SaveComment', 'Reply');

			$this->CallDelegate('PreLoadData');

			// Check permissions and make sure that the user can add comments/discussions
			// Make sure user can post
			if ($this->DiscussionID == 0 && $this->Context->Session->UserID == 0) {
				$this->Context->WarningCollector->Add($this->Context->GetDefinition('NoDiscussionsNotSignedIn'));
				$this->FatalError = 1;
			}

			$this->Comment = $this->Context->ObjectFactory->NewContextObject($this->Context, 'Comment');
			$this->Discussion = $this->Context->ObjectFactory->NewContextObject($this->Context, 'Discussion');

			$cm = $this->Context->ObjectFactory->NewContextObject($this->Context, 'CommentManager');
			$dm = $this->Context->ObjectFactory->NewContextObject($this->Context, 'DiscussionManager');
			$this->DelegateParameters['CommentManager'] = &$cm;
			$this->DelegateParameters['DiscussionManager'] = &$dm;
			// If editing a comment, define it and validate the user's permissions
			if ($this->CommentID > 0) {
				$this->Comment = $cm->GetCommentById($this->CommentID, $this->Context->Session->UserID);
				if (!$this->Comment) {
					$this->FatalError = 1;
				} else {
					$this->DiscussionID = $this->Comment->DiscussionID;
					$this->Discussion = $dm->GetDiscussionById($this->Comment->DiscussionID);
					if (!$this->Discussion) {
						$this->FatalError = 1;
					} else {
						// if editing a discussion
						if (($this->Context->Session->UserID == $this->Discussion->AuthUserID || $this->Context->Session->User->Permission('PERMISSION_EDIT_DISCUSSIONS')) && $this->Discussion->FirstCommentID == $this->CommentID) {
							$this->EditDiscussionID = $this->Discussion->DiscussionID;
							$this->Discussion->Comment = $this->Comment;
						}
						// Set the page title
						$this->DiscussionFormattedForDisplay = 1;
						$this->Discussion->FormatPropertiesForDisplay();
						$this->Context->PageTitle = $this->Discussion->Name;
					}
				}
				// Ensure that this user has sufficient priviledges to edit the comment
				if ($this->Comment
					&& $this->Discussion
					&& !$this->Context->Session->User->Permission('PERMISSION_EDIT_COMMENTS')
					&& $this->Context->Session->UserID != $this->Comment->AuthUserID
					&& !($this->Discussion->FirstCommentID == $this->CommentID && $this->Context->Session->User->Permission('PERMISSION_EDIT_DISCUSSIONS'))) {

					$this->Context->WarningCollector->Add($this->Context->GetDefinition('ErrPermissionCommentEdit'));
					$this->FatalError = 1;
				}
			}

			$this->CallDelegate('PostLoadData');

			// If saving a discussion
			if ($this->PostBackAction == 'SaveDiscussion') {

				$FirstCommentID = $this->Discussion->FirstCommentID;
				$AuthUserID = $this->Discussion->AuthUserID;
				$this->Discussion->Clear();
				$this->Discussion->GetPropertiesFromForm($this->Context);
				$this->Discussion->FirstCommentID = $FirstCommentID;
				$this->Discussion->AuthUserID = $AuthUserID;

				// If we are editing a discussion, the following line
				// will make sure we save the proper discussion topic & message
				$this->Discussion->DiscussionID = $this->EditDiscussionID;

				if ($this->IsValidFormPostBack()) {
					$this->DelegateParameters['SaveDiscussion'] = &$this->Discussion;
					$this->CallDelegate('PreSaveDiscussion');

					$ResultDiscussion = $dm->SaveDiscussion($this->Discussion);

					$this->DelegateParameters['ResultDiscussion'] = &$ResultDiscussion;
					$this->CallDelegate('PostSaveDiscussion');
				

					if ($ResultDiscussion && ($ResultDiscussion->DiscussionID > 0)) 
					{
				
						// Saved successfully, so send back to the discussion
						$Suffix = CleanupString($this->Discussion->Name).'/';
						$Redirect = GetUrl(
							$this->Context->Configuration, 'comments.php', '', 'DiscussionID',
							$ResultDiscussion->DiscussionID, '', '', $Suffix);

						// **** Twitter Code
						$twitter = new EpiTwitter( $Context->Configuration['TP_CONSUMER_KEY'], $Context->Configuration['TP_CONSUMER_SECRET'] );
						$twitter->setToken( $Context->Configuration['TP_USER_KEY'], $Context->Configuration['TP_USER_SECRET'] );
		           
						// Create our message for twitter.
						$tweet = "#vanillaforums " . $this->Discussion->Name . "\n" . $Redirect;
						
						// Initiate the status update request
						$status = $twitter->post_statusesUpdate( array( 'status' => $tweet ) );
		           
						// Ensure the request completes
						$status->response;

						// Redirec the user to the post they just created
						Redirect($Redirect);

					}
				
				}
		
			} 
			 else
			{
				// If the user is not creating a new discussion, then just call the parent function and process as usual.
				parent::DiscussionForm($Context);

			}
		
		}
		 
	}
  
?>
