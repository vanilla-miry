<?php

	/*
	Extension Name: Tweet Posts
	Extension Url: http://lussumo.com/addons/
	Description: Tweets all new posts to Twitter
	Version: 0.1
	Author: John Croucher
	Author Url: http://www.sixlabrats.com/
	*/
	
	// This class is used for the Tweet Posts administration page
	

	class TPForm extends PostBackControl
	{
	
		var $ConfigurationManager;

	 	function TPForm(&$Context)
		{
		  	$this->Name = 'TPForm';
		  	$this->ValidActions = array('TweetPosts', 'ProcessTweetPosts');
			$this->Constructor($Context);

			// Make sure the user has posted data
		  	if ($this->IsPostBack)
			{
				// Get the settings file
				$SettingsFile = $this->Context->Configuration['APPLICATION_PATH'].'conf/settings.php';
				$this->ConfigurationManager = $this->Context->ObjectFactory->NewContextObject($this->Context, 'ConfigurationManager');
		      
		      // Make sure the user has selected the ProcessTweetPosts option
				if($this->PostBackAction == 'ProcessTweetPosts')
				{

					// If oauth_token is not empty then we must be getting to this page via a redirect from twitter.
					
					if ( isset( $_GET[ 'oauth_token' ] ) && !empty( $_GET[ 'oauth_token' ] ) ) 
					{
						// Create the twitter object
						$twitter = new EpiTwitter( ForceIncomingString('TP_CONSUMER_KEY', ''), ForceIncomingString('TP_CONSUMER_SECRET', '') );
						$twitter->setToken( $_GET[ 'oauth_token' ] );

						// Get the access token
						$token = $twitter->getAccessToken();

						// Save the details
						$this->ConfigurationManager->DefineSetting('TP_USER_KEY', $token->oauth_token, 0);
						$this->ConfigurationManager->DefineSetting('TP_USER_SECRET', $token->oauth_token_secret, 0);

						$this->DelegateParameters['ConfigurationManager'] = &$this->ConfigurationManager;

					}
					else
					{
						try
						{
							// Get the twitter auth url
							$twitter = new EpiTwitter( ForceIncomingString('TP_CONSUMER_KEY', ''), ForceIncomingString('TP_CONSUMER_SECRET', '') );
							$url = $twitter->getAuthorizationUrl();

							// Save the consumer key and consumer secret
							$this->ConfigurationManager->DefineSetting('TP_CONSUMER_KEY', ForceIncomingString('TP_CONSUMER_KEY', ''), 0);
							$this->ConfigurationManager->DefineSetting('TP_CONSUMER_SECRET', ForceIncomingString('TP_CONSUMER_SECRET', ''), 0);

							$this->DelegateParameters['ConfigurationManager'] = &$this->ConfigurationManager;
							$this->ConfigurationManager->SaveSettingsToFile($SettingsFile);

							// Redirect to the auth url
							header('location: ' . $url);
							exit;

						} catch (Exception $e) {

							header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=TweetPosts&Success=0'));
						}

					}

					$this->DelegateParameters['ConfigurationManager'] = &$this->ConfigurationManager;

					if($this->ConfigurationManager->SaveSettingsToFile($SettingsFile))
					{

						header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=TweetPosts&Success=1'));

					}
					else
					{
						$this->PostBackAction = 'TweetPosts';
					}

				}

			}

		}

		function Render()
		{
			if ($this->IsPostBack)
			{
				$this->PostBackParams->Clear();
				if($this->PostBackAction == 'TweetPosts')
				{
					$this->PostBackParams->Set('PostBackAction', 'ProcessTweetPosts');
					$ThemeFilePath = $this->Context->Configuration['EXTENSIONS_PATH'].'TweetPosts/theme/Theme.TweetPosts.php';
					require($ThemeFilePath);
				}

			}

		}
	}

?>
