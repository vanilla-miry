<?php
/*
Extension Name: Auto Selection of Category
Extension Url: None
Description: If a user starts a new discussion, this extension selects category of the previous viewed discussion. Will be useful if the forum has a lot of categories.
Version: 0.1
Author: Roman "S" Vasiliev
Author Url: None

===================================
EXTENSION INSTALLATION INSTRUCTIONS
===================================
In order for Vanilla to recognize an extension, it must be contained within its
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/this_extension_name/default.php

Once this is complete, you can enable the extension through the "Manage
Extensions" form on the settings tab in Vanilla.

===================================
CHANGELOG
===================================
* 0.1 (11 Oct 2008)
- First (and probably last) release
*/
if(!defined('IN_VANILLA')) die();

if($Context->SelfUrl != 'post.php') return;
$Context->AddToDelegate('DiscussionForm', 'DiscussionForm_PreRender', 'CategoryAutoSelect');

function IsModRewrite()
{
	return $GLOBALS['Configuration']['URL_BUILDING_METHOD'] == 'mod_rewrite';
}

function CategoryAutoSelect(&$DiscussionForm)
{
	$C =& $DiscussionForm->Context;
	
	$RefURL = $_SERVER['HTTP_REFERER'];
	$CheckString = IsModRewrite() ? '/discussion/' : '?DiscussionID=';
	$Pos = StrPos($RefURL, $CheckString);
	if($Pos < 1) return;

	$DiscussionID = SubStr($RefURL, $Pos + StrLen($CheckString));
	$DiscussionID = intval($DiscussionID);

	$Query = sprintf('SELECT CategoryID FROM %s WHERE DiscussionID=%d LIMIT 1',
		$C->Configuration['DATABASE_TABLE_PREFIX'].$C->DatabaseTables['Discussion'],
		$DiscussionID);
	
	$Result = mysql_unbuffered_query($Query, $C->Database->Connection);
	List($CategoryID) = mysql_fetch_row($Result);
	$_GET['CategoryID'] = $CategoryID;
	
	UnSet($RefURL, $CheckString, $Query, $Result);
}

?>