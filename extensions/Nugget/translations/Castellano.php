<?php

$Context->SetDefinition('NuggetObj_NoInputValue', 'You must enter a Nugget name.');
$Context->SetDefinition('NuggetObj_AlreadyCreated', 'A Nugget of that name has already been created.');
$Context->SetDefinition('NuggetObj_FileError', 'NUGGETMANAGER ERROR: An error occured in attempting to save your nuggets.  '.
			'Please check that your file permissions are correct');
$Context->SetDefinition('CreateANewNugget', 'Cook a new Nugget');
$Context->SetDefinition('NuggetManagement', 'Nuggets');
$Context->SetDefinition('NuggetDocumentation', '<strong>Cooking Documentation</strong><br />'.
			'<p>Click on "Cook a new nugget" to start preparing your nugget. Give it a name, some spicing and let it marinate.</p>'.
			'<p>To cook (enable) nuggets select which pans you want to use for cooking. By default, all new nuggets are frozen (disabled).</p> '.
			'<p>You can specify where within a pan, a nugget will appear by adjusting its weight. Lighter weight nuggets (negative weight) appear higher on the pan than heavier nuggets (positive weight).</p> '.
			'<p>Negative weight or zero weight for "In Comment" Nugget will not work. You need positive weights. Use common sense when dealing with Menu Pan, there isn\'t much space on the pan, so use just one nugget or atmost 2 if you are really crafty.</p> '.
			'<p>&lt;Head&gt; Tag pan can be used to for inserting Javascript/Styles/Meta tags inside the &lt;Head&gt; tag. You obviously cannot put a div in the &lt;Head&gt; tag</p>'.
			'<p>Nuggets are arranged based on pans and weight. So you can see at a glance, which nugget will appear higher, and which will appear lower.</p>'.
			'<p>Not all weight variations show an affect in changed order. Sometimes you have to do large weight changes to see an affect. For Eg. Panel requires more than 2 or 3 weight difference.</p>');
$Context->SetDefinition('DefineNewNugget', 'Define new nugget');
$Context->SetDefinition('ModifyTheNugget', 'Modify the nugget');
$Context->SetDefinition('NuggetEdit', 'Edit the Nugget');
$Context->SetDefinition('NuggetName', 'Nugget Name');
$Context->SetDefinition('NuggetNameHide', ' Hide Nugget Name from appearing on the nugget. Ideal for ads.');
$Context->SetDefinition('NuggetDescription', 'Nugget Description');
$Context->SetDefinition('NuggetDescriptionNotes', 'A brief description of your nugget. This is just for your reference.');
$Context->SetDefinition('NuggetHTML', 'Nugget HTML');
$Context->SetDefinition('NuggetHTMLNotes', 'PHP/Javascript spicing can also be included on the Nugget. (do not escape single quotes)');
$Context->SetDefinition('NuggetPages', 'Pages');
$Context->SetDefinition('NuggetPagesNotes', 'Select the pages that you want the nugget to appear.');
$Context->SetDefinition('NuggetRoleNotes', 'The user roles which are allowed to view the nugget.');
$Context->SetDefinition('NuggetNotes', 'Nuggets are snippets of code that you can display into certain regions (called pans) of your webpage. Ads, Banners etc are perfect nuggets');
$Context->SetDefinition('NuggetPan', 'Select a pan for the Nugget');
$Context->SetDefinition('NuggetWeight', 'Select Nugget\'s weight');
$Context->SetDefinition('NuggetWeightNotes', 'Negative weight makes the nugget appear higher within a pan. Positive weight will do the exact opposite');
$Context->SetDefinition('HeadTag', 'Head tag');
$Context->SetDefinition('AboveBanner', 'Above Banner');
$Context->SetDefinition('Menu', 'Menu');
$Context->SetDefinition('BelowBanner', 'Below Banner');
$Context->SetDefinition('AboveContent', 'Above Content');
$Context->SetDefinition('BelowContent', 'Below Content');
$Context->SetDefinition('Footer', 'Footer');
$Context->SetDefinition('Panel', 'Panel');
$Context->SetDefinition('BelowPanel', 'Below Panel');
$Context->SetDefinition('InComments', 'In Comments');
$Context->SetDefinition('Frozen', '[Frozen]');

?>