<?php
/**
 * Class for displaying All Nuggets except Panel ones
*/
class DisplayNugget extends Control
{
	var $DisplayNuggetString;

	function DisplayNugget(&$Context, $Display,$NuggetHolder)
	{
		$this->Name = 'DisplayNugget';
		$this->Control($Context);
		$this->Display = $Display;
		$this->NuggetHolder = $NuggetHolder;
	}
	function Render()
	{
		$vardisplay = str_replace(" ","_",$this->Display);
		if (!$this->NuggetHolder)
		{
			$this->Display['html'] = AllowPHP($this->Display['html']);
			if(!$this->Display['hideName']) $this->DisplayNuggetString = '<h2>'.$this->Display['name'].'</h2>';
			$this->DisplayNuggetString = '<div class="Nugget '.$this->Display['position'].'">'.$this->DisplayNuggetString. $this->Display['html'].'</div>';
		}
		else
			$this->DisplayNuggetString = '<div class="NuggetHolder '. str_replace(" ","_",$this->Display) .'">'.$this->Display.'</div>';

		echo $this->DisplayNuggetString;
	}
}

?>
