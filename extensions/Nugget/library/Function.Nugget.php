<?php
/**
 * Helper function for sorting nuggets
 *
 * Active nuggets are sorted by region, then by weight.
 * Disabled nuggets are sorted by name.
 * From Drupal.org Block Module
 */
function NuggetSort($a, $b)
{
	$status = $b['status'] - $a['status'];
	// Separate enabled from disabled.
	if ($status) {
		return $status;
	}
	// Enabled blocks
	if ($a['status']) {
		$place = strcmp($a['position'], $b['position']);
		return $place ? $place : ($a['weight'] - $b['weight']);
	}
	// Disabled blocks
	else {
		return strcmp($a['name'], $b['name']);
	}
}
//Function to Display Panel Nuggets Only
function DisplayPanel ($Display,$NuggetHolder)
{
	$DisplayPanelString = "";

	if (!$NuggetHolder)
	{
		$Display['html'] = AllowPHP($Display['html']);
		if(!$Display['hideName']) $DisplayPanelString = '<h2>'.$Display['name'].'</h2>';
		$DisplayPanelString = '<ul><li>'.$DisplayPanelString.'<ul>'. $Display['html'].'</ul></li></ul>';
	}
	else
		$DisplayPanelString = '<div class="NuggetHolder Panel">'. $Display .'</div>';

		return $DisplayPanelString;
}

/**
 * Helper function allowing PHP
 *
 * Incoming string is parsed
 * Result is evaluated and send back
 * From Page Manager extension by SirNot
 */
function AllowPHP($phpHTML)
{
	global $Context;
	$Matches="";
	$MatchCount = preg_match_all("/<\?php(.*?)\?>/si", $phpHTML, $Matches);
	$HTML = preg_split("/<\?php(.*?)\?>/si", $phpHTML);
	$FullString = '';
	for($i = 0; $i < $MatchCount; $i++)
	{
		list(, $CurHTML) = each($HTML);
		ob_start();
		eval($Matches[1][$i]);
		$FullString .= $CurHTML . ob_get_contents();
		ob_end_clean();
	}
	list(, $CurHTML) = each($HTML);
	$FullString .= $CurHTML;

	return ($FullString);
}
//Function to display Nugget in between Comments
function DisplayInCommentNugget(&$CommentGrid)
{	
	global $NuggetObj;

	if ($NuggetObj->PositionCount['COMMENT']>0) {
	for ($i=0; $i < count($NuggetObj->Nugget); $i++)
	{
		if ($NuggetObj->Nugget[$i]['status']//check status
			&& in_array($CommentGrid->Context->Session->User->RoleID, $NuggetObj->Nugget[$i]['roles']) // check roles
			&& in_array("comments.php", $NuggetObj->Nugget[$i]['pages']) //check pages
			&& $NuggetObj->Nugget[$i]['position'] == 'COMMENT')
		{
			// Code by WallPhone
			$RowNumber = &$CommentGrid->DelegateParameters['RowNumber']; //Which Comment is being rendered. 1st, 2nd etc
			$Comment = &$CommentGrid->DelegateParameters['Comment']; //What does that comment currently holds
			if ($RowNumber == abs($NuggetObj->Nugget[$i]['weight'])) // If the Comment row matches the weight of the nugget
			{
				$NuggetObj->Nugget[$i]['html'] = AllowPHP($NuggetObj->Nugget[$i]['html']);

				// Close the existing CommentBody div, and open a new one that gets closed by the old div:
				$Comment->Body .= '</div></li><li>';
				if (!$NuggetObj->Nugget[$i]['hideName'])
				{
					$Comment->Body .='<div class="Nugget CommentHeader">
								<ul>
								   <li>
								      <a href="#">'.$NuggetObj->Nugget[$i]['name'].'</a>
								   </li>
								</ul><span><a></a></span>
							</div>';
				}
				$Comment->Body .= '<div class="Nugget CommentBody">'.$NuggetObj->Nugget[$i]['html'];
				$NuggetObj->PositionCount['COMMENT']--;
				break;
			}
		}
	}
	}
}
?>