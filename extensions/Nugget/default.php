<?php
/*
Extension Name: Nuggets
Extension Url: http://lussumo.com/addons
Description: Allows administrators to create/edit/delete role and page specific blocks of code called "Nuggets" For eg Ads/Banners or meta tags, widgets, pretty much anything.
Version: 1.1.6
Author: MySchizoBuddy
Author Url: N/A
*/

// Blog Path
$NuggetPath = dirname(__FILE__);

// Libraries
include($NuggetPath . '/library/Class.NuggetManagement.php');
include($NuggetPath . '/library/PostBackControl.NuggetForm.php');
include($NuggetPath . '/library/Function.Nugget.php');
include($NuggetPath . '/library/Control.DisplayNugget.php');

/**
 * Initialize Nugget Management Object
 */
$NuggetObj = $Context->ObjectFactory->NewContextObject($Context, 'NuggetManagement');


/**
 * Settings Panel. This is where all the forms are generated
 */
if(($Context->SelfUrl == 'settings.php') && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')) {

	$NuggetForm = $Context->ObjectFactory->NewContextObject($Context, 'NuggetForm');
	$Page->AddRenderControl($NuggetForm, $Configuration["CONTROL_POSITION_BODY_ITEM"]);
	$ExtensionOptions = $Context->GetDefinition('ExtensionOptions');
	$Panel->AddList($ExtensionOptions);
	$Panel->AddListItem($ExtensionOptions, $Context->GetDefinition('NuggetManagement'), GetUrl($Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=NuggetList'));

} //end settings

//Finally Display all the Nuggets on selected pages and selected roles
$GLOBALS['Head']->AddStyleSheet('extensions/Nugget/nugget.css');

//Display Active Nuggets on all pages
for ($i=0; $i < count($NuggetObj->Nugget); $i++) {
	if ($NuggetObj->Nugget[$i]['status'] && in_array($Context->Session->User->RoleID, $NuggetObj->Nugget[$i]['roles'])){ //check status and role of nugget
		if (in_array($Context->SelfUrl, $NuggetObj->Nugget[$i]['pages']) || in_array(ForceIncomingString('Page', ''), $NuggetObj->Nugget[$i]['pages']) || in_array(ForceIncomingString('PostBackAction', ''), $NuggetObj->Nugget[$i]['pages'])){ //check which page is allowed to display the nugget

			if ($Context->SelfUrl == 'index.php' && ForceIncomingString('Page', '') && !in_array(ForceIncomingString('Page', ''), $NuggetObj->Nugget[$i]['pages']))// Support for Page Manager
				break;

			//Check if the Nugget is meant for the Head Tag
			if ($NuggetObj->Nugget[$i]['position'] == 'COMMENT' || $NuggetObj->Nugget[$i]['position'] == 'MENU') {/*skip them, we will tackle them later*/}
			else if ($NuggetObj->Nugget[$i]['position'] == 'HEADER')//Check if the Nugget is meant for the Head tag
			{
				$Head->AddString($NuggetObj->Nugget[$i]['html']);
			}
			else if ($NuggetObj->Nugget[$i]['position'] == 'PANEL')//Check if the Nugget is meant for Panel
			{
				$Panel->AddString(DisplayPanel($NuggetObj->Nugget[$i],0), (2*$NuggetObj->Nugget[$i]['weight'])+10);// Panel requires more weight variation. Multiply by a constant
			}
			else if ($NuggetObj->Nugget[$i]['position'] == 'BELOW_PANEL')//Check if the Nugget is meant for Below Panel
			{
				$Panel->AddString(DisplayPanel($NuggetObj->Nugget[$i],0), ($NuggetObj->Nugget[$i]['weight'])+600);
			}
			else
			{
				($NuggetObj->Nugget[$i]['position'] == 'CONTROL_POSITION_MENU') ? $WeightCorrection=0:$WeightCorrection=11;
				$NewDisplayNugget =& new DisplayNugget($Context, $NuggetObj->Nugget[$i],0);
				$Page->AddRenderControl($NewDisplayNugget,$Configuration[$NuggetObj->Nugget[$i]['position']]+$NuggetObj->Nugget[$i]['weight'] + $WeightCorrection);
			}

		}
	}
}


//Display Nugget Placeholders only on settings.php
if ($Context->SelfUrl == 'settings.php'
	&& $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')
	&& in_array($NuggetForm->PostBackAction, array('NuggetList', 'Nugget', 'RemoveNugget')))
{
	for($i = 0; $i < count($NuggetObj->Position); $i++)
	{
		if ($NuggetObj->Position[$i]['position'] == 'HEADER' || $NuggetObj->Position[$i]['position'] == '[Frozen]' || $NuggetObj->Position[$i]['position'] == 'COMMENT'){/*Skip them*/}
		else if ($NuggetObj->Position[$i]['position'] == 'PANEL')
		{
			$Panel->AddString(DisplayPanel($NuggetObj->Position[$i]['name'],1),490);
		}
		else if ($NuggetObj->Position[$i]['position'] == 'BELOW_PANEL')
		{
			$Panel->AddString(DisplayPanel($NuggetObj->Position[$i]['name'],1),600);
		}
		else
		{
			($NuggetObj->Position[$i]['position'] == 'CONTROL_POSITION_MENU') ? $WeightCorrection=0:$WeightCorrection=11;
			$NewDisplayNugget =& new DisplayNugget($Context, $NuggetObj->Position[$i]['name'] ,1);
			$Page->AddRenderControl($NewDisplayNugget,$Configuration[$NuggetObj->Position[$i]['position']] + $WeightCorrection );
		}
	}
}

if ($NuggetObj->PositionCount['COMMENT']>0) $Context->AddToDelegate('CommentGrid', 'PreCommentOptionsRender', 'DisplayInCommentNugget');

?>
