<?php
$Context->SetDefinition('create_level_label', 'Required role level to create a new discussion');
$Context->SetDefinition('create_level_description', 'Any user with a role below this setting is allowed to post in an existing discussion, but cannot create a new discussion in this category.');
$Context->SetDefinition('post_level_label', 'Required role level to post a comment');
$Context->SetDefinition('post_level_description', 'This category is read only for users with lower role level.<br />These two last settings are only effective if the user is allowed to take part in this category by the chekboxes.');
?>