<?php
$Context->SetDefinition('create_level_label', 'Rôle requis pour ouvrir de nouvelles discussions');
$Context->SetDefinition('create_level_description', 'Un utilisateur dont le rôle est inférieur à ce réglage est autorisé à commenter une discussion existante mais ne pourra pas en créer de nouvelle dans cette categorie.');
$Context->SetDefinition('post_level_label', 'Rôle requis pour ajouter un commentaire');
$Context->SetDefinition('post_level_description', 'Cette catégorie est en lecture seule pour les utilisateurs d\'un rôle de niveau inférieur.<br />Ces deux derniers réglages ne sont effectifs que si l\'utilisateur est autorisé à participer à cette discussion par les coches ci-dessus.');

?>