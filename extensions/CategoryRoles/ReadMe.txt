===========================================
GENERAL EXTENSION INSTALLATION INSTRUCTIONS
===========================================

In order for Vanilla to recognize an extension, it must be contained within it's
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/this_extension_name/default.php

Once this is complete, you can enable the extension through the "Manage
Extensions" form on the settings tab in Vanilla.

======================
CategoryRoles SPECIFIC
======================

Usage
*****
This extension adds two drop-down menu in the Categories setting pane.
The roles are given increasing priority level from top to bottom. This is a core feature
This extension uses this priority to set the required threshold.
Adding a new role will probably imply revision of the categories settings.
Most troubles reported in the forum thread are from misunderstanding this priority order.

Theme
*****
This extension needs a few added lines in two theme files.
There is a pseudo theme comming with the CategoryRoles extension.

Move the folder containing this theme to the <Vanilla>/forum/theme folder
Copy the "styles" folder from the Vanilla theme to this one (not included to save extension files size).

Q. How do I apply this theme?
A. Go to the settings tab and clicking on the “Themes and Styles” link.
	Select this theme from the first dropdown. Check the “Apply to all users” box and click save.

If you want to use the extension with another theme, you can copy the two php files of this folder to you theme folder.
If this other theme also has modification to these files, you should merge the few lines necessary to the extension.
They are clearly marked by //CategoryRole comments.

********
Obsolete. From version 1.1.4, the delegate was added to the core.
Instruction kept there in case someone use this extension on a lower release.
Required delegate
*****************

This extension REQUIRES a specific delegate.
At the time of this writing (first release of the extension), you should add it yourself in the file <Vanilla>/forum/library/Vanilla/Vanilla.Control.CategoryForm.php
There is only one line to add. In the excerpt below it's marked with the "//CategoryRoles added delegate" comment.
Presumably Mark will add it to future releases and this part of the readme be obsolete.

line 40
			if ($this->PostBackAction == 'ProcessCategory') {
				$this->Category = $this->Context->ObjectFactory->NewObject($this->Context, 'Category');
				$this->Category->GetPropertiesFromForm($this->Context);
				$Action = ($this->Category->CategoryID == 0) ? "SavedNew" : "Saved";
				if (($this->Category->CategoryID > 0 && $this->Context->Session->User->Permission('PERMISSION_EDIT_CATEGORIES'))
					|| ($this->Category->CategoryID == 0 && $this->Context->Session->User->Permission('PERMISSION_ADD_CATEGORIES'))) {
					if ($this->CategoryManager->SaveCategory($this->Category)) {
						$this->CallDelegate('PostSaveCategory'); //CategoryRoles added delegate
						header('location: '.GetUrl($this->Context->Configuration, $this->Context->SelfUrl, '', '', '', '', 'PostBackAction=Categories&Action='.$Action));
					}
				} else {
					$this->IsPostBack = 0;
				}
