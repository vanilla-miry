<?php
/*
 Extension Name: CategoryRoles
 Extension Url: http://lussumo.com/addons/?PostBackAction=AddOn&AddOnID=230
 Description: Add a way to define distinct roles level for adding comment and opening a new discussion in a given category.
 Version: 0.7
 Author: Max Barel
 Author Url: http://ac-mb.info
 */
/*
0.1 initial
0.2 added check for install and creation of database column
0.3 corrected error on new category
	corrected bug on indexes when removing categories from select
0.4 Corrected a misuse of DicussionID rather than CategoryID
0.5 set Cat_filter column to NULL for MySQL 5 compat
	added $Context->Database->GetConnection() in case an implementation use several connections
0.6 Added a check on setting values for unitialied categories
0.7 Readme reflects addition of delegate to the core.
	Replaced "LUM_" with $Configuration['DATABASE_TABLE_PREFIX'] 
 */

//=============inline processing=============
// Check to see if this extension has been configured
if (!isset($Configuration['EXTENSION_CATEGORYROLES'])) {
	$res = mysql_query("DESCRIBE {$Configuration['DATABASE_TABLE_PREFIX']}Category Cat_filter", $Context->Database->GetConnection())
		or die("error while cheking {$Configuration['DATABASE_TABLE_PREFIX']}Category table from CategoryRoles extension<br/>\n$req<br/>\n" . mysql_error()) ;
	$current = mysql_fetch_assoc($res);
	if ($current and $current['Field'] == 'Cat_filter') { //and $current['Type'] == 'text'
		//column already there, presumably from a previous install
		$res = mysql_query("ALTER TABLE {$Configuration['DATABASE_TABLE_PREFIX']}Category MODIFY COLUMN Cat_filter text", $Context->Database->GetConnection())
		or die("error while updating Cat_filter column in {$Configuration['DATABASE_TABLE_PREFIX']}Category table, in CategoryRoles extension<br/>\n" . mysql_error()) ;
		AddConfigurationSetting($Context, 'EXTENSION_CATEGORYROLES', '0.6');
	} else {
		//add the column
		$res = mysql_query("ALTER TABLE {$Configuration['DATABASE_TABLE_PREFIX']}Category ADD COLUMN Cat_filter text", $Context->Database->GetConnection())
			or die("error while adding Cat_filter column to {$Configuration['DATABASE_TABLE_PREFIX']}Category table, in CategoryRoles extension<br/>\n" . mysql_error()) ;
		AddConfigurationSetting($Context, 'EXTENSION_CATEGORYROLES', '0.6');
	}
} elseif ($Configuration['EXTENSION_CATEGORYROLES'] < '0.6') {
	//update column definition
	$res = mysql_query("ALTER TABLE {$Configuration['DATABASE_TABLE_PREFIX']}Category MODIFY COLUMN Cat_filter text", $Context->Database->GetConnection())
	or die("error while updating Cat_filter column in {$Configuration['DATABASE_TABLE_PREFIX']}Category table, in CategoryRoles extension<br/>\n" . mysql_error()) ;
	AddConfigurationSetting($Context, 'EXTENSION_CATEGORYROLES', '0.6');
}

//=============delegation=============
if ($Context->SelfUrl == 'post.php') {
	//This is the page where we need to filter categories out
	//This add an object whose some method will be called from theme file.
	$Context->AddToDelegate("DiscussionForm", "PreLoadData", "CategoryFilter");
}
elseif ($Context->SelfUrl == 'settings.php') {
	//addon to the category setting: select for roles levels
	$Context->AddToDelegate("CategoryForm", "Constructor", "CategoryRoles");
	$Context->AddToDelegate("CategoryForm", "PostSaveCategory", "CategoryRoles");
}
elseif ($Context->SelfUrl == 'comments.php') {
	//onn this page we hide the comment form, using the core ShowForm variable.
	$Context->AddToDelegate("CommentGrid", "Constructor", "UserCanPost");
}

//=============Classes and function declarations=============
class Cat_filter {
	//this classe manage to hide categories from post and comment page for unauthorized roles.
	var $cat_levels = array(); //array to store the categories autorization levels
	var $user_level;
	
	function get_levels($user_role = false) {
		global $Context;
		//retreive level informations from the database
		$req = "SELECT CategoryID, Cat_filter FROM {$Context->Configuration['DATABASE_TABLE_PREFIX']}Category";
		$res = mysql_query($req, $Context->Database->GetConnection())
			or die("error while reading {$Context->Configuration['DATABASE_TABLE_PREFIX']}Category table from CategoryRoles extension<br/>\n$req<br/>\n" . mysql_error()) ;
		while ($c = mysql_fetch_assoc($res)) $this->cat_levels[$c['CategoryID']] = $c['Cat_filter'] ? unserialize($c['Cat_filter']) : array('create_level'=>0,'post_level'=>0);

		if ($user_role) {
			//retreive user role level (= Priority)
			$req = "SELECT Priority FROM {$Context->Configuration['DATABASE_TABLE_PREFIX']}Role WHERE RoleID=$user_role";
			$res = mysql_query($req, $Context->Database->GetConnection())
				or die("error while reading {$Context->Configuration['DATABASE_TABLE_PREFIX']}Role table from CategoryRoles extension<br/>\n$req<br/>\n" . mysql_error()) ;
			$r = mysql_fetch_assoc($res);
			$this->user_level = $r['Priority'];
		}
	}
	
	function hide_categories(&$cs, $user_role) {
		//called from discussion_form customized theme, get the select to filter and the current user role
		if (!$this->cat_levels) $this->get_levels($user_role);
		foreach ($cs->aOptions as $i=>$o)
			if (isset($this->cat_levels[$o['IdValue']]['create_level']) //the category has a create level set
				and $this->cat_levels[$o['IdValue']]['create_level'] > $this->user_level) // and it's beyond current user level
				unset($cs->aOptions[$i]); // then remove the category from the available choices
				//array_splice($cs->aOptions, $i, 1);
		$cs->aOptions = array_values($cs->aOptions); //compact array
		
	}
}

function CategoryRoles(&$cat_form) {
	global $Context;

	if ($cat_form->PostBackAction == 'Category') {
		$cat_filter = new Cat_filter();
		$cat_filter->get_levels();
		//build roles selects
		$req = "SELECT Name, Priority FROM {$Context->Configuration['DATABASE_TABLE_PREFIX']}Role ORDER BY Priority";
		$roles_data = mysql_query($req, $Context->Database->GetConnection())
			or die("error while reading {$Context->Configuration['DATABASE_TABLE_PREFIX']}Role table from CategoryRoles extension<br/>\n$req<br/>\n" . mysql_error()) ;
		$cat_form->create_level = $cat_form->Context->ObjectFactory->NewObject($cat_form->Context, 'Select');
		$cat_form->create_level->Name = 'create_level';
		$cat_form->create_level->CssClass = 'SmallInput';
		$cat_form->create_level->AddOptionsFromDataSet($cat_form->Context->Database, $roles_data, 'Priority', 'Name');
		if ($cat_form->Category->CategoryID)
			$cat_form->create_level->SelectedValue = $cat_filter->cat_levels[$cat_form->Category->CategoryID]['create_level'];

		$cat_form->post_level = clone($cat_form->create_level);
		$cat_form->post_level->Name = 'post_level';
		if ($cat_form->Category->CategoryID)
			$cat_form->post_level->SelectedValue = $cat_filter->cat_levels[$cat_form->Category->CategoryID]['post_level'];
	}
	elseif ($cat_form->PostBackAction == 'ProcessCategory') {
		$cat_level['create_level'] = ForceIncomingInt('create_level', 0);
		$cat_level['post_level'] = ForceIncomingInt('post_level', 0);
		$cat_level = serialize($cat_level);
		$req = "UPDATE {$Context->Configuration['DATABASE_TABLE_PREFIX']}Category SET Cat_filter='$cat_level' WHERE CategoryID={$cat_form->Category->CategoryID}";
		mysql_query($req, $Context->Database->GetConnection())
			or die("error while updating {$Context->Configuration['DATABASE_TABLE_PREFIX']}Category table from CategoryRoles extension<br/>\n$req<br/>\n" . mysql_error()) ;
	}
}

function UserCanPost(&$comment_grid) {
	$cat_filter = new Cat_filter();
	$cat_filter->get_levels($comment_grid->Context->Session->User->RoleID);
	$CategoryID = $comment_grid->Discussion->CategoryID;
	if (isset($cat_filter->cat_levels[$CategoryID]['post_level']) //the category has a post level set
		and $cat_filter->cat_levels[$CategoryID]['post_level'] > $cat_filter->user_level) // and it's beyond current user level
		$comment_grid->ShowForm=0;
}
function CategoryFilter(&$discuss_form) {
	$discuss_form->cat_filter = new Cat_filter();
}
?>