<?php
/*
Extension Name: Twitter Feed
Extension Url: http://vanillaforums.org/get/447
Description: Show the most recent 5 tweets of your users.
Version: 2.1
Author: Paul Fraser
Author Url: http://www.paulOr.net
*/

	global $Context, $Head;

	##	DEFINE CUSTOMIZATIONS
	###########################################
	$Context->Configuration['CUSTOMIZATION_TWITTER_USERNAME'] = '';
	$Context->SetDefinition('CUSTOMIZATION_TWITTER_USERNAME', 'Your Twitter Username');
	$Context->SetDefinition('CUSTOMIZATION_TWITTER_USERNAME_DESCRIPTION', 'Please enter your twitter username.');
	$Context->Configuration['CUSTOMIZATION_TWITTER_NO'] = '';
	$Context->SetDefinition('CUSTOMIZATION_TWITTER_NO', 'No\' Of Tweets');
	$Context->SetDefinition('CUSTOMIZATION_TWITTER_NO_DESCRIPTION', 'How many tweets would you like to show? Choose from 1 - 10');
	$Context->SetDefinition('TWITTER_FEED', 'Twitter Feed');
	
	##	ATTACH FEED TO USERS ACCOUNT
	###########################################
	if ($Context->SelfUrl == 'account.php' && ForceIncomingString('PostBackAction', '') == '') {

		## RENDER THE TWEETS
		function Account_Rendertwitter(&$Account) {
			$twitter = FormatHtmlStringInline($Account->User->Customization('CUSTOMIZATION_TWITTER_USERNAME'), 1, 1);
			$twitterno = FormatHtmlStringInline($Account->User->Customization('CUSTOMIZATION_TWITTER_NO'), 1, 1);
			if ($twitter != '') {
				$twitter = urlencode($twitter);
				echo '<script type="text/javascript" src="http://platform.twitter.com/anywhere.js?id=0PlwqRljQPawWAeJwtaDjg&amp;v=1"></script>';
				echo '<script type="text/javascript">';
				echo 'twttr.anywhere(function(twitter) {';
				echo 'twitter.hovercards();';
				echo '});';
				echo '</script>';
				echo '<div id="twitterdiv">';
				echo '<h2><a href="http://www.twitter.com/'.$twitter.'/" target="_blank">'.$twitter.'s '.$Account->Context->GetDefinition('TWITTER_FEED').'</a></h2>';
				
				## HOW MANY TWEETS
				if($twitterno < 1) {
					$twitterno = 5;
				} elseif($twitterno > 10) {
					$twitterno = 10;
				}
				
				## PARSE URLS PL0X
				function autolink($url){
					$url = str_replace("\\r","\r",$url);
					$url = str_replace("\\n","\n<BR>",$url);
					$url = str_replace("\\n\\r","\n\r",$url);
					$in = array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
					$out=array('<a href="$1"  target=\"_blank\" rel=nofollow>$1</a> ','<a href="http://$1" target=\"_blank\" rel=\'nofollow\'>$1</a>');
					return preg_replace($in,$out,$url);
				}
				
				## BUILD THE URL
				$feed ='http://search.twitter.com/search.json?q=from:'.$twitter.'&rpp='.$twitterno.'';
				
				## GRAB THE FEED
				$arr = json_decode(file_get_contents($feed));
				
				foreach ($arr->results as $result) { 
				
					## COVERT TO EPOCH
					$time = strtotime($result->created_at); 
					
					## ECHO OUT THE TWEETS
					echo '<p class="twitter">';
						echo '&#8226; '.autolink($result->text);
					echo '</p>';
				}
				
				echo '</div>';
			}
		}
		$Head->AddStyleSheet( 'extensions/Twitter/theme/style.css');
		$Context->AddToDelegate('Account', 'PostProfileRender', 'Account_Rendertwitter');
	}

?>
