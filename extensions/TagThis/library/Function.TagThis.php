<?php

//Used for adding tags prior to using TagThis
function AutomaticTags()
	{
	global $Context;
	$query = "SELECT DiscussionID,Name FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion`;";
	$resource = $Context->Database->Execute($query,'','','An error occured selecting all discussions.');

	$result = $Context->Database->GetRow($resource);

	while ($result)
		{
		$DiscussionID = $result['DiscussionID'];
		$originaltags = GetDiscussionTags($result['DiscussionID']);
		$tags = GetTitleTags($result['Name']);

		if (is_array($originaltags)) {$tags = array_merge($originaltags, $tags);}

		$tags = array_unique($tags);

		echo "<p>DiscussionID #".$DiscussionID." : ".implode(" ",$tags)."</p>";

		//first delete the old tags
		$query = "DELETE FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` WHERE DiscussionID = '".$DiscussionID."';";
		$result = $Context->Database->Execute($query,'','','An error occured removing tags for this discussions.');

		//now add the new ones
		foreach ($tags as &$tag)
			{
			$tag = trim(strtolower($tag));
			if 	($tag > "")
				{
				$query = "INSERT INTO `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` (`TagName`, `DiscussionID`) VALUES ('".FormatStringForDatabaseInput($tag)."', '".$DiscussionID."');";
				$result = $Context->Database->Execute($query,'','','An error occured adding tags for this discussions.');				
				}
			}

		$result = $Context->Database->GetRow($resource);
		}
	}


//removes common words from discussion titles
function GetTitleTags($title)
	{

	//removes anything that's not alphanumeric
	$title = ereg_replace("[^A-Za-z0-9 ]", "", $title);

	//Found both these lists - one has a lot less than the other, but might be less harsh
	include('CommonList.inc.php');

	$tags = explode(" ", $title);

	//only return tags that aren't common
	foreach ($tags as $tag)
		{
		if (!in_array(strtolower($tag), $commonWords))
			{
			$newtags[] = strtolower($tag);			
			}		
		}

	return $newtags;

	}


//for searching by tag

function SearchByTag(&$DiscussionManager)
	{
	if 	($Tag = ForceIncomingString('Tag', ""))
		{
		$DiscussionManager->DelegateParameters['SqlBuilder']->AddJoin('Tags', 'tag', 'DiscussionID', 't', 'DiscussionID', 'left join', "");
		$DiscussionManager->DelegateParameters['SqlBuilder']->AddWhere('tag', 'TagName', '', FormatStringForDatabaseInput(urldecode($Tag)), '=');
		}
	}

function SearchByTagInfo(&$SearchForm)
	{
	$SearchForm->PageDetails = substr($SearchForm->PageDetails,0, strlen($SearchForm->PageDetails)-16).' tag "'.ForceIncomingString('Tag', "").'"</strong></p>';
	}

//tagging

function SaveTags(&$DiscussionForm)
	{
	global $Context;

	//This means it's a fresh discussion, so we want the title tags
 	if (($DiscussionForm->DelegateParameters['SaveDiscussion']->FirstCommentID == 0) && ($Context->Configuration['TT_TITLE_TAG']))
		{
		$titletags = GetTitleTags($DiscussionForm->DelegateParameters['ResultDiscussion']->Name);
		}

	//only save the tags if saving the discussions was successful
	if ($DiscussionForm->DelegateParameters['ResultDiscussion'])
		{
		//first delete the old tags
		$query = "DELETE FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` WHERE DiscussionID = '".$DiscussionForm->DelegateParameters['ResultDiscussion']->DiscussionID."';";
		$result = $Context->Database->Execute($query,'','','An error occured removing tags for this discussions.');

		//now add the new ones

		$tags = explode(",", (ForceIncomingString('Tags', '')));
		$tags2 = explode(" ", (str_replace(",","",ForceIncomingString('Tags', ''))));

		//have they done it by command or space?
		if (count($tags2) > count($tags)) {$tags = $tags2;}
		
		//add title tags if there are any.
		if (isset($titletags))
			{
			$tags = array_merge ($tags, $titletags);
			}

		$tags = array_unique($tags);

		//no tags entered - hopefully solve a persons issue.
		if 	(!isset($tags))
			{
			return;
			}

		foreach ($tags as &$tag)
			{
			$tag = trim(strtolower($tag));
			if 	($tag > "")
				{
				$query = "INSERT INTO `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` (`TagName`, `DiscussionID`) VALUES ('".FormatStringForDatabaseInput($tag)."', '".$DiscussionForm->DelegateParameters['ResultDiscussion']->DiscussionID."');";
				$result = $Context->Database->Execute($query,'','','An error occured adding tags for this discussions.');				
				}
			}

		}
	}

function GetDiscussionTags($DiscussionID)
	{
	global $Context;
	
	$query = "SELECT TagName FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` as t LEFT JOIN `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion` as d ON t.DiscussionID = d.DiscussionID WHERE t.DiscussionID = '".$DiscussionID."' AND Active = 1;";
	$result = $Context->Database->Execute($query,'','','An error occured getting tags for this discussions.');

	$taglist = "";
	while 	(@$row = $Context->Database->GetRow($result))
		{
		$taglist.= $row['TagName'] . ", ";
		}
	$taglist = substr($taglist, 0, count($taglist)-3);
	return $taglist;
	}

function AddUserTagCloud($UserID)
	{
	global $Context;
	global $Configuration;

	//get discussions this user has started
	$query = "SELECT DiscussionID FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion` WHERE AuthUserID = '".$UserID."';";
	$result = $Context->Database->Execute($query,'','','An error occured getting discussions starts by this user.');

	while 	(@$row = $Context->Database->GetRow($result))
		{
		$Discussions[$row['DiscussionID']] = 1;
		}
	
	//if BlogThis is in use, select discussions where he has a comment blogged in it to get the tags from that discussion
	if 	(array_key_exists('BLOGTHIS', $Configuration))
		{
		$query = "SELECT DiscussionID FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Comment` WHERE AuthUserID = '".$UserID."' AND BlogThis = 1;";
		$result = $Context->Database->Execute($query,'','','An error occured getting discussions blogged by this user.');

		while 	(@$row = $Context->Database->GetRow($result))
			{
			$Discussions[$row['DiscussionID']] = 1;		
			}
		}

	//if there are no tags, display a site wide tag cloud
	if 	(!isset($Discussions))
		{
		AddDiscussionsTagCloud ();
		return;
		}

	//now go through each discussion and get the tags
	foreach ($Discussions as $DiscussionID => $Value)
		{
		$query = "SELECT TagName FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` as t LEFT JOIN `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion` as d ON t.DiscussionID = d.DiscussionID WHERE t.DiscussionID = '".$DiscussionID."' AND Active = 1;";
		$tagresult = $Context->Database->Execute($query,'','','An error occured getting tags for this discussions.');

		while 	(@$tagrow = $Context->Database->GetRow($tagresult))
			{
			if 	(isset($tags[$tagrow['TagName']]))
				{
				$tags[$tagrow['TagName']]++;
				}
			else
				{
				$tags[$tagrow['TagName']] = 1;
				}
			}
		}

	//if there are no tags, display a site wide tag cloud
	if 	(!isset($tags))
		{
		AddDiscussionsTagCloud ();
		return;
		}

	AddTagPanel($tags, 'User');

	}

function AddDiscussionsTagCloud()
	{
	global $Context;
	global $Configuration;

	$query = "SELECT TagName FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` as t LEFT JOIN `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion` as d ON t.DiscussionID = d.DiscussionID WHERE Active = 1;";
	$tagresult = $Context->Database->Execute($query,'','','An error occured getting all tags for this forum.');

	while 	(@$tagrow = $Context->Database->GetRow($tagresult))
		{
		if 	(isset($tags[$tagrow['TagName']]))
			{
			$tags[$tagrow['TagName']]++;
			}
		else
			{
			$tags[$tagrow['TagName']] = 1;
			}
		}

	if (!isset($tags)) {return;}

	AddTagPanel($tags, 'Site');
	}

//for a single discussion
function AddDiscussionTagCloud($DiscussionID)
	{
	global $Context;
	global $Configuration;

	$query = "SELECT t.DiscussionID, TagName FROM `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Tags` as t LEFT JOIN `".$Context->Configuration['DATABASE_TABLE_PREFIX']."Discussion` as d ON t.DiscussionID = d.DiscussionID WHERE Active = 1;";
	$tagresult = $Context->Database->Execute($query,'','','An error occured getting all tags for this forum.');

	while 	(@$tagrow = $Context->Database->GetRow($tagresult))
		{
		if 	(isset($tags[$tagrow['TagName']]))
			{
			$tags[$tagrow['TagName']]++;
			}
		else
			{
			$tags[$tagrow['TagName']] = 1;
			}
		//Is this a tag for this discussion?
		if 	($tagrow['DiscussionID'] == $DiscussionID)
			{
			$tags2[$tagrow['TagName']] = 1;
			}
		}

	//if there are no tags, display a site wide tag cloud
	if 	(!isset($tags2))
		{
		AddDiscussionsTagCloud ();
		return;
		}

	//go through the tags from this discussion and add the weight from the overall

	foreach ($tags2 as $key => &$value)
		{
		$value = $tags[$key];
		}

	AddTagPanel($tags2, 'Discussion');
	}


function AddTagPanel($tags, $tagtype)
	{
	global $Context;
	global $Panel;

	if 	(isset($tags))
		{
		$tagcloud = GetTagCloud($tags);

		if	($tagtype)
			$ListName = $Context->GetDefinition($tagtype . 'TagCloud');
		else
			$ListName = $Context->GetDefinition('TagCloud');

		$Panel->AddString("<h2>".$ListName."</h2><div id=\"TagCloud\">".$tagcloud."</div>", $Context->Configuration['TT_PANEL_POSITION']);
		}
	}

function GetTagCloud($tags)
	{
	global $Configuration;

	//sorts by value to get the highest value
	arsort($tags);
	$maxnumber = each ($tags);
	$maxnumber = $maxnumber['value'];

	if ($Configuration['TT_CLOUD_LIMIT'] > 0)
		{
		$tags = array_slice($tags, 0, $Configuration['TT_CLOUD_LIMIT'], true); 		
		}

	//sort alphabetically
	ksort ($tags);

	$output = "";

	foreach ($tags as $tag => $amount)
		{
		$fontsize = (($Configuration['TT_MAX_FONT'] - $Configuration['TT_MIN_FONT']) * ($amount / $maxnumber)) + $Configuration['TT_MIN_FONT'];
	
		//it looked strange having everything really big if they were the only tags for that topic
		if	($maxnumber == "1")
			{
			$fontsize = "100";
			}

		$output .= '<span style="font-size:'.ceil($fontsize).'%"><a href="'.GetUrl($Configuration, 'search.php', '', '', '', '',  'PostBackAction=Search&Type=Topics&Tag='.urlencode($tag)).'">'.$tag . "</a></span> ";
		}

	$output = substr($output,0,strlen($output)-1);
	return $output;
	}

function AddTagEntry(&$DiscussionForm)
	{
	global $Context;

	$tags = GetDiscussionTags($DiscussionForm->DiscussionID);

	echo '<li>
		<label for="txtTags">'.$Context->GetDefinition('DiscussionTags').'</label>
		<input id="txtTags" type="text" name="Tags" class="DiscussionBox" value="'.$tags.'" />
	</li>';

	}
?>
