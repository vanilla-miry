<?php
$Context->SetDefinition('TagThis Management', 'TagThis');
$Context->SetDefinition('DiscussionTags', 'Tags (Comma Separated)');
$Context->SetDefinition('TagCloud', 'Tag Cloud');
$Context->SetDefinition('SiteTagCloud', 'Site\'s Tag Cloud');
$Context->SetDefinition('UserTagCloud', 'User\'s Tag Cloud');
$Context->SetDefinition('DiscussionTagCloud', 'Discussion\'s Tag Cloud');
$Context->SetDefinition('TTSettings', 'TagThis Settings');
$Context->SetDefinition('Site', 'Site');
?>