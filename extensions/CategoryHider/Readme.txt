Category Hider
--------------

Version 2.0, By WallPhone

Removes select categories from the discussions page

Installation
============

In order for Vanilla to recognize an extension, it must be contained within it's
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/CategoryHider/default.php

Now the extension may be enabled by going to the settings tab, the extensions 
section, and checking the checkbox for the Category Hider extension.

Keep in mind the extension only appears in the panel for any visitor with an 
inactive session.


Configuration
=============

Edit the category you wish to remove, and check the box found near the bottom to
hide that particular category. If the checkbox doesn't appear, ensure you are 
using Vanilla 1.1.4 or later and the default theme. You can switch the theme back
and the categories will remain hidden.  