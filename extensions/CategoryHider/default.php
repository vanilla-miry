<?php
/*
Extension Name: Category Hider
Extension Url: http://www.lussumo.com/addons
Description: Hides certian categories from the discussion grid
Version: 2.0
Author: WallPhone
Author Url: http://wallphone.com/

Changelog
1.0 Unreleased
2.0 Added control panel configuration options
*/

if ( ('index.php' == $Context->SelfUrl) &&
	(isset($Context->Configuration['HiddenCategories'])) &&
	($Context->Configuration['HiddenCategories'] != '0') &&
	(!in_array(ForceIncomingInt('CategoryID', 0), explode(',', $Context->Configuration['HiddenCategories']))) ) {

	function Category_HideFromDiscussions(&$DiscussionManager) {
		$SB = &$DiscussionManager->DelegateParameters['SqlBuilder'];
		$HiddenCategories = explode(',', $SB->Context->Configuration['HiddenCategories']);

		foreach ($HiddenCategories as $CurrentBlock)
			$SB->AddWhere('t', 'CategoryID', '', $CurrentBlock, '<>', 'and', '', 0, 0);
	}
	$Context->AddToDelegate('DiscussionManager', 'PostGetDiscussionBuilder', 'Category_HideFromDiscussions');
	$Context->AddToDelegate('DiscussionManager', 'PreGetDiscussionCount', 'Category_HideFromDiscussions');
}

if ('settings.php' == $Context->SelfUrl) {
	function AddCatOption(&$CF) {
		$Checked = ( (isset($CF->Context->Configuration['HiddenCategories'])) &&
			in_array($CF->Category->CategoryID, explode(',', $CF->Context->Configuration['HiddenCategories'])) )
			? 1 : 0;

		echo  '
		<li><p class="Description">
			<strong>'.$CF->Context->GetDefinition('HiddenCats').'</strong>
			<span>'.
			GetDynamicCheckbox('HideThisCat',
				1,
				$Checked,
				'',
				$CF->Context->GetDefinition('HideThisCat'),
				'',
				'HideThisCat') .'</span>
		</p></li>';
	}
	$Context->AddToDelegate('CategoryForm', 'PostRolesRender', 'AddCatOption');

	function SaveHiddenCatValue(&$CF) {
		$Hiddens = array();

		if (isset($CF->Context->Configuration['HiddenCategories']))
			$Hiddens = explode(',', $CF->Context->Configuration['HiddenCategories']);

		if (ForceIncomingInt('HideThisCat', 0)) {
			$Hiddens[] = $CF->Category->CategoryID;
		} else {
			for ($x = 0; $x < count($Hiddens); $x++)
				if ($Hiddens[$x] == $CF->Category->CategoryID) $Hiddens[$x] = 0;
		}

		$Hiddens = array_unique($Hiddens);
		AddConfigurationSetting($CF->Context, 'HiddenCategories', implode(',', $Hiddens));
	}
	$Context->AddToDelegate('CategoryForm', 'PostSaveCategory', 'SaveHiddenCatValue');
}