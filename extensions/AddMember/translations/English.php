<?php
$Context->SetDefinition('AddMember', 'Add A New Member');
$Context->SetDefinition('AddMemberUserName', 'Username');
$Context->SetDefinition('AddMemberUserNameInfo', 'The username will appear next to the member\'s discussions and comments.');

$Context->SetDefinition('AddMemberFirstName', 'First name');
$Context->SetDefinition('AddMemberFirstNameInfo', 'This should be the member\'s "real" first name. This will only be visible from the account page.');
$Context->SetDefinition('AddMemberLastName', 'Last name');
$Context->SetDefinition('AddMemberLastNameInfo', 'This should be the member\'s "real" last name. This will only be visible from the account page.');
$Context->SetDefinition('AddMemberEmail', 'Email address');
$Context->SetDefinition('AddMemberEmailInfo', 'You must provide a valid email address so that the member can retrieve their password should they lose it and so that they can be notified of their new account details.');
$Context->SetDefinition('AddMemberPassword', 'Password');
$Context->SetDefinition('AddMemberPasswordInfo', 'Do not use birth-dates, bank-card pin numbers, telephone numbers, or anything that can be easily guessed.');
$Context->SetDefinition('AddMemberConfirmPassword', 'Password again');
$Context->SetDefinition('AddMemberConfirmPasswordInfo', 'Re-enter the password to be sure that you have not made any mistakes.');
$Context->SetDefinition('AddMemberInfo', 'Define your new account profile');
$Context->SetDefinition('AddMemberRequired', ' (Required)');
$Context->SetDefinition('AddMemberSendMail', 'Send a confirmation email to the member');

$Context->SetDefinition('AddMemberSuccess', 'A new member\'s account for {UserName} has been created.');

$Context->SetDefinition('AddMemberEmailSubject', 'New Account');
$Context->SetDefinition('AddMemberRole', 'Choose a New Role');
$Context->SetDefinition('AddMemberRoleInfo', 'Choose the required role for this new member. The member will be automatically approved for all roles other than \''.$Context->GetDefinition('Unauthenticated').'\'.');
$Context->SetDefinition('AddMemberRoleNotes', 'Role change notes');
$Context->SetDefinition('AddMemberRoleNotesInfo', 'Please provide some notes regarding this role change. These notes will be visible to all users in the role-history for this user.');
$Context->SetDefinition('AddMemberRoleChange', 'Updated by the AddMember extension.');

$Context->SetDefinition('UserExtensions', 'Account Options');
$Context->SetDefinition('PERMISSION_CREATE_MEMBER', 'Can create a new member');
?>