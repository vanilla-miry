<?php
$Context->SetDefinition('AddMember', 'A&ntilde;adir una nueva persona');
$Context->SetDefinition('AddMemberUserName', 'Identificador');
$Context->SetDefinition('AddMemberUserNameInfo', 'The username will appear next to the member\'s discussions and comments.');

$Context->SetDefinition('AddMemberFirstName', 'Nombre');
$Context->SetDefinition('AddMemberFirstNameInfo', 'This should be the member\'s "real" first name. This will only be visible from the account page.');
$Context->SetDefinition('AddMemberLastName', 'Apellidos');
$Context->SetDefinition('AddMemberLastNameInfo', 'This should be the member\'s "real" last name. This will only be visible from the account page.');
$Context->SetDefinition('AddMemberEmail', 'Direcci&oacute;n de correo electr&oacute;nico');
$Context->SetDefinition('AddMemberEmailInfo', 'You must provide a valid email address so that the member can retrieve their password should they lose it and so that they can be notified of their new account details.');
$Context->SetDefinition('AddMemberPassword', 'Contrase&ntilde;a');
$Context->SetDefinition('AddMemberPasswordInfo', 'Do not use birth-dates, bank-card pin numbers, telephone numbers, or anything that can be easily guessed.');
$Context->SetDefinition('AddMemberConfirmPassword', 'Repetir la contrase&ntilde;a');
$Context->SetDefinition('AddMemberConfirmPasswordInfo', 'Re-enter the password to be sure that you have not made any mistakes.');
$Context->SetDefinition('AddMemberInfo', 'Informaci&oacute;n de la nueva cuenta a crear');
$Context->SetDefinition('AddMemberRequired', ' (Required)');
$Context->SetDefinition('AddMemberSendMail', 'Enviar un correo electr&oacute;nico de confirmaci&oacute;n a la persona a&ntilde;adida');

$Context->SetDefinition('AddMemberSuccess', 'A new member\'s account for {UserName} has been created.');

$Context->SetDefinition('AddMemberEmailSubject', 'Nueva Cuenta');
$Context->SetDefinition('AddMemberRole', 'Elegir el rol');
$Context->SetDefinition('AddMemberRoleInfo', 'Choose the required role for this new member. The member will be automatically approved for all roles other than \''.$Context->GetDefinition('Unauthenticated').'\'.');
$Context->SetDefinition('AddMemberRoleNotes', 'Role change notes');
$Context->SetDefinition('AddMemberRoleNotesInfo', 'Please provide some notes regarding this role change. These notes will be visible to all users in the role-history for this user.');
$Context->SetDefinition('AddMemberRoleChange', 'Updated by the AddMember extension.');

$Context->SetDefinition('UserExtensions', 'Opciones de Cuenta');
$Context->SetDefinition('PERMISSION_CREATE_MEMBER', 'Puede a&ntilde;adir a nuevas personas');
?>