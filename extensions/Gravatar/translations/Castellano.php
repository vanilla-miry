<?php
$Context->SetDefinition('Gravatar_Icon', 'Icono');
$Context->SetDefinition(
	'Gravatar_IconNotes',
	'Aqu&iacute; puedes introducir cualquier URL v&aacute;lida que apunte a una imagen, del tipo:
	<strong>http://www.misitioweb.com/myicono.jpg</strong>. Eso har&aacute; que aparezca ese icono
	junto a tu nombre en los comentarios de las conversaciones y en tu perfil.
	Si tienes alg&uacute;n icono asociado a tu direcci&oacute;n de correo electr&oacute;nico en
	<a href="http://site.gravatar.com/">Gravatar</a>, &eacute;ste ser&aacute; usado de forma
	autom&aacute;tica sin que tengas que hacer nada.'
);
?>