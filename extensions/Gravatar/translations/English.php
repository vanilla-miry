<?php
$Context->SetDefinition('Gravatar_Icon', 'Icon');
$Context->SetDefinition(
	'Gravatar_IconNotes',
	'You can enter any valid URL to an image here, such as: <strong>http://www.mywebsite.com/myicon.jpg</strong>.
	An icon will appear next to your name in discussion comments and on your account page.
	We will try to get your icon from <a href="http://site.gravatar.com/">Gravatar</a>. If an icon
	is associated to your email address on <a href="http://site.gravatar.com/">Gravatar</a>,
	You do not need to enter anything.'
);
?>