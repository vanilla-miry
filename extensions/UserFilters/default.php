<?php
/*
Extension Name: User Filters
Extension Url: http://lussumo.com/addons/
Description: Adds links to the Control Panel of each user's Account page that show that user's discussions and comments
Version: 1.0.2
Author: Aaron Olson
Author Url: http://www.houseblogs.net

You should cut & paste these language definitions into your
conf/your_language.php file (replace "your_language" with your chosen language,
of course):
*/

$Context->Dictionary["UsersFilters"] = "Activity";
$Context->Dictionary["UsersDiscussions"] = "Discussions Started";
$Context->Dictionary["UsersComments"] = "Comments Added";


if ($Context->SelfUrl == "account.php" && ForceIncomingString('PostBackAction', '') == '') {

//Define the account that is being viewed
// Don't reload objects if you don't need to (ie. If another extension has already loaded it)
if (!@$UserManager) $UserManager = $Context->ObjectFactory->NewContextObject($Context, "UserManager");
$AccountUserID = ForceIncomingInt("u", $Context->Session->UserID);
$AccountUser = $UserManager->GetUserById($AccountUserID);

//Define the URLs to search for this User's content   
   $UserDiscussions_SearchString = 'PostBackAction=Search&Type=Discussions&Advanced=1&Keywords=&Categories=&AuthUsername=' . $AccountUser->Name . '&btnSubmit=Search';
   $UserComments_SearchString = 'PostBackAction=Search&Keywords=' . $AccountUser->Name . '%3A&Type=Comments&btnSubmit=Search';

//Customize the Control Panel list title
   $UserFilterTitle = $AccountUser->Name . '\'s ' . $Context->GetDefinition('UsersFilters');


//Add the links to the panel
   $UsersFilters = $UserFilterTitle;
   $DiscussionFilters = $Context->GetDefinition("UsersDiscussionFilters");
   $Panel->AddList($UsersFilters, 0, 1);
   $Panel->AddListItem($UsersFilters, $Context->GetDefinition("UsersDiscussions"), GetUrl($Configuration, "search.php", "", "", "", "", $UserDiscussions_SearchString), "", "", 10);
   $Panel->AddListItem($UsersFilters, $Context->GetDefinition("UsersComments"), GetUrl($Configuration, "search.php", "", "", "", "", $UserComments_SearchString), "", "", 10);
}

?>