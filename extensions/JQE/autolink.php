<?php
# This program is free software and open source software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  or visit
# http://www.gnu.org/licenses/gpl.html

# Based on AutoLinks version 1.5
# http://vanillaforums.org/addon/437/autolinks
# Copyright (C) 2010 Miriam Ruiz <miriam@debian.org>
# Copyright (C) 2009 Tim Marston <edam@waxworlds.org>
# Copyright (C) 2008 Gambit

class AutoLinksHelper
  {

    //
    // ACCEPTED PROTOTYPES
    //
    var $accepted_protocols = array(
      'http://', 'https://', 'ftp://', 'ftps://', 'mailto:', 'telnet://',
      'news://', 'nntp://', 'nntps://', 'feed://', 'gopher://', 'sftp://' );

    //
    // AUTO-EMBED IMAGE FORMATS
    //
    var $accepted_image_formats = array(
      'gif', 'jpg', 'jpeg', 'tif', 'tiff', 'bmp', 'png', 'svg', 'ico' );

    function AutoLink( $link )
    {
      // $link[0] = the complete URL
      // $link[1] = link prefix, lowercase (e.g., 'http://')
      // $link[2] = URL up to, but not including, the ?
      // $link[3] = URL params, including initial ?

      // sanitise input
      $link[1] = strtolower( $link[1] );
      if( !isset( $link[3] ) ) $link[3] = '';

      // check protocol is allowed
      if( !in_array( $link[1], $this->accepted_protocols ) ) return $link[0];

      // check for forced-linking and strip prefix
      $forcelink = substr( $link[0], 0, 1 ) == '!';
      if( $forcelink ) $link[0] = substr( $link[0], 1 );

      $params = array();
      $matches = array();

      if( !$forcelink && ( $link[1] == 'http://' || $link[1] == 'https://' ) )
      {
        // images
        if( ( preg_match( '/\.([a-z]{1,5})$/i', $link[2], $matches ) && in_array( strtolower( $matches[1] ), $this->accepted_image_formats ) ) ||
	    ( preg_match( '/\.([a-z]{1,5})$/i', $link[3], $matches ) && in_array( strtolower( $matches[1] ), $this->accepted_image_formats ) ) )
          return '<img class="auto-embedded" src="'.$link[1].$link[2].$link[3].'" />';
        // mp3s
        else if( strtolower( substr( $link[2], -4 ) ) == '.mp3' )
          return '<embed width="240" height="24" src="http://skreemr.com/audio/player.swf" type="application/x-shockwave-flash" flashvars="playerID=1&soundFile='.$link[0].'" class="auto-embedded" />';
        // youtube
        else if( strcasecmp( 'www.youtube.com/watch', $link[2] ) == 0 && $this->Params( $params, $link[3], 'v' ) )
          return '<embed src="'.$link[1].'www.youtube.com/v/'.$params['v'].'&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="448" height="361" class="auto-embedded" />';
        // google video
        else if( preg_match( '/^(video\.google\.co(?:m|\.uk))\/videoplay$/i', $link[2], $matches ) && $this->Params( $params, $link[3], 'docid' ) )
          return '<embed id="VideoPlayback" style="width:448px;height:362px" allowfullscreen="true" src="'.$link[1].$matches[1].'/googleplayer.swf?docid='.$params['docid'].'&fs=true" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" class="auto-embedded" />';
        // vimeo
        else if( preg_match( '/^(?:www\.)?vimeo\.com\/([^\/]+)/i', $link[2], $matches ) )
          return '<embed src="http://vimeo.com/moogaloop.swf?clip_id='.$matches[1].'&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="448" height="336" class="auto-embedded" />';
        // metacafe
        else if( preg_match( '/^www\.metacafe\.com\/watch\/([0-9]+)\/([^\/]+)\/?$/', $link[2], $matches ) )
          return '<embed src="http://www.metacafe.com/fplayer/'.$matches[1].'/'.$matches[2].'.swf" width="400" height="345" wmode="transparent" type="application/x-shockwave-flash" allowfullscreen="true" class="auto-embedded" />';
        // dailymotion
        else if( preg_match( '/^www\.dailymotion\.com\/(?:[a-z]+\/)?video\/([^\/]+)/i', $link[2], $matches ) )
          return '<embed src="http://www.dailymotion.com/swf/'.$matches[1].'" type="application/x-shockwave-flash" width="448" height="357" allowfullscreen="true" allowscriptaccess="always" class="auto-embedded" />';
        // myspace
        else if( preg_match( '/^(?:(?:www|vids)\.)?myspace\.com\/index\.cfm$/', $link[2] ) && $this->Params( $params, $link[3], array( 'fuseaction', 'VideoID' ) ) && $params['fuseaction'] == 'vids.individual' )
          return '<embed src="http://lads.myspace.com/videos/vplayer.swf" flashvars="m='.$params['VideoID'].'&v=2&type=video" type="application/x-shockwave-flash" width="430" height="346" class="auto-embedded" />';
        // spike
        else if( preg_match( '/^[a-z]+\.spike\.com\/video\/(?:.+\/)?([-0-9a-z]+)$/', $link[2], $matches ) )
          return '<embed width="448" height="365" src="http://www.spike.com/efp" type="application/x-shockwave-flash" flashvars="flvbaseclip='.$matches[1].'" allowfullscreen="true" class="auto-embedded" />';
        // redlasso
        else if( strcasecmp( 'redlasso.com/ClipPlayer.aspx', $link[2] ) == 0 && $this->Params( $params, $link[3], 'id' ) )
          return '<embed src="http://media.redlasso.com/xdrive/WEB/vidplayer_1b/redlasso_player_b1b_deploy.swf" flashvars="embedId='.$params['id'].'" width="390" height="320" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" class="auto-embedded" />';
        // onsmash.com
        else if( preg_match( '/^videos\.onsmash\.com\/v\/([a-z0-9A-Z]+)/', $link[2], $matches ) )
          return '<embed src="http://videos.onsmash.com/e/'.$matches[1].'" type="application/x-shockwave-flash" allowfullscreen="true" width="448" height="374" class="auto-embedded" />';
        // tangle.com
        else if( strcasecmp( 'www.tangle.com/view_video.php', $link[2] ) == 0 && $this->Params( $params, $link[3], 'viewkey' ) )
          return '<embed src="http://www.tangle.com/flash/swf/flvplayer.swf" type="application/x-shockwave-flash" allowscriptaccess="always" flashvars="viewkey='.$params['viewkey'].'" wmode="transparent" width="330" height="270" class="auto-embedded" />';
        // LiveLeak.com
        else if( strcasecmp( 'www.liveleak.com/view', $link[2] ) == 0 && $this->Params( $params, $link[3], 'i' ) )
          return '<embed src="http://www.liveleak.com/e/'.$params['i'].'" type="application/x-shockwave-flash" wmode="transparent" width="450" height="370" class="auto-embedded" />';
	// FAIRTILIZER
	else if( preg_match( '/^fairtilizer.com\/(track|playlist)\//', $link[2] ) )
	  return '<iframe scrolling="no" frameborder="0" width="160" height="40" src="http://'.$link[2].'?fairplayer=small" class="auto-embedded"></iframe>';
      }

      // default to linkifying
      return '<a href="'.$link[0].'" rel="nofollow" class="auto-embedded" >'.$link[0].'</a>';
    }

    function Params( &$params, $string, $required )
    {
      if( !is_array( $required ) ) $required = array( $required );
      if( substr( $string, 0, 1 ) == '?' ) $string = substr( $string, 1 );
      $params = array();
      $bits = split( '&', $string );
      foreach( $bits as $bit ) {
        $pair = split( '=', $bit, 2 );
        if( in_array( $pair[0], $required ) ) $params[ $pair[0] ] = $pair[1];
      }
      return count( $required ) == count( $params );
    }

    var $ignore;

    function Separate($matches) { // here's the callback function
        if ($matches[1]) {             // Case 1: this is a HTMLTAG
            if (preg_match('/^\S*[aA]\b/', $matches[1]))
                $this->ignore = true;
            else
                $this->ignore = false;
            return $matches[1];        // return HTMLTAG unmodified
        }
        elseif (isset($matches[2])) {  // Case 2: a non-HTMLTAG chunk
            // quick check to rule out complete wastes of time
            if( $this->ignore === true)
                return $matches[2];        // return non-HTMLTAG unmodified
            if( strpos( $matches[2], '://' ) === false && strpos( $matches[2], 'mailto:' ) === false )
                return $matches[2];        // return non-HTMLTAG unmodified
            return preg_replace_callback( '/(?<=^|\r\n|\n| |\t|<br>|<br\/>|<br \/>)!?([a-z]+:(?:\/\/)?)([^ <>"\r\n\?]+)(\?[^ <>"\r\n]+)?/i', array( $this, 'AutoLink' ), $matches[2] );
        }
    }

    function AddLinks( $string )
    {
        $this->ignore = false;
        $re = '%(</?\w++[^<>]*+>)' . //grab HTML open or close TAG into group 1
          '|' . // or
          '([^<]*+(?:(?!</?\w++[^<>]*+>)<[^<]*+)*+)' . //grab non-HTMLTAG text into group 2
          '%x';
        // walk through the content, chunk, by chunk, replacing keywords in non-NTMLTAG chunks only
        return preg_replace_callback( $re, array( $this, 'Separate' ), $string );
    }

  }
?>