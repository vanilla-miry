<?php
/*
Extension Name: JQE WYSIWYG editor
Extension Url: http://vanillaforums.org/get/487
Description: Add a WYSIWYG editor to the comment form using Jquery JQE
Version: 1.0.0
Author: Cyril Russo
Author Url: stage.nexvision at laposte.net

*/

if (!defined('IN_VANILLA')) exit();
define('JQE_PATH', dirname(__FILE__) . '/');
require(JQE_PATH . 'settings.php');

require(JQE_PATH . 'autolink.php');

if (!function_exists('kses')) require(JQE_PATH . 'kses.php');
// Check to see if user is allowed to comment
$Configuration["JQE_LOGINREQUIRED"] = false;          	//if u are using Add Comments Extension set this to false
if( $Configuration["JQE_LOGINREQUIRED"]===false or $Context->Session->UserID > 0 )
{
	//If user is allowed to comment then add JQE javasript
	if ( in_array($Context->SelfUrl, array("post.php", "comments.php")) ) 
    {
        includeJQuery();
        $Head->AddScript('extensions/JQE/jquery.rte.js');
        $Head->AddStyleSheet('extensions/JQE/rte.css');

		$Head->AddString('
			<script type="text/javascript">
                jQuery(document).ready(function($){
                    $("#CommentBoxController").hide();
                    $("#CommentFormats").hide();
                    $("#CommentBox").rte({
                        content_css_url: "'. $Context->Configuration['WEB_ROOT'].'extensions/JQE/rte.css'.'",
                        media_url: "'. $Context->Configuration['WEB_ROOT'].'extensions/JQE/'.'",
                    });
                    $("#abshosturls").rte({
                        content_css_url: "'. $Context->Configuration['WEB_ROOT'].'extensions/JQE/rte.css'.'",
                        media_url: "'. $Context->Configuration['WEB_ROOT'].'extensions/JQE/'.'",
                    });                    
                });
			</script>');
            
	}
}

//add Kses formater

class JQEFormatter extends StringFormatter {
	var $allowed_tags;
	var $allowed_protocols;
	var $autolink;

	function JQEFormatter($tags, $protocols) 
    {
		$this->allowed_tags = $tags;
		$this->allowed_protocols = $protocols;
		$this->autolink = new AutoLinksHelper();
	}

	function Parse($String, $Object, $FormatPurpose) 
    {
		if ($FormatPurpose == FORMAT_STRING_FOR_DISPLAY) 
	{
			return $this->autolink->AddLinks(kses($String, $this->allowed_tags, $this->allowed_protocols));
		}
		return $String;
	}
}

if ( $Context->Session->UserID > 0 && $Context->Session->User->Permission('PERMISSION_HTML_ALLOWED') ) {
	//Make JQE formatter the only formatter available to post a new comment or to edit an old one
	$Context->Configuration['DEFAULT_FORMAT_TYPE'] = 'JQE';
	$Context->Session->User->DefaultFormatType = 'JQE';
	$Context->Session->User->Preferences['ShowFormatSelector'] = 0;
}
$JQEFormatter = $Context->ObjectFactory->NewObject($Context, "JQEFormatter", $JQE_allowed_tags, $JQE_allowed_protocols);
$Context->StringManipulator->AddManipulator("JQE", $JQEFormatter);
?>