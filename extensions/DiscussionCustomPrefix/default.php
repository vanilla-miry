<?php
/*
Extension Name: Discussion Custom Prefix
Extension Url: http://lussumo.com/addons/
Description: Custom prefix for discussion.
Version: 0.01
Author: Roman "S" Vasiliev
Author Url: http://lussumo.com/community/account/8576/
*/

if(!defined('IN_VANILLA')) die();

define('DCP_VERSION', 0.01);
define('DCP_PATH', $Configuration['EXTENSIONS_PATH'].'DiscussionCustomPrefix/');
define('DCP_ROOT', $Configuration['WEB_ROOT'].'extensions/DiscussionCustomPrefix/');
define('DCP_IMAGES', DCP_ROOT.'icons/');

$DatabaseColumns['Discussion']['CustomPrefixID'] = 'CustomPrefixID';

@$_VERSION = FloatVal($Configuration['DCP_VERSION']);
if($_VERSION != DCP_VERSION) require DCP_PATH.'setup.php';

switch($Context->SelfUrl){
	case 'post.php':{
		$Context->AddToDelegate('DiscussionForm', 'DiscussionForm_PreTopicRender', 'DCP_DiscussionForm_PreTopicRender');
		$Context->AddToDelegate('DiscussionManager', 'PreSaveDiscussion', 'DCP_PreSaveDiscussion');
		break;
	}
	case 'index.php':{
		$Context->AddToDelegate('DiscussionGrid', 'Constructor', 'DCP_SetDelegateParameter');
		$Context->AddToDelegate('DiscussionGrid', 'PreDiscussionOptionsRender', 'DCP_PreDiscussionOptionsRender');
		break;
	}
	case 'comments.php':{
		$Context->AddToDelegate('CommentGrid', 'Constructor', 'DCP_SetDelegateParameter');
		$Context->AddToDelegate('CommentGrid', 'PreRender', 'DCP_CommentsPreRender');
		break;
	}
	default: return;
}

$Context->AddToDelegate('DiscussionManager', 'PostGetDiscussionBuilder', 'DCP_PostGetDiscussionBuilder');
$Context->AddToDelegate('Discussion', 'PostGetPropertiesFromDataSet', 'DCP_PostGetPropertiesFromDataSet');
$Head->AddStyleSheet('extensions/DiscussionCustomPrefix/style.css');

function AddDiscussionCustomPrefixSelectStyles($Head) {
	include DCP_PATH.'config.php';
	$ListStyles='';
	while(list($ID, $Info) = each($Config))
		$ListStyles .= '.LargeSelect option.CustomPrefix_'.$ID.':before {content: url(extensions/DiscussionCustomPrefix/icons/'.$Info['img'].') !important;padding-right:10px;}'."\n";
	$Head->AddString("<style type=\"text/css\">\n".$ListStyles."</style>\n");
}

if ($Context->SelfUrl == 'post.php')
	AddDiscussionCustomPrefixSelectStyles($Head);

function GetDiscussionCustomPrefixSelect(&$Context, $Selected = 0){
	$S = $Context->ObjectFactory->NewContextObject($Context, 'Select');
	$S->Name = 'DiscussionCustomPrefix';
	$S->SelectedValue = $Selected;
	include DCP_PATH.'config.php';
	$S->AddOption(0, $Context->GetDefinition('SelectOptionNoPrefix'));
	while(list($ID, $Info) = each($Config))
		$S->AddOption($ID, $Info['name'], ' class="CustomPrefix_'.$ID.'"');
	return $S;
}

function DCP_DiscussionForm_PreTopicRender(&$DiscussionForm){
	$Context =& $DiscussionForm->Context;
	$CustomPrefixID = @$DiscussionForm->Discussion->CustomPrefixID;
	$Text = $Context->GetDefinition('LabelTextDiscussionPrefix');
	$Label = "<label for='DiscussionCustomPrefix'>$Text</label>";
	$Select = GetDiscussionCustomPrefixSelect($Context, $CustomPrefixID)->Get();
	printf('<li>%s%s</li>', $Label, $Select);
}

function DCP_SetDelegateParameter(&$UnknownGrid){
	include DCP_PATH.'config.php'; // $Config
	$UnknownGrid->DelegateParameters['CustomPrefixConfig'] = $Config;
}

function DCP_PostGetDiscussionBuilder(&$DiscussionManager){
	$SqlBuilder =& $DiscussionManager->DelegateParameters['SqlBuilder'];
	$SqlBuilder->AddSelect('CustomPrefixID', 't');
}

function DCP_PostGetPropertiesFromDataSet(&$Discussion){
	$DataSet =& $Discussion->DelegateParameters['DataSet'];
	@$Discussion->CustomPrefixID = ForceInt($DataSet['CustomPrefixID'], 0);
}

function DCP_PreSaveDiscussion(&$DiscussionManager){
	$SqlBuilder =& $DiscussionManager->DelegateParameters['SqlBuilder'];
	$CustomPrefixID = ForceIncomingInt('DiscussionCustomPrefix', 0);
	$SqlBuilder->AddFieldNameValue('CustomPrefixID', $CustomPrefixID);
}

function DCP_PreDiscussionOptionsRender(&$DiscussionGrid){
	$Discussion =& $DiscussionGrid->DelegateParameters['Discussion'];
	$DiscussionList =& $DiscussionGrid->DelegateParameters['DiscussionList'];
	$CustomPrefixConfig =& $DiscussionGrid->DelegateParameters['CustomPrefixConfig'];
	$PrefixInfo = @$CustomPrefixConfig[$Discussion->CustomPrefixID];
	if($PrefixInfo)
		$DiscussionList .= '<img class="CustomPrefix" src="'.DCP_IMAGES.$PrefixInfo['img'].'" alt="'.$PrefixInfo['name'].'"> ';
}

function DCP_CommentsPreRender(&$CommentGrid){
	$Discussion =& $CommentGrid->Discussion;
	$CustomPrefixConfig =& $CommentGrid->DelegateParameters['CustomPrefixConfig'];
	$PrefixInfo = @$CustomPrefixConfig[$Discussion->CustomPrefixID];
	if($PrefixInfo)
		$Discussion->Name = '<img class="CustomPrefix" src="'.DCP_IMAGES.$PrefixInfo['img'].'" alt="'.$PrefixInfo['name'].'"> '.$Discussion->Name;
}
?>