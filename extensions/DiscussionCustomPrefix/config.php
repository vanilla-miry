<?php
# $Config[ID] = array(Name, URL, Width)
# DCP_IMAGES = /extensions/DiscussionCustomPrefix/images/

$Config[1]  = array('name' => 'Charla',        'img' => 'chat.png');
$Config[2]  = array('name' => 'Noticia',       'img' => 'news.png');
$Config[3]  = array('name' => 'Artículo',      'img' => 'article.png');
$Config[4]  = array('name' => 'Aviso',         'img' => 'warning.png');
$Config[5]  = array('name' => 'Pregunta',      'img' => 'question.png');
$Config[6]  = array('name' => 'Evento',        'img' => 'calendar.png');
$Config[7]  = array('name' => 'Ciencia',       'img' => 'lab.png');
$Config[8]  = array('name' => 'Cita',          'img' => 'quote.png');
$Config[9]  = array('name' => 'Datos',         'img' => 'data.png');
$Config[10] = array('name' => 'Técnico',       'img' => 'tech.png');
$Config[11] = array('name' => 'En proceso',    'img' => 'gears.png');
$Config[12] = array('name' => 'Pensamiento',   'img' => 'mind.png');
$Config[13] = array('name' => 'Conflicto',     'img' => 'swords.png');
$Config[14] = array('name' => 'Tema caliente', 'img' => 'fire.png');
$Config[15] = array('name' => 'Provocación',   'img' => 'finger.png');
$Config[16] = array('name' => 'Basurilla',     'img' => 'trash.png');

$Config[20] = array('name' => 'Femenino',      'img' => 'female.png');
$Config[21] = array('name' => 'Masculino',     'img' => 'male.png');
$Config[22] = array('name' => 'Transgénero',   'img' => 'transgender.png');
$Config[23] = array('name' => 'Lesbianas',     'img' => 'lesbian.png');
$Config[24] = array('name' => 'Gays',          'img' => 'gay.png');
$Config[25] = array('name' => 'Bisexual',      'img' => 'bisexual.png');
$Config[26] = array('name' => 'Pansexual',     'img' => 'pansexual.png');
$Config[27] = array('name' => 'Asexual',       'img' => 'asexual.png');
$Config[28] = array('name' => 'Orgullo',       'img' => 'pride.png');

$Config[50] = array('name' => 'Película',      'img' => 'film.png');
$Config[51] = array('name' => 'Libro',         'img' => 'book.png');
$Config[52] = array('name' => 'Música',        'img' => 'music.png');
$Config[53] = array('name' => 'Pintura',       'img' => 'paint.png');
$Config[54] = array('name' => 'Internet',      'img' => 'at.png');

$Config[60] = array('name' => 'Correcto',      'img' => 'tick.png');
$Config[61] = array('name' => 'Erróneo',       'img' => 'cross.png');
$Config[62] = array('name' => 'Bien',          'img' => 'up.png');
$Config[63] = array('name' => 'Mal',           'img' => 'down.png');
$Config[64] = array('name' => 'Esperar',       'img' => 'wait.png');

$Config[80] = array('name' => 'Universo',      'img' => 'planet.png');
$Config[81] = array('name' => 'Mundo',         'img' => 'world.png');
$Config[82] = array('name' => 'Europa',        'img' => 'europe.png');
$Config[83] = array('name' => 'España',        'img' => 'spain.png');
$Config[84] = array('name' => 'Asturias',      'img' => 'asturias.png');


$Config[90] = array('name' => 'Alegre',        'img' => 'smiley-happy.png');
$Config[91] = array('name' => 'Triste',        'img' => 'smiley-sad.png');
$Config[92] = array('name' => 'Risas',         'img' => 'smiley-laugh.png');
$Config[93] = array('name' => 'Sorpresa',      'img' => 'smiley-surprised.png');
$Config[94] = array('name' => 'Guiño',         'img' => 'smiley-wink.png');
$Config[95] = array('name' => 'Lengua',        'img' => 'smiley-tongue.png');

$Config[100] = array('name' => 'Información',  'img' => 'information.png');
$Config[101] = array('name' => 'Copyright',    'img' => 'copyright.png');
$Config[102] = array('name' => 'Copyleft',     'img' => 'copyleft.png');
$Config[103] = array('name' => 'Paz',          'img' => 'peace.png');
$Config[104] = array('name' => 'Anarquía',     'img' => 'anarchy.png');
$Config[105] = array('name' => 'Pirata',       'img' => 'pirate.png');
$Config[106] = array('name' => 'Heart-a-gram', 'img' => 'heartagram.png');
$Config[107] = array('name' => 'Unicornio Rosa Invisible', 'img' => 'ipu.png');
$Config[108] = array('name' => 'Yin-Yang',     'img' => 'yinyang.png');
$Config[109] = array('name' => 'Arco Iris',    'img' => 'rainbow.png');
$Config[110] = array('name' => 'Mujer',        'img' => 'woman.png');
$Config[111] = array('name' => 'Hombre',       'img' => 'man.png');
$Config[112] = array('name' => 'Beso',         'img' => 'kiss.png');
$Config[113] = array('name' => 'Rosa',         'img' => 'rose.png');
$Config[114] = array('name' => 'Trébol',       'img' => 'clover.png');
$Config[115] = array('name' => 'Cerveza',      'img' => 'beer.png');
$Config[116] = array('name' => 'Perro',        'img' => 'dog.png');
$Config[117] = array('name' => 'Casa',         'img' => 'house.png');
$Config[118] = array('name' => 'Viaje',        'img' => 'travel.png');
$Config[119] = array('name' => 'Candado',      'img' => 'lock.png');
$Config[120] = array('name' => 'Ordenador',    'img' => 'computer.png');
$Config[121] = array('name' => 'Portátil',     'img' => 'laptop.png');
$Config[122] = array('name' => 'Pliego',       'img' => 'attachment.png');
$Config[123] = array('name' => 'Cohete',       'img' => 'rocket.png');
$Config[124] = array('name' => 'Alien',        'img' => 'alien.png');
$Config[125] = array('name' => 'Persona',      'img' => 'profile.png');
$Config[126] = array('name' => 'Grupo',        'img' => 'group.png');
$Config[127] = array('name' => 'Mujer-Mujer',  'img' => 'woman-woman.png');
$Config[128] = array('name' => 'Mujer-Hombre', 'img' => 'woman-man.png');
$Config[129] = array('name' => 'Hombre-Hombre','img' => 'man-man.png');

$Config[150] = array('name' => 'Linux',        'img' => 'linux.png');
$Config[152] = array('name' => 'Open Source',  'img' => 'opensource.png');
$Config[155] = array('name' => 'Debian',       'img' => 'debian.png');
$Config[156] = array('name' => 'Ubuntu',       'img' => 'ubuntu.png');
$Config[160] = array('name' => 'Mozilla',      'img' => 'mozilla.png');

$Config[180] = array('name' => 'Google',       'img' => 'google.png');
$Config[181] = array('name' => 'Facebook',     'img' => 'facebook.png');
$Config[182] = array('name' => 'Tuenti',       'img' => 'tuenti.png');
$Config[183] = array('name' => 'Technorati',   'img' => 'technorati.png');

$Config[190] = array('name' => 'Barrapunto',   'img' => 'barrapunto.png');
$Config[191] = array('name' => 'Menéame',      'img' => 'meneame.png');

?>