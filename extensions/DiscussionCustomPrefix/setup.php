<?php
// ALTER TABLE `LUM_Discussion` ADD `CustomPrefixID` TINYINT UNSIGNED NOT NULL AFTER `CategoryID` ;
// ALTER TABLE `LUM_Discussion` DROP `CustomPrefixID` 
// SHOW [FULL] COLUMNS FROM tbl_name [FROM db_name] [LIKE 'pattern']
// GetTableName('Role', $DatabaseTables, $Configuration["DATABASE_TABLE_PREFIX"]) 

if(!defined('DCP_VERSION')) die();

$DB =& $Context->Database;
$EM =& $Context->ErrorManager;

if($_VERSION == 0){
	$Discussion = GetTableName('Discussion', $DatabaseTables, $Configuration['DATABASE_TABLE_PREFIX']);
	$Query = "ALTER TABLE {$Discussion} ADD CustomPrefixID TINYINT UNSIGNED NOT NULL default 0 AFTER CategoryID";
	$DB->Execute($Query, 'DiscussionCustomPrefix', 'Install', $Query, 1);
}

if($EM->ErrorCount > 0) $EM->Write($Context);
AddConfigurationSetting($Context, 'DCP_VERSION', DCP_VERSION);
?>