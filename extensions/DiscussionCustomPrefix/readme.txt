EXTENSION INSTALLATION INSTRUCTIONS
===================================
In order for Vanilla to recognize an extension, it must be contained within its
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/DiscussionCustomPrefix/default.php

Once this is complete, you can enable the extension through the "Manage
Extensions" form on the settings tab in Vanilla.

No need to upload _screenshots directory and readme.txt file!

EXTENSION UPDATE
===================================
Don't upload config.php file!

ABOUT
===================================
Allows users to select (image) prefix for discussion.
See example at http://forums.beyondunreal.com/forumdisplay.php?f=101
Useful if in one category discussing things which are different one from each.
For example, the category name is "Windows", prefix can be: "Windows XP", "Vista", "Windows 7".
Another example, the category name is "Unreal Tournament", prefixes: "UT 99", "UT 2004", "UT 3".

SETUP
===================================
config.php - this file contains info about all prefixes.
Format:
$Config[ID] = array(Name, URL, Width)
where ID - prefix ID (0..255)
Name - name, it will displayed in discussion form while creating new (or edit) discussion.
URL - url to image
Width - width of image

style.css - this file holds style for display prefix.
In order to look nice image in different vanilla themes may be need to edit this stylesheet file.
.CustomPDG - used in discussions list page
.CustomPrefixName - used in comments page (shows once at top page)
In most cases to obtain nice looking image with discussion name you need edit
'margin-top' value (in class .CustomPDG)
and 'top' value (in class .CustomPrefixName)

LINKS
===================================
Here some links. Here you can make nice buttons online.

80x15 Brilliant Button Maker: http://www.lucazappa.com/brilliantMaker/buttonImage.php
Button Maker: http://www.kalsey.com/tools/buttonmaker/
The Button Effect: http://www.thebuttoneffect.com/
Another Button Maker: http://tools.blogflux.com/buttonmaker/

HELP
===================================
Need retranslate this readme to English ;)

TODO
===================================
- LinkToCategory
- Search prefix

CHANGELOG
===================================
* v0.01 (18 Apr 2009)
- first release