<?php
$ShowIcon = ($ParticipantIcon != '' && $this->Context->Session->User->Preference('HtmlOn'));
$ParticipantList .= '<li class="UserAccount'.($Alternate ? ' Alternate' : '').($FirstRow?' FirstUser':'').'">
	<ul class="Participant">
		<li class="User Name'.($ShowIcon?' WithIcon':'').'">';
			if ($ShowIcon) $ParticipantList .= '
            <div class="UserIcon" style="'."background-image:url('".$ParticipantIcon."');\">&nbsp;</div>";
			$ParticipantList .= '
			<span>'.$this->Context->GetDefinition('Participant').'</span>
            <a href="'.GetUrl($this->Context->Configuration, 'account.php', '', 'u', $ParticipantID).'">'.$ParticipantName.'</a> - 
            '.FormatPlural($ParticipantCount, $Comment, $Comments).' '.$ParticipantCount.'
		</li>
	</ul>
</li>';
?>