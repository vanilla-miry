<?php
/*
Extension Name: Show Participants
Extension Url: http://lussumo.com/addons/
Description: This extension shows in the discussion-page's Panel all users that have participated. Also allows to add a "Show All" link redirecting to an extension page which shows the entire list of participants.
Version: 0.5
Author: miquel
Author Url: N/A

Extension Change log:

0.5  - 01.05.2008
    -class definition excluded from the source code, moved to /library as a standalone file
    -SetList extension used to generate the settings form
    -added option to count also whispered comments to the user (via user preference), requested by fysicsluvr.
    It only applies when the system have whispers enabled

0.4  - 27.03.2008
    -bug detected when working with whispers and deleted comments, which were
     also counted as participations. Solved
    -included a "Back" link in the "Show All Participants" page, redirecting back
     to the discussion page

0.3  - 17.03.2008
    -included "Show all" link, redirecting to an extension page showing the
     entire list of participants, subdivided in pages when needed, of length
     SEARCH_RESULTS_PER_PAGE from Configuration
    -the "All Participants" page also shows users icons
    -added a CSS trick to left-align the extension page title in vanilla theme
    -used FormatPlural in the comments label to produce more elegant code
    -used AddLimit to the SQL query to be more efficient in memory

0.2  - 10.03.2008
    -list of participants can be sorted (via configuration settings) by username
     or by amount of participations on the discussion (also alphabetically on ties)
    -list name can be changed (via configuration settings)
    -list length can be limited (via configuration settings)
    -settings added on /conf/settings.php

0.1  - 07.03.2008
    -first release
*/

// Default Configuration Settings
if (!array_key_exists('EXT_SHOWPARTICIPANTS_LISTNAME', $Configuration)) {
    // The list's name
    AddConfigurationSetting($Context, 'EXT_SHOWPARTICIPANTS_LISTNAME', 'Participants');
}
if (!array_key_exists('EXT_SHOWPARTICIPANTS_ORDER', $Configuration)) {
    // The ordering method: 1 -> Username, 0 -> #contributions
    AddConfigurationSetting($Context, 'EXT_SHOWPARTICIPANTS_ORDER', '1');
}
if (!array_key_exists('EXT_SHOWPARTICIPANTS_LENGTH', $Configuration)) {
    // The length of the displayed list. -1 for showing all the participants
    AddConfigurationSetting($Context, 'EXT_SHOWPARTICIPANTS_LENGTH', '-1');
}
if (!array_key_exists('EXT_SHOWPARTICIPANTS_SHOWALLLINK', $Configuration)) {
    // Add a "Show all" link at the bottom of the list or not
    // only needed when length != -1
    AddConfigurationSetting($Context, 'EXT_SHOWPARTICIPANTS_SHOWALLLINK', '0');
}

// The extension shows the list only in a discussion page, i.e. in comments.php
if ($Context->SelfUrl == "comments.php") {
    // Fetching the current discussion to be seen
    $DiscussionID = ForceIncomingInt("DiscussionID", 0);
    // Creating the participants list in the panel
    $ParticipantsList = $Context->Configuration['EXT_SHOWPARTICIPANTS_LISTNAME'];
    $Panel->AddList($ParticipantsList, 10);

    // Getting the list length limit
    // A limit value of '-1' means building the whole participants list
    $limit = $Context->Configuration['EXT_SHOWPARTICIPANTS_LENGTH'];

    // Building the sql query to fetch the participants in the discussion
    $Sql = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
    $Sql->SetMainTable('User', 'u');
    $Sql->AddJoin('Comment', 'c', 'AuthUserID', 'u', 'UserID', 'inner join');
    $Sql->AddSelect(array('Name', 'UserID'), 'u');
    $Sql->AddSelect('CommentID', 'c', 'Count', 'count');
    $Sql->AddWhere('c', 'DiscussionID', '', $DiscussionID, '=');
    if($Context->Configuration['ENABLE_WHISPERS'] && $Context->Session->User->Preference('WhispersInParticipants')) {
        $Sql->AddWhere('c', 'WhisperUserID', '', $Context->Session->UserID, '=');
    } else {
        $Sql->AddWhere('c', 'WhisperUserID', '', 0, '=');
    }
    // Excluding deleted comments
    $Sql->AddWhere('c', 'Deleted', '', 0, '=');
    // Grouping every user
    $Sql->AddGroupBy('UserID', 'u');
    // Selecting the ordering method: 1 -> Username, 0 -> #contributions
    if ($Context->Configuration['EXT_SHOWPARTICIPANTS_ORDER'])
        $Sql->AddOrderBy('Name','u');
    else {
        $Sql->AddOrderBy(array('Count'), array(''), 'desc');
        $Sql->AddOrderBy('Name','u');
    }
    // Adding the limit when needed
    if ($limit != -1)
        $Sql->AddLimit(0, $limit);

    // Sending the query to the DB
    $Participants = $Context->Database->Select($Sql, 'ShowParticipantsExtension', '',
                                               $Context->GetDefinition("ParticipantsErrorFetching"));

    // Getting some language definitions
    $Comments = $Context->GetDefinition("ParticipationPlural");
    $Comment = $Context->GetDefinition("ParticipationSingle");

    // Building the participants list, each participant from a row of the DB query
    while ($Participant = $Context->Database->GetRow($Participants)) {
        // Collecting the fields retrieved from the DB related to the participants
        $ParticipantName = ForceString($Participant['Name'], '');
        $ParticipantID = ForceInt($Participant['UserID'], 0);
        $ParticipantCount = ForceInt($Participant['Count'], 0);
        // Building each participant entry on the list. It just differs on appending
        // "comment" or "comments" to the entry depending on the number of contributions
        // using the library function FormatPlural
        $Panel->AddListItem($ParticipantsList, $ParticipantName,
                            GetUrl($Configuration, "account.php", "", "u", $ParticipantID),
                            $ParticipantCount.' '.FormatPlural($ParticipantCount, $Comment, $Comments));
    }

    // If the "Show all" link has to be shown, then show it
    if ($Context->Configuration['EXT_SHOWPARTICIPANTS_SHOWALLLINK']
        and  $Context->Configuration['EXT_SHOWPARTICIPANTS_LENGTH'] != -1) {
        // Adding the "Show all" link to the panel list
        $Panel->AddListItem($ParticipantsList, $Context->Dictionary['ShowAll'].'...',
                            GetUrl($Configuration, "extension.php", "", "", "", "", "PostBackAction=ShowAllParticipants&amp;DiscussionID=$DiscussionID"));
    }
}

if ($Context->SelfUrl == 'extension.php' && ForceIncomingString('PostBackAction','') == 'ShowAllParticipants') {

    // Including the css trick. Don't overload other pages
    $Head->AddStyleSheet('extensions/ShowParticipants/style/participants.css');

    // Fetching DiscussionID
    $DiscussionID = ForceIncomingInt("DiscussionID", 0);
    // Adding the new page title
    $Context->PageTitle = $Context->GetDefinition("AllParticipantsTitle");

    // Instantiating and adding the AllParticipantsGrid control
    include(dirname(__FILE__) . '/library/Form.AllParticipantsGrid.php');
    $ParticipantsGrid = $Context->ObjectFactory->NewContextObject($Context, 'AllParticipantsGrid', $DiscussionID);
    $Page->AddRenderControl($ParticipantsGrid, $Configuration["CONTROL_POSITION_BODY_ITEM"]);
}

if ($Context->SelfUrl == 'account.php') {
    if($Context->Configuration['ENABLE_WHISPERS']) {
        $Context->AddToDelegate('PreferencesForm', 'PreRender', 'ShowParticipants_PreferencesForm');
    }
    function ShowParticipants_PreferencesForm(&$PreferencesForm) {
        $PreferencesForm->AddPreference('Show Participants', 'CountWhispersInParticipantsList', 'WhispersInParticipants');
    }
}

?>
