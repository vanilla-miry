<?php

class AllParticipantsGrid extends PostBackControl {

    var $Context;
    var $Participants;
    var $DiscussionName;
    var $DataCount;

    function AllParticipantsGrid(&$Context, $DiscussionID) {
        // 1st adding the name
        $this->Name = 'AllParticipantsGrid';

        // 2nd defining valid actions
        $this->ValidActions = array('ShowAllParticipants');

        // 3rd calling PostBackControl's Constructor
        $this->Constructor($Context);

        // 4th checking the validity of the PostBackAction
        if ($this->IsPostBack) {
            // Building the sql query to fetch the participants IDs
            $Sql = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
            $Sql->SetMainTable('User', 'u');
            $Sql->AddJoin('Comment', 'c', 'AuthUserID', 'u', 'UserID', 'inner join');
            $Sql->AddSelect(array('Name', 'UserID', 'Icon'), 'u');
            $Sql->AddSelect('CommentID', 'c', 'Count', 'count');
            $Sql->AddWhere('c', 'DiscussionID', '', $DiscussionID, '=');
            // Excluding whispered & deleted comments
            $Sql->AddWhere('c', 'WhisperUserID', '', 0, '=');
            $Sql->AddWhere('c', 'Deleted', '', 0, '=');
            // Grouping every user
            $Sql->AddGroupBy('UserID', 'u');
            // Selecting the ordering method: 1 -> Username, 0 -> #contributions
            if ($Context->Configuration['EXT_SHOWPARTICIPANTS_ORDER'])
                $Sql->AddOrderBy('Name','u');
            else {
                $Sql->AddOrderBy(array('Count'), array(''), 'desc');
                $Sql->AddOrderBy('Name','u');
            }

            // Sending the query to the DB and storing the results in the object
            $this->Participants = $Context->Database->Select($Sql, 'ShowParticipantsExtension', '',
                                                             $Context->GetDefinition("ParticipantsErrorFetching"));

            // Copying the $Context object
            $this->Context = $Context;

            $CurrentPage = ForceIncomingInt('page', 1);
            $this->DataCount = $this->Context->Database->RowCount($this->Participants);

            $pl = $this->Context->ObjectFactory->NewContextObject($this->Context, 'PageList');
            $pl->NextText = $this->Context->GetDefinition('Next');
            $pl->PreviousText = $this->Context->GetDefinition('Previous');
            $pl->Totalled = 0;
            $pl->CssClass = 'PageList';
            $pl->TotalRecords = $this->DataCount;
            $pl->PageParameterName = 'page';
            $pl->CurrentPage = $CurrentPage;
            $pl->RecordsPerPage = $this->Context->Configuration['SEARCH_RESULTS_PER_PAGE'];
            $pl->PagesToDisplay = 10;
            $this->PageList = $pl->GetLiteralList();
            $this->PageDetails = str_replace(array('//1', '//2', '//3'), 
                                             array($pl->FirstRecord, $pl->LastRecord, $pl->TotalRecords), 
                                             $this->Context->GetDefinition('ParticipantsResultsMessage'));
        }
    }

    function Render() {
        // Getting some language definitions
        $Comments = $this->Context->GetDefinition("ParticipationPlural");
        $Comment = $this->Context->GetDefinition("ParticipationSingle");

        $this->CallDelegate('PreRender');

//         include(dirname(__FILE__) . '/themes/all_participants_top.php');
        include(substr(dirname(__FILE__),0,-7) .  'themes/all_participants_top.php');

        // Initializations
        $ThemePath = substr(dirname(__FILE__),0,-7) .  'themes/all_participants.php';
        $ParticipantList = '';
        $Alternate = 0;
        $FirstRow = 1;
        $Counter = 0;

        // Building the participants list, each participant from a row of the DB query
        while ($Participant = $this->Context->Database->GetRow($this->Participants)) {
            // Collecting the fields retrieved from the DB related to the participants
            $ParticipantName = ForceString($Participant['Name'], '');
            $ParticipantID = ForceInt($Participant['UserID'], 0);
            $ParticipantCount = ForceInt($Participant['Count'], 0);
            $ParticipantIcon = ForceString($Participant['Icon'], '');
            if ($Counter < $this->Context->Configuration['SEARCH_RESULTS_PER_PAGE']) {
                include($ThemePath);
            }
            $FirstRow = 0;
            $Counter++;
            $Alternate = FlipBool($Alternate);
        }

        echo $ParticipantList;

        include(substr(dirname(__FILE__),0,-7) .  'themes/all_participants_bottom.php');

        $this->CallDelegate('PostRender');
    }
}
?>