<?php
$Context->SetDefinition('Show Participants Management', 'Mostrar Participantes');

$Context->SetDefinition('AllParticipantsTitle', 'All participants');
$Context->SetDefinition('Participant', 'Participant');
$Context->SetDefinition('ParticipationSingle', 'commentario');
$Context->SetDefinition('ParticipationPlural', 'commentarios');
$Context->SetDefinition('ParticipantsResultsMessage', 'Participants //1 to //2 of //3');
$Context->SetDefinition('ParticipantsErrorFetching', 'An error ocurred when fetching this discussion participants.');
$Context->SetDefinition('Back', 'Volver');

$Context->SetDefinition('CountWhispersInParticipantsList', 'Count whispers to myself in the participants lists.');
?>