<?php
$Context->SetDefinition('Show Participants Management', 'Show Participants');

$Context->SetDefinition('AllParticipantsTitle', 'All participants');
$Context->SetDefinition('Participant', 'Participant');
$Context->SetDefinition('ParticipationSingle', 'comment');
$Context->SetDefinition('ParticipationPlural', 'comments');
$Context->SetDefinition('ParticipantsResultsMessage', 'Participants //1 to //2 of //3');
$Context->SetDefinition('ParticipantsErrorFetching', 'An error ocurred when fetching this discussion participants.');
$Context->SetDefinition('Back', 'Back');

$Context->SetDefinition('CountWhispersInParticipantsList', 'Count whispers to myself in the participants lists.');
?>