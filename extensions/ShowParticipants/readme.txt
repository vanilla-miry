===================================
EXTENSION INSTALLATION INSTRUCTIONS
===================================

In order for Vanilla to recognize an extension, it must be contained within its
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/ShowParticipants/default.php

Once this is complete, you can enable the extension through the "Manage
Extensions" form on the settings tab in Vanilla.


Extension Change log:

0.5  - 01.05.2008
    -class definition excluded from the source code, moved to /library as a standalone file
    -SetList extension used to generate the settings form
    -added option to count also whispered comments to the user (via user preference), requested by fysicsluvr.
    It only applies when the system have whispers enabled

0.4  - 27.03.2008
    -bug detected when working with whispers and deleted comments, which were
     also counted as participations. Solved
    -included a "Back" link in the "Show All Participants" page, redirecting back
     to the discussion page

0.3  - 17.03.2008
    -included "Show all" link, redirecting to an extension page showing the
     entire list of participants, subdivided in pages when needed, of length
     SEARCH_RESULTS_PER_PAGE from Configuration
    -the "All Participants" page also shows users icons
    -added a CSS trick to left-align the extension page title in vanilla theme
    -used FormatPlural in the comments label to produce more elegant code
    -used AddLimit to the SQL query to be more efficient in memory

0.2  - 10.03.2008
    -list of participants can be sorted (via configuration settings) by username
     or by amount of participations on the discussion (also alphabetically on ties)
    -list name can be changed (via configuration settings)
    -list length can be limited (via configuration settings)
    -settings added on /conf/settings.php

0.1  - 07.03.2008
    -first release
