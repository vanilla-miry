<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Reset Cache for ExtensionLanguageLoader
*
*/

// read init
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// reset cache
// - removes cache files so they will be recreated
function reset_cache() {
	global $LL_dir_cache;
	if ( file_exists( $LL_dir_cache ) &&
	     ( ( $dh = opendir( $LL_dir_cache ) ) !== false ) ) {
	    while ( ( $entry = readdir( $dh ) ) !== false ) {
	        if ( $entry != "." && $entry != ".." && is_file( $LL_dir_cache.$entry ) ) {
	        	unlink( $LL_dir_cache.$entry );
	        }
	    }
		closedir( $dh );
	}		
}
