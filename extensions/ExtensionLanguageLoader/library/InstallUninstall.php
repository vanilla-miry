<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Decision if the extension is enabled and also if we use caching or not
* 
*/

// read settings
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// install identifier comment
$LL_install_comment = "/* this is added by $LL_extension_name extension, please do not edit this block */";

// code to add to conf/language.php checking if extension is enabled and including language loader if so
$LL_language_file_add = "<?$LL_install_comment
@include (\$Configuration['EXTENSIONS_PATH'].'$LL_extension_name/LanguageLoader.php');
$LL_install_comment?>";

// removes from string the code added by version 1.0
function LL_remove_code_added_by_1_0( $language_file_content ) {

	return preg_replace( '|<\?\s*/\* this is added by ExtensionLanguageLoader extension, please do not edit bellow this line \*/.*\?>|s', '', $language_file_content );
}

// removes from string the code added by version 1.1
function LL_remove_code_added_by_1_1( $language_file_content ) {
	global $LL_language_file_add;
	return str_replace( $LL_language_file_add, '', $language_file_content );
}

// runs uninstall of code
function LL_uninstall() {
	LL_uninstall_and_install();
}

// runs installation of code
function LL_install() {
	LL_uninstall_and_install( true );
}

// common function for un/install, pass true parameter for install
function LL_uninstall_and_install( $install = false ) {
	global $Configuration;
	$language_file = $Configuration['APPLICATION_PATH'].'conf/language.php';
	//get language.php content
	$language_file_content = implode( '', file( $language_file ) );
	//remove 1.0 code
	$language_file_content = LL_remove_code_added_by_1_0( $language_file_content );
	//remove 1.1 code
	$language_file_content = LL_remove_code_added_by_1_1( $language_file_content );
	//save content preceded by new code
	$fp = fopen( $language_file, "w" );
	$add_before = '';
	if ( $install ) {
		global $LL_language_file_add;
		$add_before = $LL_language_file_add;
	}
	fwrite( $fp, $add_before . $language_file_content );
	fclose( $fp );
}
