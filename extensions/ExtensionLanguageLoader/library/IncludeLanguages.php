<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Reads definitions from
* 
* conf/Language.php
* languages/Language/definitions.php
* languages/Language/extensions/ * .php
* extensions/ * /Language.php
* extensions/ * /language.php
* extensions/ * /translations/Language.php
* extensions/ * /translations/language.php
*
*/

// read settings
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// search and includes languages' definitions and adds lang code prefix for lower priority definitions
function LL_include_languages( $language_priorities, $caching = false ) {

	LL_debug( $language_priorities );

	// reverse array of priorities for loop,
	// because a higher priority definition has to overdefine a lower priority definition
	$rlangs = array_reverse( $language_priorities );

	global $Context;
	global $LL_prefix_lower_langs;

	// if we are goind to use lang code prefix for lower priority definitions
	if ($LL_prefix_lower_langs) {
		// we are
		// prepare temporary language dictionaries
		global $LL_lang_main;		
		$temp_langs = Array();
		// loop languages
		foreach ( $rlangs as $lang ) {			
			LL_debug( $lang );
			// clean current dictionary data
			$Context->Dictionary = Array ();
			// load data
			LL_include_language( $lang );
			// save language dictionary into temp langs under its name
			$temp_langs[$lang] = $Context->Dictionary;
		}
		// clean current dictianry data
		$Context->Dictionary = Array ();
		// loop languages
		foreach ( $rlangs as $lang ) {
			// create language code
			$temp_lang = $temp_langs[$lang];
			$temp_lang_code = ($temp_lang['XMLLang']) ? $temp_lang['XMLLang'] : $lang;
			// loop through all dictionary definitions and add them into global dictionary data
			// because we go from lower to higher priority language we overdefine lower priority definitions
			// also add lang code prefix if it is not main language
			foreach ( $temp_lang as $term => $definition ) {
				$Context->Dictionary[$term] = (($lang != $LL_lang_main) ? '('.$temp_lang_code.') ' : '').$definition;
			}
		}
	} else {
		// if we are not going to prefix lower priority definitions by language code
		// we just clean dictionary data
		$Context->Dictionary = Array();
		// and loop through all dictionary definitions and add them into global dictionary data
		// because we go from lower to higher priority language we overdefine lower priority definitions
		foreach ( $rlangs as $lang ) {
			LL_debug( $lang );		
			LL_include_language( $lang );		
		}
	}
}

// includes
// - base definitions from language addon and from conf
// - extension definitions from extensions
// - extension definitions from language add-on
function LL_include_language( $lang, $caching = false ) {
	LL_include_base_definitions_from_language_addon( $lang, $caching );
	LL_include_extension_definitions_from_language_addon( $lang, $caching );
	LL_include_extension_definitions_from_extensions( $lang, $caching );
}

// load extension definitions from language addon if it is there
function LL_include_extension_definitions_from_language_addon( $lang, $caching = false ) {
	LL_debug( 'include_extension_definitions_from_language_addon' );
	// reads language files if language pack contains extension translations in extensions folder
	// - vanilla/languages/LANGUAGE/extensions/*.php
	global $Context;
	global $LL_dir_lang;
	$addons_translation_folder = $LL_dir_lang.$lang.'/extensions/';
//	LL_debug( $addons_translation_folder );
	if ( file_exists( $addons_translation_folder ) &&
		 ( $dh = opendir( $addons_translation_folder ) ) !== false ) {
	    while ( ( $entry = readdir( $dh ) ) !== false ) {
	        if ( $entry != "." && $entry != ".." && ( substr( $entry, -4 ) == '.php' ) ) {
	        	$language_file = $addons_translation_folder.$entry;
	            if ( file_exists( $language_file ) ) {
					LL_debug( 'loaded '.$language_file );
	                include( $language_file );
	            }
	        }
	    }
		closedir( $dh );
	}
	
}

function LL_include_extension_definitions_from_extensions( $lang, $caching = false ) {
	LL_debug( 'include_extension_definitions_from_extensions' );
	// Reading extension translations from extensions subfolders (depends on $LL_addon_language_folders)
	// - vanilla/extensions/*/LANGUAGE.php
	// - vanilla/extensions/*/translations/LANGUAGE.php
	global $Context;
	global $LL_dir_ext;
	global $LL_addon_language_folders;
	if ( ( $dh = opendir( $LL_dir_ext ) ) !== false ) {
	    while ( ( $entry = readdir( $dh ) ) !== false ) {
	        if ( $entry != "." && $entry != ".." && is_array( $LL_addon_language_folders ) ) {
	            foreach ($LL_addon_language_folders as $addon_language_folder) {
	            	$lang_names = LL_get_alternative_language_names ( $lang );
					foreach ( $lang_names as $lang_name ) {
		            	$language_file = $LL_dir_ext.$entry.
										$addon_language_folder.
										$lang_name.'.php';
			            if ( file_exists( $language_file ) ) {
							LL_debug( 'loaded '.$language_file );
			                include( $language_file );
			            }
					}
				}
	        }
	    }
	}
	closedir( $dh );
}

function LL_include_base_definitions_from_language_addon( $lang, $caching = false ) {
	LL_debug( 'include_base_definitions_from_language_addon' );
	// reads global language files for chosen language
	// - vanilla/conf/LANGUAGE.php
	global $Context;
	global $LL_dir_conf;
	$language_file = $LL_dir_conf.$lang.'.php';
	if ( file_exists( $language_file ) ) {
		LL_debug( 'loaded '.$language_file );
		include( $language_file );
	}
	// - vanilla/languages/LANGUAGE/definitions.php
	global $LL_dir_lang;
	$language_file = $LL_dir_lang.$lang.'/definitions.php';
	if ( file_exists( $language_file ) ) {
		LL_debug( 'loaded '.$language_file );
		include( $language_file );
	}
}

// returns alternative language names
// for example: English, english, en-ca, en
function LL_get_alternative_language_names( $lang ) {
	$alternatives = array ( $lang, strtolower( $lang ) );
	global $Context;
	$lang_code = $Context->Dictionary['XMLLang'];
	if (lang_code) {
		array_push( $alternatives, $lang_code );
		List( $lang_code_general ) = split( '-', $lang_code );
		if ( $lang_code != $lang_code_general ) {
			array_push( $alternatives, $lang_code_general );
		}
	}
	return $alternatives;
}