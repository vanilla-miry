<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Reads extensions translations
*
*/

// read init
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// indicator if we need to create cache
$LL_redo_cache = false;

// check if there is a cache file for this language settings
if ( file_exists( $LL_cache_file ) ) {
	// load the file if so
	LL_debug('loading cache');
	include( $LL_cache_file );
	LL_debug('cache loaded');
	// check for timeout
	LL_debug( $LL_CACHE_TIMESTAMP );
	if ( $LL_CACHE_TIMESTAMP != '0' ) {
		$timeout_in_seconds = $Configuration['LL_CACHE_TIMEOUT'] * 3600;
		LL_debug( time() - ($LL_CACHE_TIMESTAMP + $timeout_in_seconds ));
		if ( $LL_CACHE_TIMESTAMP + $timeout_in_seconds < time() ) {
			$LL_redo_cache = true;
		}
	}
} else {
	$LL_redo_cache = true;
}

// if we have to redo cache
if ( $LL_redo_cache ) {
	LL_debug('redoing cache');
	// include static and save cache
	require( $LL_root_lib.'IncludeLanguages.php' );
	LL_include_languages( $LL_lang_priorities, true );
	LL_save_cache( $LL_cache_file );
}

// adds slashes so it is possible to use it as a content of 'string'
function LL_add_php_slashes( $string ) {
	return preg_replace('{([\'\\\\])}', '\\\\$1', $string);
}

// saves cache
function LL_save_cache( $cache_file ) {
	
	LL_debug('saving cache to: '.$cache_file);

	// cache file content at the beginning of file
	$cache = '<?php
/*
* This is a generated language cache file
* All your changes will be destroyed or will destroy your language behaviour
* You can delete this file. It is created if not present.
*
* If you need to make changes to these definitions, add the new definitions to
* your conf/language.php file. Definitions made there will override these.
*/

// timestamp of this cache (0 causes to never timeout)
$LL_CACHE_TIMESTAMP = '.time().';

// Define all dictionary codes
'; //we don't need to save XMLLang explicitly, because it is in $Context->Dictionary

	global $Context;
	$definitions = $Context->Dictionary;
	// loop through all language definitions we have currently loaded and
	// add php assignment to the cache content
	foreach ($definitions as $term => $definition) {
		$cache .= "\$Context->Dictionary['".LL_add_php_slashes( $term )."']".
					" = ".
					"'".LL_add_php_slashes( $definition )."'".
					";\n";
	}

	global $LL_dir_cache;
	// create cache folder if not exists
	if ( !file_exists( $LL_dir_cache )) {
		LL_debug( $LL_dir_cache );
		mkdir( $LL_dir_cache, 0777 );
	}

	// save cache content to cache file
	$fp = fopen( $cache_file, 'w' );
	fwrite( $fp, $cache );
	fclose( $fp );
}
