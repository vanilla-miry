<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Decision if the extension is enabled and also if we use caching or not
*              also it uninstalls code if extension is enabled and if the code is present
*/

// read init
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// check if LL is installed, then enabled in Extensions settings
// and then check if we are caching or not and include code according to it
if ( $Configuration['LL_SETUP'] ) {
	// we are installed
	// check if we are enabled
	LL_debug('installed');
	define( $LL_extension_name.'_INSTALLED', true );
	$extensions_file = $LL_dir_conf.'extensions.php';
	if ( LL_is_enabled( $extensions_file ) ) {
		// we are enabled
		// check if we are caching
		LL_debug('enabled');
		$Context->Dictionary = Array ();
		if ( $Configuration['LL_CACHING'] ) {
			// we are caching load CacheLanguages.php
			LL_debug('cache');
			include( $LL_root_lib.'CacheLanguages.php' );
		} else {
			// we are not caching load StaticLanguages.php
			LL_debug('static');
			include( $LL_root_lib.'StaticLanguages.php' );
		}
	} else {
		// we are disabled
		// uninstall
		require( $LL_root_lib.'InstallUninstall.php' );
		LL_uninstall();
	}
}
LL_debug( $Context->Dictionary, true );


// checks if LL is enabled in Extensions settings
function LL_is_enabled( $extensions_file ) {
	global $LL_extension_name;
	$extensions = file( $extensions_file );
	foreach ( $extensions as $extension ) {
		if ( chop( $extension ) == 'include($Configuration[\'EXTENSIONS_PATH\']."'.$LL_extension_name.'/default.php");' ) {
			return true;
		}
	}
	return false;
}

// debug function must be set $LL_debug to true
// then it displays debug info
function LL_debug( $msg, $exit = false ) {
	global $LL_debug;
	if ( $LL_debug ) {
		var_dump( $msg );
		if ( $exit ) {
			exit;
		}
	}
}
