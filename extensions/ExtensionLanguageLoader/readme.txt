===================================
EXTENSION INSTALLATION INSTRUCTIONS
===================================

In order for Vanilla to recognize an extension, it must be contained within it's
own directory within the extensions directory. So, once you have downloaded and
unzipped the extension files, you can then place the folder containing the
default.php file into your installation of Vanilla. The path to your extension's
default.php file should look like this:

/path/to/vanilla/extensions/this_extension_name/default.php

Once this is complete, you can enable the extension through the "Manage
Extensions" form on the settings tab in Vanilla.

If you have 1.0 version installed, it will be upgraded automatically.

This extension adds some small code into your /path/to/vanilla/conf/language.php file which goes through
some places where might be found translations for extensions for the curently selected language.
This process takes some time so there is a cache which is defaulting to 1 hour.
In admin panel you can disable cache, change cache timeout in hours(!) and you can manually reset (empty) cache

Locations which is this extension looking for (in /path/to/vanilla/):
	conf/Language.php
	languages/Language/definitions.php
	languages/Language/extensions/ * .php
	extensions/*/Language.php
	extensions/*/language.php
	extensions/*/languagecode-countrycode.php
	extensions/*/languagecode.php
	extensions/*/translations/Language.php
	extensions/*/translations/language.php
	extensions/*/translations/languagecode-countrycode.php
	extensions/*/translations/languagecode.php

 (Actually this is also the order of search)
 
If you want to setup language priorities, then you can use $Configuration['LANGUAGE_PRIORITIES'] to specify
languages separated by commas. For example if you want to display your forum in French, if there is
no definition in French then to use Spanish and if Spanish definition is not found then to use Italian,
then you can set this in your conf/settings.php:

$Configuration['LANGUAGE_PRIORITIES'] = 'French,Spanish,Italian'; // no spaces :)

Still $Configuration['LANGUAGE'] is used as the highest priority language. Therefore it works fine without
any priorities set and also it works fine if you use some language selecting mechanism before, for example
if you use extension LanguageSelector from Max_B

As default behaviour there is added a bracketed language code prefix for definitions not found in the language
with the highest priority, so if there is some text displayed in another then your the most preferred language
it is preceded by (langaugecode).
You can turn off this behaviour by adding:
$Configuration['LANGUAGE_PRIORITIES_HIDE_CODE'] = '1';
And do a cache reset (or just leave it and wait for cache timeout)


This extension keeps conf/language.php custom language definitions for your site untouched.

If you are a translator or an extension developer and you want to add a language translation of an add-on
you are encouraged to save language definitions in:
 -	language/LANGUAGE/extensions/languageEXTENSION.php
		 (if you are translator and produces language add-on named LANGUAGE and
		  you translate extension named EXTENSION)
 -	extensions/EXTENSION/translations/LANGUAGE.php
 		 (if you are extension developer of extension named EXTENSION and
 		  you translate your extension definitions into langauge named LANGUAGE)
and tell everybody to install this extension.

Disabling this extension disables also loading translations and next run also uninstalls the code installed
into conf/language.php.

Remember that this extension will install itself in the next run of Vannila if you enabled it first time.
It means that if you enabled it and you don't see translations immediately, try to do a refresh.

Also some old extensions use old way of defining new dictionary definitions. These extensions overdefine
any translations loaded. Please take a look at FixLanguageDefinitions extension which brings bash script
which tries to fix these extensions.
(2007-12-06 Actually I'm considering improving the extension FixLanguageDefinitions so it will be easier
to use. So check it later)

Or you just go through source files (probably in default.php or definitions.php) of the buggy extension and
replace manually all these lines:
$Context->Dictionary["word"] = 'definition';
by this code for each pair word/definition:
$Context->SetDefinition( 'word', 'definition' );

Changelog:

ExtensionLanguageLoader 1.1
- loads all language definitions not only extensions
- language priorities (can be passed from language selector)
- optional language code prefix for definitions with lower priority
- implemented cache, cache timeout and manual cache reset
- settings panel in admin
- better install and uninstall + upgrades smoothly from 1.0
- new folder structure
- nicer code
- code commented