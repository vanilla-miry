<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Default Dictionary Definitions for ExtensionLanguageLoader
* 
*/

$Context->SetDefinition( 'ExtensionOptions', 'Extension Options' );
$Context->SetDefinition( 'LL_Settings', 'Extension Language Loader Settings' );
$Context->SetDefinition( 'LL_SettingsNotes', 'Here you can change settings for Language Loader.<br/>Search for all possible places of translations takes some time and if it\'s done every run of Vanilla, it might cause performance issues. Therefore it is recommended to use cache.<br/>If you installed a new add-on and you don\'t want to wait for cache timeout, you can empty cache here.');
$Context->SetDefinition( 'LL_CachingEnabled', 'Enable Caching' );
$Context->SetDefinition( 'LL_CacheTimeout', 'Cache timeout (in hours)' );
$Context->SetDefinition( 'LL_ResetCache', 'Reset Cache' );
