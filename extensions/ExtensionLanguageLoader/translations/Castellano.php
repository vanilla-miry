<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Czech Dictionary Definitions for ExtensionLanguageLoader
* Popis: Český jazykový slovník pro rozšíření ExtensionLanguageLoader
* 
*/

$Context->Dictionary['ExtensionOptions'] = 'Extensiones';
$Context->Dictionary['LL_Settings'] = 'Idiomas';
$Context->Dictionary['LL_SettingsNotes'] = 'Aqu&iacute; puedes modificar la configuraci&oacute;n del gestor de idiomas. <br/> La b&uacute;squeda en todos los sitios posibles en los que se pueden ubicar las traducciones lleva un cierto tiempo y, si la consulta se efect&uacute;a cada vez que se carga una p&aacute;gina, podr&iacute;a tener problemas de rendimiento. Por ese motivo se recomienda el uso de la caché. <br/> Si se instala una nueva extensión y no se desea hasta que caduque la caché, se puede eliminar aqu&iacute;.';
$Context->Dictionary['LL_CachingEnabled'] = 'Activar cach&eacute;';
$Context->Dictionary['LL_CacheTimeout'] = 'Tiempo de caducidad de la cach&eacute; (en horas)';
$Context->Dictionary['LL_ResetCache'] = 'Refrescar la cach&eacute;';
