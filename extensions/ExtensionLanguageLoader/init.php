<?php
/*
* Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
* This file is part of Vanilla.
* Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
* Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
* The latest source code for Vanilla is available at www.lussumo.com
*
* Description: Initialization of settings
* 
*/

// this is just for the case I would want to change the name of this extension which is unlikely
// if it happens, there is still needed to change extension name in lines including this settings file
$LL_extension_name = 'ExtensionLanguageLoader';

// turn on/off debug info
//$LL_debug = true;

// initial settings
$LL_dir_app = $Configuration['APPLICATION_PATH'];
$LL_dir_ext = $Configuration['EXTENSIONS_PATH'];
$LL_dir_lang = $Configuration['LANGUAGES_PATH'];
$LL_dir_conf = $LL_dir_app.'conf/';
$LL_root = $LL_dir_ext.$LL_extension_name.'/';
$LL_root_conf = $LL_root.'conf/';
$LL_root_lib = $LL_root.'library/';

// read settings
@require( $LL_root_conf.'settings.php' );

// set another initial settings depending on settings
$LL_dir_cache = $LL_dir_conf.$LL_cache_folder_name.'/';

// read dictionary definitions
@include( $LL_root_conf.'definitions.php' );

// get user language preferences settings
$LL_lang_main = $Configuration['LANGUAGE'];
if ( $Configuration['LANGUAGE_PRIORITIES'] ) {
	$LL_lang_priorities = split( ',', $Configuration['LANGUAGE_PRIORITIES'] );
} else {
	$LL_lang_priorities = Array( $LL_lang_main );
}
// if first lang is not the main one then insert it to the first place
if ( $LL_lang_priorities[0] != $LL_lang_main ) {
	array_unshift( $LL_lang_priorities, $LL_lang_main );
}
// make sure there are no duplicit values
$LL_lang_priorities = array_unique( $LL_lang_priorities );

// put "(lang-code) " in front of lower definitions
$LL_prefix_lower_langs = true;
if ($Configuration['LANGUAGE_PRIORITIES_HIDE_CODE'] == '1') {
	$LL_prefix_lower_langs = false;
}

// create cache file name
$LL_lang_settings = Implode( ',,', $LL_lang_priorities);
$LL_cache_file = $LL_dir_cache.$LL_lang_settings.'.php';

