<?php
/*
Extension Name: Extension Language Loader
Extension Url: http://lussumo.com/addons/
Description: Searches all possible places for translations and loads it according to language priorities. Read readme.txt for more information.
Version: 1.1
Author: Tomáš Klapka aka klip
Author Url: http://tomas.klapka.cz/

Copyright 2007 Tomáš Klapka aka klip (tomas@klapka.cz)
This file and content of the folder containing this file is part of Vanilla.
Vanilla is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
Vanilla is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Vanilla; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
The latest source code for Vanilla is available at www.lussumo.com
*/

// read init
@require( $Configuration['EXTENSIONS_PATH'].'ExtensionLanguageLoader/init.php' );

// INSTALL
//
// install the code to the beginning of conf/language.php if we are not installed yet
if ( !defined( $LL_extension_name.'_INSTALLED' ) ) {
	require( $LL_root_lib.'InstallUninstall.php' );
	LL_install();
}

// UNINSTALL
//
// delegation for uninstall before disable
// doesn't work without enabling include of extensions at appg/init_ajax.php at the end of file
// but it might cause trouble coming from other extensions
if ($Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')) {
	function ExtensionForm_PreExtensionDisable_LL_uninstall(&$ExtensionForm) {
		global $LL_extension_name;
		$ExtensionName = &$ExtensionForm->DelegateParameters['ExtensionName'];
		if ( $ExtensionName == $LL_extension_name) {
			require( $LL_root_lib.'InstallUninstall.php' );
			LL_uninstall();
		}
	}
	$Context->AddToDelegate( 'ExtensionForm', 'PreExtensionDisable', 'ExtensionForm_PreExtensionDisable_LL_uninstall' );
}

// SETTINGS
//
// add settings into conf/settings.php
LL_add_setting( 'LL_SETUP' );
LL_add_setting( 'LL_CACHING' );       // enable caching as default
LL_add_setting( 'LL_CACHE_TIMEOUT' ); // 1 hour

// ADMIN CONTROL PANEL
//
if ($Context->SelfUrl == 'settings.php' && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')) {

	// creates settings page for admin - enable/disable cache, set cache timeout, reset cache
	class LLSettings extends PostBackControl {
		var $ConfigurationManager;

		function LLSettings(&$Context) {
			global $LL_root_lib;
			$this->Name = 'LLSettings';
			$this->ValidActions = array('LanguageLoader', 'ProcessLanguageLoader', 'LanguageLoaderResetCache');
			if(!isset($_POST['LL_CACHING'])) {
			   $_POST['LL_CACHING'] = 0;
			}
			$this->Constructor($Context);
			if (!$this->Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')) {
				$this->IsPostBack = 0;
			} elseif( $this->IsPostBack ) {
				$SettingsFile = $this->Context->Configuration['APPLICATION_PATH'].'conf/settings.php';
				$this->ConfigurationManager = $this->Context->ObjectFactory->NewContextObject($this->Context, 'ConfigurationManager');
				if ($this->PostBackAction == 'ProcessLanguageLoader') {
					$this->ConfigurationManager->GetSettingsFromForm($SettingsFile);
					// And save everything
					if ($this->ConfigurationManager->SaveSettingsToFile($SettingsFile)) {
						header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=LanguageLoader&Success=1'));
					} else {
						$this->PostBackAction = 'LanguageLoader';
					}
				}
				if ( $this->PostBackAction == 'LanguageLoaderResetCache' ) {
					$Configuration = $this->Context->Configuration;
					require( $LL_root_lib.'ResetCache.php' );
					reset_cache();
					header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=LanguageLoader&Success=1'));
				}

			}
			$this->CallDelegate('Constructor');
		}

		function Render() {
			if ($this->IsPostBack) {
				$this->CallDelegate('PreRender');
				$this->PostBackParams->Clear();
				if ( $this->PostBackAction == 'LanguageLoader' ) {
					$this->PostBackParams->Set('PostBackAction', 'ProcessLanguageLoader');
					echo '
					<div id="Form" class="Account GlobalsForm">';
					if (ForceIncomingBool('Success', 0)) echo '<div id="Success">'.$this->Context->GetDefinition('ChangesSaved').'</div>';
					echo '
					<fieldset>
						<legend>'.$this->Context->GetDefinition('LL_Settings').'</legend>
						'.$this->Get_Warnings().'
						'.$this->Get_PostBackForm('frmLanguageLoader').'
						<p>'.$this->Context->GetDefinition('LL_SettingsNotes').'</p>
						<ul>
							<li id="ForumOptions">
								<p><span><label for="chkCachingEnabled"><input type="checkbox" name="LL_CACHING" id="chkCachingEnabled" value="1"'.
								($this->ConfigurationManager->GetSetting('LL_CACHING') ? ' checked="checked"' : '').
								' />'.$this->Context->GetDefinition('LL_CachingEnabled').'</label></span></p>
								<p><label for="txtCachingTimeout">'.$this->Context->GetDefinition('LL_CacheTimeout').'</label>
								<input type="text" name="LL_CACHE_TIMEOUT" id="txtCachingTimeout"  value="'.$this->ConfigurationManager->GetSetting('LL_CACHE_TIMEOUT').'" maxlength="200" class="Checkbox" /></p>
								<p><a href="'.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=LanguageLoaderResetCache').'">'.$this->Context->GetDefinition('LL_ResetCache').'</a></p>
							</li>
						</ul>
						<div class="Submit">
							<input type="submit" name="btnSave" value="'.$this->Context->GetDefinition('Save').'" class="Button SubmitButton" />
							<a href="'.GetUrl($this->Context->Configuration, $this->Context->SelfUrl).'" class="CancelButton">'.$this->Context->GetDefinition('Cancel').'</a>
						</div>
						</form>
					</fieldset>
					</div>';
				}
			}
			$this->CallDelegate('PostRender');
		}
	}

	$LLSettings = $Context->ObjectFactory->NewContextObject($Context, 'LLSettings');
	$Page->AddRenderControl($LLSettings, $Configuration['CONTROL_POSITION_BODY_ITEM'] + 1);

	$ExtensionOptions = $Context->GetDefinition('ExtensionOptions');
	$Panel->AddList($ExtensionOptions, 10);
	$Panel->AddListItem($ExtensionOptions, $Context->GetDefinition('LL_Settings'), GetUrl($Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=LanguageLoader'));

}

// this function adds setting to conf/settings.php
// it is just helper
function LL_add_setting( $setting, $value = 1 ) {
	global $Configuration;
	global $Context;
	if ( !array_key_exists( $setting, $Configuration ) ) {
		AddConfigurationSetting( $Context, $setting, $value );
	}
}
