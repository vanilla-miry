<?php
/*
Extension Name: Downtime
Extension Url: http://code.google.com/p/vanilla-friends/
Description: Take a forum offline for maintainance.
Version: 1.1.0
Author: squirrel
Author Url: http://digitalsquirrel.com/
*/


// Set up permissions
$Configuration['PERMISSION_USE_OFFLINE_FORUM'] = '0';

// Settings form initialization.
if ( 'settings.php' == $Context->SelfUrl ) {
	function SetList_Init_Downtime(&$SetList)
	{
		// Replace '//1' with the text of the use offline permission.
		$elements = &$SetList->DelegateParameters['Form']['elements'];
		$elements['IsOffline']['description'] =
			str_replace('//1', $SetList->Context->GetDefinition('PERMISSION_USE_OFFLINE_FORUM'), $elements['IsOffline']['description']);
	}
	$Context->AddToDelegate('SetList', 'Init_Downtime', 'SetList_Init_Downtime');
}

// Check to see if the user can access this page.
if ( @$Context->Configuration['FORUM_IS_OFFLINE']
     && !($Context->Session->User->Permission('PERMISSION_USE_OFFLINE_FORUM') || $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS'))
   ) {
	if ( 'people.php' != $Context->SelfUrl ) {
		// Kill the current session and redirect to the login page.
		$Context->Session->End($Context->Authenticator);
		header('location: ' . $Context->Configuration['SAFE_REDIRECT']);
		die();
	}
	else if ( ForceIncomingString('PostBackAction', '') != 'SignIn' ) {
		// Non-permitted users can only try to log in, nothing else.
		if (@$Context->Configuration['FORUM_OFFLINE_MESSAGE']) {
			$Context->WarningCollector->Add($Context->Configuration['FORUM_OFFLINE_MESSAGE']);
		}
		unset($_GET['PostBackAction']);
		unset($_POST['PostBackAction']);
	}
}


// many minds wandering from room to room
// many trees slain just to write it to you

?>
