<?php
//English definition files
$Context->SetDefinition('VanillaVoting', 'Voting');
$Context->SetDefinition('VanillaVotingCategory', 'Pick the category that should have voting enabled');
$Context->SetDefinition('Votes', 'votes');
$Context->SetDefinition('Vote!', 'Vote!');
$Context->SetDefinition('VoteHistory','Vote History');
$Context->SetDefinition('UnVote','Remove Vote');
$Context->SetDefinition('BelowViewingThreshold','below viewing threshold');
$Context->SetDefinition('ShowComment','show/hide comment');
$Context->SetDefinition('CategoriesAll','All');
$Context->SetDefinition('CategoriesNone','None');
$Context->SetDefinition('ErrLoveItCreateTable', 'Could not create "Love it" database tables!');
$Context->SetDefinition('ErrLoveItCreateConfig', 'Could not save "Love it" settings to configuration file!');
$Context->SetDefinition('PERMISSION_VOTING_FOR_DISCUSSIONS','Allow voting for discussions');
$Context->SetDefinition('PERMISSION_VOTING_FOR_COMMENTS','Allow voting for comments');
?>