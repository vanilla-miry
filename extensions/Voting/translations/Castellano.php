<?php
//Spanish definition files
$Context->SetDefinition('VanillaVoting', 'Votaciones');
$Context->SetDefinition('VanillaVotingCategory', 'Categor&iacute;as en las que se permiten las votaciones');
$Context->SetDefinition('Votes', 'votos');
$Context->SetDefinition('Vote!', '&iquest;Vota!');
$Context->SetDefinition('VoteHistory','Vote History');
$Context->SetDefinition('UnVote','Retirar voto');
$Context->SetDefinition('BelowViewingThreshold','por debajo del umbral de visualizaci&oacute;n');
$Context->SetDefinition('ShowComment','mostrar/ocultar commentario');
$Context->SetDefinition('CategoriesAll','Todas');
$Context->SetDefinition('CategoriesNone','Ninguna');
$Context->SetDefinition('ErrLoveItCreateTable', 'No se han podido crear las tablas "Love it" de la base de datos');
$Context->SetDefinition('ErrLoveItCreateConfig', 'No se han podido grabar las opciones "Love it" al archivo de configuraci&oacute;n');
$Context->SetDefinition('PERMISSION_VOTING_FOR_DISCUSSIONS','Permitir votar las conversaciones');
$Context->SetDefinition('PERMISSION_VOTING_FOR_COMMENTS','Permitir votar los comentarios');
?>