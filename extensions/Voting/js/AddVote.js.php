<?php 
header("Content-Type: text/javascript");
include("../../../appg/settings.php");
include("../../../appg/init_vanilla.php");
global $Context; 
?>

function AddDiscussionVote (AjaxURL, DiscussionID, Vote, CurrentTotalVotes, PostBackKey) {

	$.post(AjaxURL, {'DiscussionID': DiscussionID, 'Vote': Vote, 'CurrentTotalVotes': CurrentTotalVotes, 'PostBackKey': PostBackKey },
		function(data) {
			if (data == 'Complete') {
                            $("li#Discussion_"+DiscussionID+" li.DiscussionVoteCount span:first-child").fadeTo('slow','0.1');
                            $("li#Discussion_"+DiscussionID+" li.DiscussionVoteCount span:first-child").text(CurrentTotalVotes + Vote);
                            if(Vote > 0) {
				$("li#Discussion_"+DiscussionID+" li.DiscussionAddVote").html('<span class="VotedPositive"><input class="VotePositive" type="button" disabled="disabled"/><input class="VoteNegative" type="button" disabled="disabled"/></span>');
                                $("li#Discussion_"+DiscussionID).addClass("VotedPositive");
			    } else if (Vote < 0) {
                                $("li#Discussion_"+DiscussionID+" li.DiscussionAddVote").html('<span class="VotedNegative"><input class="VotePositive" type="button" disabled="disabled"/><input class="VoteNegative" type="button" disabled="disabled"/></span>');
                                $("li#Discussion_"+DiscussionID).addClass("VotedNegative");
                            }
                        }
			else {
				alert('Adding vote to discussion did not work');
			}
			$("li#Discussion_"+DiscussionID+" li.DiscussionVoteCount span:first-child").fadeTo('slow','1');
		}
	);
        
	return false;
};
function AddCommentVote (AjaxURL, CommentID, Vote, CurrentTotalVotes, PostBackKey) {

	$.post(AjaxURL, {'CommentID': CommentID, 'Vote': Vote, 'CurrentTotalVotes': CurrentTotalVotes, 'PostBackKey': PostBackKey },
		function(data) {
			if (data == 'Complete') {
                            $("li#Comment_"+CommentID+" span.CommentVoteCount").fadeTo('slow','0.1');
                            $("li#Comment_"+CommentID+" span.CommentVoteCount").text(CurrentTotalVotes + Vote);
                            if(Vote > 0) {
                                $("li#Comment_"+CommentID+" span.AddCommentVote").html('<span class="VotedPositive"><input class="VotePositive" type="button" disabled="disabled"/><input class="VoteNegative" type="button" disabled="disabled"/></span>');
                                
			    } else if (Vote < 0) {
                                $("li#Comment_"+CommentID+" .CommentBody").slideUp('slow');
                                $("li#Comment_"+CommentID+" span.AddCommentVote").html('<span class="VotedNegative"><input class="VotePositive" type="button" disabled="disabled"/><input class="VoteNegative" type="button" disabled="disabled"/></span>');
                                $("li#Comment_"+CommentID+" span.CommentVoteCount").addClass('VotedNegative');
                                $("li#Comment_"+CommentID).addClass('VotedNegative');
                            }
                        }
			else {
				alert('Adding vote to comment did not work');
			}
			$("li#Comment_"+CommentID+" span.CommentVoteCount").fadeTo('slow','1');
		}
	);
	return false;
};
function ShowComment (CommentID) {   
    $("li#Comment_"+CommentID+" .CommentBody").slideToggle('slow');
}

$(document).ready(function() {
   $("li ul.VotedNegative").each(function() {
        $(this).parent().addClass('VotedNegative');
   });
   $("li span.VotedNegative").each(function() {
        $(this).parent().parent().parent().addClass('VotedNegative');
        $("li.VotedNegative .CommentBody").hide();
    });
    
});