<?php
/*
Extension Name: Vanilla Voting 0.2beta
Extension Url: http://vanillaforums.org/get/347
Description: Digg style voting
Version: 0.2beta
Author: Ziyad (MySchizoBuddy) Saeed
Author Url: N/A

*/

// Vanilla Voting root path;
if (!defined('VANILLAVOTING_ROOT')) define('VANILLAVOTING_ROOT', dirname(__FILE__).'/');
$Configuration["VANILLAVOTING_PATH"] = 'extensions/Voting/';

$Context->Configuration['PERMISSION_VOTING_FOR_DISCUSSIONS'] = '0';
$Context->Configuration['PERMISSION_VOTING_FOR_COMMENTS'] = '0';

if( !array_key_exists('VANILLAVOTING_CATEGORYID', $Configuration)) {AddConfigurationSetting($Context, 'VANILLAVOTING_CATEGORYID', '');}

//Add the required database table and fields
if (!array_key_exists('VANILLAVOTING_VERSION', $Configuration)) {
	$Errors = 0;
	$UserDiscussionVoteTable = "CREATE TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."UserDiscussionVote` (
`DiscussionID` int(8) NOT NULL,
`UserID` int(8) NOT NULL,
`Vote` tinyint(3) NOT NULL,
`DateCreated` datetime NOT NULL default '0000-00-00 00:00:00',
KEY `UserDiscussionVote_Key` (`DiscussionID`,`UserID`,`Vote`)
)";

$UserCommentVoteTable = "CREATE TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."UserCommentVote` (
`CommentID` int(8) NOT NULL,
`UserID` int(8) NOT NULL,
`Vote` tinyint(3) NOT NULL,
`DateCreated` datetime NOT NULL default '0000-00-00 00:00:00',
KEY `UserCommentVote_Key` (`CommentID`,`UserID`,`Vote`)
)";
	
	
	$AlterDiscussionTable = "ALTER TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."Discussion` ADD `Vote` smallint(6) NOT NULL default '0';";
	$AlterCommentTable = "ALTER TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."Comment` ADD `Vote` smallint(6)  NOT NULL default '0';";
			
	if (!mysql_query($UserDiscussionVoteTable, $Context->Database->Connection)) $Errors = 1;
	if (!mysql_query($UserCommentVoteTable, $Context->Database->Connection)) $Errors = 1;
	if (!mysql_query($AlterDiscussionTable, $Context->Database->Connection)) $Errors = 1;
	if (!mysql_query($AlterCommentTable, $Context->Database->Connection)) $Errors = 1;
	$r= mysql_error();
	
	if ($Errors == 0) {
		// Add the db structure to the database configuration file
		$Structure = "// Voting database structure for Discussions
\$DatabaseTables['UserDiscussionVote'] = 'UserDiscussionVote';
\$DatabaseColumns['UserDiscussionVote']['DiscussionID'] = 'DiscussionID';
\$DatabaseColumns['UserDiscussionVote']['UserID'] = 'UserID';
\$DatabaseColumns['UserDiscussionVote']['Vote'] = 'Vote';
\$DatabaseColumns['UserDiscussionVote']['DateCreated'] = 'DateCreated';
// Voting database structure for Comments
\$DatabaseTables['UserCommentVote'] = 'UserCommentVote';
\$DatabaseColumns['UserCommentVote']['CommentID'] = 'CommentID';
\$DatabaseColumns['UserCommentVote']['UserID'] = 'UserID';
\$DatabaseColumns['UserCommentVote']['Vote'] = 'Vote';
\$DatabaseColumns['UserCommentVote']['DateCreated'] = 'DateCreated';
// Vote field for the Discussion Table
\$DatabaseColumns['Discussion']['Vote'] = 'Vote';
// Vote field for the Comment Table
\$DatabaseColumns['Comment']['Vote'] = 'Vote';
";
		if (!AppendToConfigurationFile($Configuration['APPLICATION_PATH'].'conf/database.php', $Structure)) $Errors = 1;
		if ($Errors == 0) {
			AddConfigurationSetting($Context, 'VANILLAVOTING_VERSION', '0.1');
		} else {
			// Could not save configuration
			$NoticeCollector->AddNotice($Context->GetDefinition('ErrCreateConfig'));
		}
	}
}
if ($Context->SelfUrl == 'index.php' && $Context->Session->User->Permission('PERMISSION_VOTING_FOR_DISCUSSIONS')) {

	require_once VANILLAVOTING_ROOT . 'library/Class.DiscussionVote.php';
	
	if (defined('JQUERY_EXTENSION')) {
		if (!defined('JQUERY_INCLUDED')) {
			includeJQuery();
		}
		
	} else {
		$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'js/jquery-1.2.1.pack.js');
	}
	
	$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'js/AddVote.js.php');
	$Head->AddStyleSheet($Configuration["VANILLAVOTING_PATH"].'theme/vote.css');
	
	function DiscussionManager_InitDiscussionVote(&$DiscussionManager) {		
		$DiscussionVote = $DiscussionManager->Context->ObjectFactory->NewContextObject($DiscussionManager->Context, 'DiscussionVote');
		$sql = &$DiscussionManager->DelegateParameters['SqlBuilder'];
		$DiscussionVote->GetDiscussionVote(&$sql);
		$DiscussionManager->Context->DelegateParameters['DiscussionVote'] = &$DiscussionVote;
	}
	
	$Context->AddToDelegate('DiscussionManager', 'PostGetDiscussionBuilder', 'DiscussionManager_InitDiscussionVote');
	
	function DiscussionGrid_RenderDiscussionVote(&$DiscussionGrid) {
		$DiscussionVote = &$DiscussionGrid->Context->DelegateParameters['DiscussionVote'];
		$DataSet = &$DiscussionGrid->DelegateParameters['Discussion']->DelegateParameters['DataSet'];
		$DiscussionList = &$DiscussionGrid->DelegateParameters['DiscussionList'];
		$DiscussionVote->Render(&$DiscussionList, &$DataSet);
	}
	
	$Context->AddToDelegate('DiscussionGrid', 'PreDiscussionOptionsRender', 'DiscussionGrid_RenderDiscussionVote');
}
if ($Context->SelfUrl == 'comments.php' && $Context->Session->User->Permission('PERMISSION_VOTING_FOR_COMMENTS')) {

	require_once VANILLAVOTING_ROOT . 'library/Class.CommentVote.php';
	
	if (defined('JQUERY_EXTENSION')) {
		if (!defined('JQUERY_INCLUDED')) {
			includeJQuery();
		}
		
	} else {
		$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'js/jquery-1.2.1.pack.js');
	}
	$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'js/AddVote.js.php');
	
	$Head->AddStyleSheet($Configuration["VANILLAVOTING_PATH"].'theme/vote.css');
	
	function CommentManager_InitCommentVote(&$CommentManager) {
		$CommentVote = $CommentManager->Context->ObjectFactory->NewContextObject($CommentManager->Context, 'CommentVote');
		$sql = &$CommentManager->DelegateParameters['SqlBuilder'];
		$CommentVote->GetCommentVote(&$sql);
		$CommentManager->Context->DelegateParameters['CommentVote'] = &$CommentVote;
	}
	
	$Context->AddToDelegate('CommentManager', 'CommentBuilder_PreWhere', 'CommentManager_InitCommentVote');
	
	function CommentGrid_RenderCommentVote(&$CommentGrid) {
		$CommentVote = &$CommentGrid->Context->DelegateParameters['CommentVote'];
		$DataSet = &$CommentGrid->DelegateParameters['Comment']->DelegateParameters['DataSet'];
		$CommentList = &$CommentGrid->DelegateParameters['CommentList'];
		$CommentVote->Render(&$CommentList, &$DataSet);
	}
	
	$Context->AddToDelegate('CommentGrid', 'PreCommentOptionsRender', 'CommentGrid_RenderCommentVote');
}

/**
 * Settings Panel. This is where all the forms are generated.
 */
if(($Context->SelfUrl == 'settings.php') && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS')) {
	require_once VANILLAVOTING_ROOT . 'library/PostBackControl.VanillaVotingForm.php';
	
    $VanillaVotingForm= $Context->ObjectFactory->NewContextObject($Context, 'VanillaVotingForm');
    $Page->AddRenderControl($VanillaVotingForm, $Configuration["CONTROL_POSITION_BODY_ITEM"]);
    $ExtensionOptions = $Context->GetDefinition('ExtensionOptions');
    $Panel->AddList($ExtensionOptions);
    $Panel->AddListItem($ExtensionOptions, $Context->GetDefinition('VanillaVoting'), GetUrl($Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=VanillaVoting'));
}
/*
if ($Context->SelfUrl == 'account.php' && $Context->Configuration['PERMISSION_ALLOW_VOTING_FOR_DISCUSSIONS']) {
	require_once VANILLAVOTING_ROOT . 'library/Class.VanillaVotingHistory.php';
	
	if (defined('JQUERY_EXTENSION')) {
		if (!defined('JQUERY_INCLUDED')) {
			includeJQuery();
		}
		
	} else {
		$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'js/jquery-1.2.1.pack.js');
	}
	$Head->AddScript($Configuration["VANILLAVOTING_PATH"].'AddVote.js.php');
	$Head->AddStyleSheet($Configuration["VANILLAVOTING_PATH"].'theme/vote.css');
	
	$AccountUserID = ForceIncomingInt('u', $Context->Session->UserID);
	$DiscussionVoteHistory = $Context->ObjectFactory->NewContextObject($Context, 'VanillaVotingHistory', $AccountUserID);
	$Page->AddRenderControl($DiscussionVoteHistory, $Configuration["CONTROL_POSITION_BODY_ITEM"]);
	
}*/

?>