CREATE TABLE `LUM_UserDiscussionVote` (
`DiscussionID` int(8) NOT NULL,
`UserID` int(8) NOT NULL,
`Vote` tinyint(3) NOT NULL,
`DateCreated` datetime NOT NULL default '0000-00-00 00:00:00',
KEY `UserDiscussionVote_Key` (`DiscussionID`,`UserID`,`Vote`)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `LUM_UserCommentVote` (
`CommentID` int(8) NOT NULL,
`UserID` int(8) NOT NULL,
`Vote` tinyint(3) NOT NULL,
`DateCreated` datetime NOT NULL default '0000-00-00 00:00:00',
KEY `UserCommentVote_Key` (`CommentID`,`UserID`,`Vote`)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `LUM_Discussion` ADD `Vote` smallint(6) NOT NULL default '0';

ALTER TABLE `LUM_Comment` ADD `Vote` smallint(6)  NOT NULL default '0';