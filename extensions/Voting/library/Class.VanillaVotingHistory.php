<?php
class VanillaVotingHistory extends Control {

	var $VanillaVotingHistory;
	var $TotalLoved;
	var $TotalHated;
	var $UserID;
	
	function VanillaVotingHistory(&$Context, $UserID) {
		$this->Name = 'VanillaVotingHistory';
		$this->Control($Context);
		$this->UserID = $UserID;
		$this->PostBackAction = ForceIncomingString('PostBackAction', '');
		if ($this->PostBackAction == '') {
			$this->GetVanillaVotingHistoryTotals($UserID);
			$this->GetVanillaVotingHistoryPerUserID($UserID);
		}
	}

	function GetVanillaVotingHistoryTotals($UserID) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('UserDiscussionVote', 'UserDiscussionVote');
		$sql->AddSelect('Vote' ,'UserDiscussionVote');
		$sql->AddSelect('Vote', 'UserDiscussionVote', 'Total', 'COUNT');
		$sql->AddWhere('UserDiscussionVote', 'UserID', '', $UserID, '=', '', '', 0);
		$sql->AddGroupBy('Vote','UserDiscussionVote');
		
		$ResultSet = $this->Context->Database->Select($sql, $this->Name, 'GetVanillaVotingHistoryTotals', 'Failed to get love/hate totals.');
		while ($Row = $this->Context->Database->GetRow($ResultSet)) {
			if ($Row['Vote'] == 0) {
				$this->TotalHated = $Row['Total'];
			} else if ($Row['Vote'] != 0) {
				$this->TotalLoved = $Row['Total'];
			}
		}
	}

	function GetVanillaVotingHistoryPerUserID($UserID) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('UserDiscussionVote', 'UserDiscussionVote');
		$sql->AddSelect(array('DiscussionID','Vote') ,'UserDiscussionVote');
		$sql->AddJoin('Discussion', 'Discussion', 'DiscussionID', 'UserDiscussionVote', 'DiscussionID', 'left join');
		$sql->AddSelect('Name' ,'Discussion', 'Title');
		$sql->AddJoin('User', 'User', 'UserID', 'Discussion', 'AuthUserID', 'left join');
		$sql->AddSelect('Name' ,'User','UserName');
		$sql->AddWhere('UserDiscussionVote', 'UserID', '', $UserID, '=', '', '', 0);
		$sql->AddLimit(0,10);
		$this->VanillaVotingHistory = $this->Context->Database->Select($sql, $this->Name, 'GetVanillaVotingHistoryPerUserID', 'Failed to get love history.');
	}

	function Render() {
		$this->CallDelegate('PreRender');
		if ($this->PostBackAction == '') {
			echo '
				<h2><span>Total Loved ('.$this->TotalLoved.')  Total Hated ('.$this->TotalHated.')</span>'.$this->Context->GetDefinition('VanillaVotingHistory').'</h2>
				<ul class="VanillaVotingHistory">';
				$Alternate = 0;
				while ($Row = $this->Context->Database->GetRow($this->VanillaVotingHistory)) {
					$Alternate = FlipBool($Alternate);
					($Alternate) ? $class='even': $class='odd';
					echo'<li class="'.$class.'">';
					if ($this->UserID == $this->Context->Session->UserID) {
						echo '<span class="UnLove"><a href="">'.($Row['Vote'] == 0 ? $this->Context->GetDefinition('UnHate'): $this->Context->GetDefinition('UnLove')).'</a></span>';
					}
					echo '<span class="LoveResult">'.($Row['Vote'] == 0 ? $this->Context->GetDefinition('Hated'): $this->Context->GetDefinition('Loved')).'</span>
						<span class="DiscussionTitle"><a href="">'.$Row['Title'].'</a></span>
						<span class="DiscussionAuthor">by '.$Row['UserName'].'</span>
							
					</li>';
				}
				echo '</ul>';
		}
		$this->CallDelegate('PostRender');
	}
}
?>