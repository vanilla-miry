<?php
/**
 * VanillaVoting class. This is where all the Admin side forms are generated
 */
class VanillaVotingForm extends PostBackControl
{
    var $ConfigurationManager;
    var $AllowedCategory;
    var $CategoryCheckboxes;

    function VanillaVotingForm(&$Context)
    {
        $this->Name = 'VanillaVotingForm';
        $this->ValidActions = array('VanillaVoting','ProcessVanillaVoting');
	$this->Constructor($Context);

        if($this->IsPostBack)
        {
            $SettingsFile = $this->Context->Configuration['APPLICATION_PATH'].'conf/settings.php';
            $this->ConfigurationManager = $this->Context->ObjectFactory->NewContextObject($this->Context, 'ConfigurationManager');
            if ($this->PostBackAction == 'ProcessVanillaVoting')
            {
                $this->ConfigurationManager->GetSettingsFromForm($SettingsFile);
                $this->ConfigurationManager->DefineSetting('VANILLAVOTING_CATEGORYID', implode(",", $_POST['AllowedCategory']), 0);
		$this->DelegateParameters['ConfigurationManager'] = &$this->ConfigurationManager;
                // And save everything
		if ($this->ConfigurationManager->SaveSettingsToFile($SettingsFile)) {
		    header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=VanillaVoting&Success=1'));
		} else {
		    $this->PostBackAction = 'VanillaVoting';
		}
            }
        }
    }

    function Render()
    {
        if ($this->IsPostBack)
	{
            $this->PostBackParams->Clear();
	    if ($this->PostBackAction == 'VanillaVoting')
            {
               $this->AllowedCategory = explode(",", $this->Context->Configuration["VANILLAVOTING_CATEGORYID"]);
                // Load the category selector
		$cm = $this->Context->ObjectFactory->NewContextObject($this->Context, 'CategoryManager');
		$CategoryData = $cm->GetCategories(0, 1);
		$cat = $this->Context->ObjectFactory->NewObject($this->Context, 'Category');
		while ($Row = $this->Context->Database->GetRow($CategoryData)) {
			$cat->Clear();
			$cat->GetPropertiesFromDataSet($Row);
                        $this->CategoryCheckboxes .= "<li><p><span>".GetDynamicCheckBox("AllowedCategory[]", $cat->CategoryID,
                                                    (in_array($cat->CategoryID, $this->AllowedCategory)) ? 1 : 0,
                                                    "",$cat->Name,"","Category_".$cat->CategoryID )."</span></p></li>\r\n";
		}

                $this->PostBackParams->Set('PostBackAction', 'ProcessVanillaVoting');

                echo '<div id="Form" class="Account VanillaVotingSettings Preferences">';
                if (ForceIncomingInt('Success', 0)) echo '<div id="Success">'.$this->Context->GetDefinition('ChangesSaved').'</div>';
                echo '<fieldset>
                	<legend>'.$this->Context->GetDefinition("VanillaVoting").'</legend>
                	'.$this->Get_Warnings().'
                	'.$this->Get_PostBackForm('frmVanillaVoting').'
                        <ul>
                            <li>
                                <p class="Description"><strong>'.$this->Context->GetDefinition("VanillaVotingCategory").'</strong></p>
                                <p class="Description">Check: <a onclick="CheckAll(\'Category_\'); return false;" href="./">'.$this->Context->GetDefinition('CategoriesAll').'</a>, <a onclick="CheckNone(\'Category_\'); return false;" href="./">'.$this->Context->GetDefinition('CategoriesNone').'</a></p>
                             </li>
                            '.$this->CategoryCheckboxes.'
                        </ul>
                        <div class="Submit">
                            <input type="submit" name="btnSave" value="'.$this->Context->GetDefinition('Save').'" class="Button SubmitButton" />
                            <a href="'.GetUrl($this->Context->Configuration, $this->Context->SelfUrl).'" class="CancelButton">'.$this->Context->GetDefinition('Cancel').'</a>
                        </div>
                        </form>
                    </fieldset>
                </div>';

            }
        }
    }
}
?>
