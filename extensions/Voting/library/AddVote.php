<?php
/*
* Description: File used by Dynamic Data Management object to add/delete vote per discussion
*/

include('../../../appg/settings.php');
include('../../../appg/init_ajax.php');

$PostBackKey = ForceIncomingString('PostBackKey', '');
$ExtensionKey = ForceIncomingString('ExtensionKey', '');
if ($PostBackKey != '' && $PostBackKey == $Context->Session->GetVariable('SessionPostBackKey', 'string')) {
	
	$CurrentTotalVotes = ForceIncomingInt('CurrentTotalVotes', 0);
	$Vote = (int)ForceIncomingString('Vote', 0);
	$DiscussionID = ForceIncomingInt('DiscussionID', 0);
	$CommentID = ForceIncomingInt('CommentID', 0);
	// Is it Discussion Vote
	if ($DiscussionID != 0) {
		include('Class.DiscussionVote.php');
		$DiscussionVote = $Context->ObjectFactory->NewContextObject($Context, 'DiscussionVote');
		if ($Vote != 0) { // Add Vote
			$DiscussionVote->AddDiscussionVote($DiscussionID, $Vote, $CurrentTotalVotes);
		} else if ($Vote == 0){ // Delete vote from database
			$DiscussionVote->DeleteDiscussionVote($DiscussionID, $Vote, $CurrentTotalVotes);
		}
		echo 'Complete';
		
	// Is it Comment Vote
	} else if ($CommentID != 0) {
		include('Class.CommentVote.php');
		$CommentVote = $Context->ObjectFactory->NewContextObject($Context, 'CommentVote');
		if ($Vote != 0) { // Add Vote
			$CommentVote->AddCommentVote($CommentID, $Vote, $CurrentTotalVotes);
		}
		echo 'Complete';
	}
} else {
	echo $Context->GetDefinition('ErrPostBackKeyInvalid');
}
$Context->Unload();

?>