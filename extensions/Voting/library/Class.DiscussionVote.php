<?php
class DiscussionVote {
	
	var $Name;
	var $Context;
	var $CategoryID;
	var $SessionPostBackKey;
	var $PositiveVote;
	var $NegativeVote;

	function DiscussionVote(&$Context) {
		$this->Name = 'DiscussionVote';
		$this->Context = &$Context;
		$this->CategoryID = explode(",", $this->Context->Configuration['VANILLAVOTING_CATEGORYID']);
		$this->SessionPostBackKey = $this->Context->Session->GetVariable('SessionPostBackKey', 'string');
		$this->PositiveVote = 1;
		$this->NegativeVote = -1;
	}
	
	function GetDiscussionVote (&$sql) {
		$sql->AddSelect('Vote', 't','TotalDiscussionVotes');
		$sql->AddJoin('UserDiscussionVote', 'UserDiscussionVote', 'DiscussionID', 't', 'DiscussionID', 'left join', ' and UserDiscussionVote.'.$this->Context->DatabaseColumns['UserDiscussionVote']['UserID'].' = '.$this->Context->Session->UserID);
		$sql->AddSelect('Vote', 'UserDiscussionVote');
	}
	
	function AddDiscussionVote ($DiscussionID, $Vote, $CurrentTotalVotes) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('UserDiscussionVote', 'UserDiscussionVote');
		$sql->AddFieldNameValue('UserID', $this->Context->Session->UserID);
		$sql->AddFieldNameValue('DiscussionID', $DiscussionID);
		$sql->AddFieldNameValue('Vote', $Vote);
		$sql->AddFieldNameValue('DateCreated', MysqlDateTime());
		$this->Context->Database->Insert($sql, $this->Name, 'AddDiscussionVote', 'Failed to add discussion vote');
		$this->UpdateDiscussionTotalVotes ($DiscussionID, $Vote, $CurrentTotalVotes);
		
	}
	function DeleteDiscussionVote ($DiscussionID, $Vote, $CurrentTotalVotes) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('UserDiscussionVote', 'UserDiscussionVote');
		$sql->AddWhere('UserDiscussionVote', 'DiscussionID', '', $DiscussionID, '=', '', '', 0, 1);
		$sql->AddWhere('UserDiscussionVote', 'UserID', '', $this->Context->Session->UserID, '=', 'AND', '', 0);
		$sql->EndWhereGroup();
		$this->Context->Database->Delete($sql, $this->Name, 'DeleteDiscussionVote', 'Failed to delete discussion vote');
		
		//Now update the total votes for the discussion
		$this->UpdateDiscussionTotalVotes ($DiscussionID, $Vote, $CurrentTotalVotes);
	}
	function UpdateDiscussionTotalVotes ($DiscussionID, $Vote, $CurrentTotalVotes) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('Discussion', 'Discussion');
		$sql->AddFieldNameValue('Vote', $CurrentTotalVotes+$Vote);
		$sql->AddWhere('Discussion', 'DiscussionID', '', $DiscussionID, '=');
		$this->Context->Database->Update($sql, $this->Name, 'UpdateDiscussionTotalVotes', 'Failed to update discussion total votes');
	}
	
	function Render(&$DiscussionList, &$DataSet) {
		
		if (in_array($DataSet['CategoryID'], $this->CategoryID)) {
			$verdict = '';
			
			if ($DataSet['Vote'] < 0) {
				$verdict = 'VotedNegative';
			} else if ($DataSet['Vote'] > 0) {
				$verdict = 'VotedPositive';
			}
			
			$DiscussionList .= '<ul class="DiscussionVote '.$verdict.'">
						<li class="DiscussionVoteCount"><span>'.$DataSet['TotalDiscussionVotes'].'</span><span class="DiscussionVoteCountLabel"> '.$this->Context->GetDefinition('Votes').'</span></li>
						<li class="DiscussionAddVote">';
			if ($this->Context->Session->UserID > 0) {
				$DiscussionList .= '<form action="'.GetRequestUri().'" method="get">';
				if (is_null($DataSet['Vote'])) {
					$DiscussionList .= '<span class="Vote"><input class="VotePositive" type="button" onClick="AddDiscussionVote(\''.$this->Context->Configuration["VANILLAVOTING_PATH"].'library/AddVote.php\', '.$DataSet['DiscussionID'].', '.$this->PositiveVote.', '.$DataSet['TotalDiscussionVotes'].', \''.$this->SessionPostBackKey.'\');" /><input type="button" class="VoteNegative" onClick="AddDiscussionVote(\''.$this->Context->Configuration["VANILLAVOTING_PATH"].'library/AddVote.php\', '.$DataSet['DiscussionID'].', '.$this->NegativeVote.', '.$DataSet['TotalDiscussionVotes'].', \''.$this->SessionPostBackKey.'\');" /></span>';
				} else if ($DataSet['Vote'] < 0) {
						$DiscussionList .= '<span class="VotedNegative"><input class="VotePositive" type="button" disabled="disabled" /><input type="button"  class="VoteNegative" disabled="disabled"/></span>';
				} else if ($DataSet['Vote'] > 0) {
						$DiscussionList .= '<span class="VotedPositive"><input class="VotePositive" type="button" disabled="disabled" /><input type="button"  class="VoteNegative" disabled="disabled"/></span>';
				}
				$DiscussionList .='</form>';
			}
			else {
				$DiscussionList .= '<span class="NotLoggedIn"><a href="'.AppendUrlParameters($this->Context->Configuration['SIGNIN_URL'], 'ReturnUrl='.GetRequestUri()).'">'.$this->Context->GetDefinition('Vote!').'</a></span>';
			}
			$DiscussionList .= '</li>
			</ul>';
		}
	}
	
}
?>