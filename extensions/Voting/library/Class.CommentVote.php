<?php
class CommentVote {
	
	var $Name;
	var $Context;
	var $CategoryID;
	var $SessionPostBackKey;
	var $PositiveVote;
	var $NegativeVote;	

	function CommentVote(&$Context) {
		$this->Name = 'CommentVote';
		$this->Context = &$Context;
		$this->CategoryID = explode(",", $this->Context->Configuration['VANILLAVOTING_CATEGORYID']);
		$this->SessionPostBackKey = $this->Context->Session->GetVariable('SessionPostBackKey', 'string');
		$this->PositiveVote = 1;
		$this->NegativeVote = -1;		
	}
	
	function GetCommentVote (&$sql) {
		$sql->AddSelect('Vote', 'm','TotalCommentVotes');
		$sql->AddJoin('UserCommentVote', 'UserCommentVote', 'CommentID', 'm', 'CommentID', 'left join', ' and UserCommentVote.'.$this->Context->DatabaseColumns['UserCommentVote']['UserID'].' = '.$this->Context->Session->UserID);
		$sql->AddSelect('Vote', 'UserCommentVote');
	}
	
	function AddCommentVote ($CommentID, $Vote, $CurrentTotalVotes) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('UserCommentVote', 'UserCommentVote');
		$sql->AddFieldNameValue('UserID', $this->Context->Session->UserID);
		$sql->AddFieldNameValue('CommentID', $CommentID);
		$sql->AddFieldNameValue('Vote', $Vote);
		$sql->AddFieldNameValue('DateCreated', MysqlDateTime());
		$this->Context->Database->Insert($sql, $this->Name, 'AddCommentVote', 'Failed to add Comment vote');
		$this->UpdateCommentTotalVotes ($CommentID, $Vote, $CurrentTotalVotes);
	}
	function UpdateCommentTotalVotes ($CommentID, $Vote, $CurrentTotalVotes) {
		$sql = $this->Context->ObjectFactory->NewContextObject($this->Context, 'SqlBuilder');
		$sql->SetMainTable('Comment', 'Comment');
		$sql->AddFieldNameValue('Vote', $CurrentTotalVotes+$Vote);
		$sql->AddWhere('Comment', 'CommentID', '', $CommentID, '=');
		$this->Context->Database->Update($sql, $this->Name, 'UpdateCommentTotalVotes', 'Failed to update Comment total votes');
	}
	
	function Render(&$CommentList, &$DataSet) {
		
		$verdict = '';
			
		if ($DataSet['Vote'] < 0) {
			$verdict = 'VotedNegative';
			$CommentList .= '<span class="BelowViewingThreshold"><a href="'.GetRequestUri().'" onClick="ShowComment('.$DataSet['CommentID'].');return false;">'.$this->Context->GetDefinition('ShowComment').'</a></span>';
			
		} else if ($DataSet['Vote'] > 0) {
			$verdict = 'VotedPositive';
		}
		
		$CommentList .= '<span class="CommentVoteCount '.$verdict.'">'.$DataSet['TotalCommentVotes'].'</span><span class="CommentVoteCountLabel"> '.$this->Context->GetDefinition('Votes').'</span>';
		$CommentList .= '<span class="AddCommentVote">';
		if ($this->Context->Session->UserID > 0) {
			if (is_null($DataSet['Vote'])) {
				$CommentList .= '<span class="Vote"><input class="VotePositive" type="button" onClick="AddCommentVote(\''.$this->Context->Configuration["VANILLAVOTING_PATH"].'library/AddVote.php\', '.$DataSet['CommentID'].', '.$this->PositiveVote.', '.$DataSet['TotalCommentVotes'].', \''.$this->SessionPostBackKey.'\');" /><input type="button" class="VoteNegative" onClick="AddCommentVote(\''.$this->Context->Configuration["VANILLAVOTING_PATH"].'library/AddVote.php\', '.$DataSet['CommentID'].', '.$this->NegativeVote.', '.$DataSet['TotalCommentVotes'].', \''.$this->SessionPostBackKey.'\');" /></span>';
			} else if ($DataSet['Vote'] < 0) {
				$CommentList .= '<span class="VotedNegative"><input class="VotePositive" type="button" disabled="disabled" /><input type="button"  class="VoteNegative" disabled="disabled"/></span>';
			} else if ($DataSet['Vote'] > 0) {
				$CommentList .= '<span class="VotedPositive"><input class="VotePositive" type="button" disabled="disabled" /><input type="button"  class="VoteNegative" disabled="disabled"/></span>';
			}
		}
		else {
			$CommentList .= '<a href="'.AppendUrlParameters($this->Context->Configuration['SIGNIN_URL'], 'ReturnUrl='.GetRequestUri()).'">'.$this->Context->GetDefinition('Vote').'</a>';
		}
		$CommentList .= '</span>';
		
	}
}
?>