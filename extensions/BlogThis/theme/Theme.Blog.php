<?php

global $Configuration;

$UnreadUrl = GetUnreadQuerystring($BlogPost, $this->Context->Configuration, $CurrentUserJumpToLastCommentPref);
$NewUrl = GetUnreadQuerystring($BlogPost, $this->Context->Configuration, 1);
$LastUrl = GetLastCommentQuerystring($BlogPost, $this->Context->Configuration, $CurrentUserJumpToLastCommentPref);

//Adsense section targetting
if 	($this->Context->Configuration['BLOG_ADSENSE'] == 1)
	{
	$adsensestart="<!-- google_ad_section_start -->"; 
	$adsenseend="<!-- google_ad_section_end -->";
	}
else 
	{
	$adsensestart="";
	$adsenseend="";
	}

$BlogList .= '
<li id="Blog_'.$BlogPost->DiscussionID.'" class="Blog'.$BlogPost->Status.($BlogPost->CountComments == 1?' NoReplies':'').($Alternate ? ' Alternate' : '').($this->RowNumber == 1 ? ' First' : '').($this->RowNumber == $this->Context->Configuration['BLOG_PER_PAGE'] ? ' Last' : '').' ">
<ul>
   <li class="BlogTitle"> <!--Blog Post Title-->'.$adsensestart.'
        <h2><a href="'.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', 'Focus='.$BlogPost->CommentID.'#Comment_'.$BlogPost->CommentID, CleanupString($BlogPost->DiscussionName).'/').'">'.$BlogPost->DiscussionName.'</a></h2>'.$adsenseend.'
   </li>
   <li class="BlogInfo"> <!--Blog Post Author and Date and Category-->
         <em>by</em><a href="'.GetUrl($this->Context->Configuration, 'account.php', '', 'u', $BlogPost->AuthUserID).'">'.$BlogPost->DisplayName.'</a>
        <em>on</em>'.date($this->Context->Configuration['BLOG_DATE'], $BlogPost->DateCreated).' <em>in</em>'.$adsensestart.'<a href="'.GetUrl($this->Context->Configuration, 'index.php', '', 'CategoryID', $BlogPost->CategoryID).'">'.$BlogPost->CategoryName.'</a>'.$adsenseend.' 
    </li>
    <li class="BlogContent"> <!--Blog Post Content-->'.$adsensestart.'
         '.$CommentBody['main'];
        if ($CommentBody['extended'] != '')
                $BlogList .= '<p class="more"><a href="'.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', 'Focus='.$BlogPost->CommentID.'#Comment_', CleanupString($BlogPost->DiscussionName).'/').'">'.$this->Context->GetDefinition('More').'</a></p>';
$BlogList .= ''.$adsenseend.'</li>';

$BlogList .= '<li class="BlogFooter"><div class="BlogMoreInfo">';
if ($BlogPost->Closed == 0   && $BlogPost->CountComments == 1) {
       $BlogList .= '<span class="BlogAddComments"><a href="'.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '#pgbottom', CleanupString($BlogPost->DiscussionName).'/').'">'.$this->Context->GetDefinition('AddComments').'</a></span>';
}

if ($BlogPost->CountComments > 1) {
    $BlogPost->CountComments--;
    $BlogList .= '<span class="BlogComments"><a href="'.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '#pgbottom', CleanupString($BlogPost->DiscussionName).'/').'">'.$this->Context->GetDefinition('Comments').': '.$BlogPost->CountComments.'</a></span>';

}

if ($this->Context->Session->UserID > 0) {
    if ($BlogPost->AuthUserID == $this->Context->Session->UserID || $PERMISSION_EDIT_COMMENTS || $PERMISSION_EDIT_DISCUSSIONS) {
        if ((!$BlogPost->Closed && $BlogPost->Active) || $PERMISSION_EDIT_COMMENTS || $PERMISSION_EDIT_DISCUSSIONS)
            $BlogList .= '<span class="BlogEdit"><a href="'.GetUrl($this->Context->Configuration, 'post.php', 'category/', '','','','CommentID=').$BlogPost->CommentID.'">'.$this->Context->GetDefinition('Edit').'</a></span>';
    }
}
$BlogList .= '</div>';

$BlogList .= '<div class="BlogBookmarks">';
if ($this->Context->Configuration['BLOG_SOCIALBOOKMARKS_DIGG'] == 1)
    $BlogList .= '<a target="_blank" href="http://digg.com/submit?phase=2&url='.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '', CleanupString($BlogPost->DiscussionName).'/').'&title='.CleanupString($BlogPost->DiscussionName).'"><img src="'.$this->Context->Configuration["BASE_URL"].'extensions/BlogThis/theme/images/digg.gif" /></a>';
if ($this->Context->Configuration['BLOG_SOCIALBOOKMARKS_DELICIOUS'] == 1)
    $BlogList .= '<a target="_blank" href="http://del.icio.us/post?v=4&noui&jump=close&url='.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '', CleanupString($BlogPost->DiscussionName).'/').'&title='.CleanupString($BlogPost->DiscussionName).'"><img src="'.$this->Context->Configuration["BASE_URL"].'extensions/BlogThis/theme/images/delicious.gif" /></a>';
if ($this->Context->Configuration['BLOG_SOCIALBOOKMARKS_STUMBLE'] == 1)
    $BlogList .= '<a href="http://www.stumbleupon.com/newurl.php?url='.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '', CleanupString($BlogPost->Name).'/').'&rating=1" target="_blank"><img src="'.$this->Context->Configuration["BASE_URL"].'extensions/BlogThis/theme/images/stumble.gif" /></a>';
if ($this->Context->Configuration['BLOG_SOCIALBOOKMARKS_TECHNORATI'] == 1)
    $BlogList .= '<a href="http://technorati.com/faves?add='.GetUrl($this->Context->Configuration, 'comments.php', '', 'DiscussionID', $BlogPost->DiscussionID, '', '', CleanupString($BlogPost->DiscussionName).'/').'&rating=1" target="_blank"><img src="'.$this->Context->Configuration["BASE_URL"].'extensions/BlogThis/theme/images/technorati.gif" /></a>';
    $BlogList .= '</div>';

$BlogList .= '</li>';
   $this->DelegateParameters['BlogList'] = &$BlogList;
   $this->DelegateParameters['RowNumber'] = &$this->RowNumber;

   $this->CallDelegate('PostDiscussionOptionsRender');

$BlogList .= '</ul>';

?>
