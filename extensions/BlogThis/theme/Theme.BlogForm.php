<?php
echo '<div id="Form" class="Account BlogSettings Preferences">';

if	(ForceIncomingInt('Success', 0))
	{
	echo '<div id="Success">'.$this->Context->GetDefinition('ChangesSaved').'</div>';
	}

echo '<fieldset>
	<legend>'.$this->Context->GetDefinition("BlogSettings").'</legend>
	'.$this->Get_Warnings().'
	'.$this->Get_PostBackForm('frmBlog').'
	<ul>
		<li>
                <label for="txtBlogNotes">'.$this->Context->GetDefinition("BlogNotes").'</label>
            </li>	    
            <li>
                <label for="txtBlogTabName">'.$this->Context->GetDefinition("BlogTabName").'</label>
		<input type="text" name="BLOG_TAB_NAME" id="txtBlogTabName"  value="'.$this->ConfigurationManager->GetSetting('BLOG_TAB_NAME').'" maxlength="255" class="SmallInput" />
            </li>	    
	    <li>
                <label for="txtBlogTitle">'.$this->Context->GetDefinition("BlogTitle").'</label>
		<input type="text" name="BLOG_TITLE" id="txtBlogTitle"  value="'.$this->ConfigurationManager->GetSetting('BLOG_TITLE').'" maxlength="255" class="SmallInput" />
            </li>
            <li>
                <label for="txtBlogPostPerPage">'.$this->Context->GetDefinition("BlogPostPerPage").'</label>
		<input type="text" name="BLOG_PER_PAGE" id="txtBlogPostPerPage"  value="'.$this->ConfigurationManager->GetSetting('BLOG_PER_PAGE').'" maxlength="3" class="SmallInput" />
            </li>
	    <li>
                <label for="txtWordLimit">'.$this->Context->GetDefinition("WordLimit").'</label>
		<input type="text" name="BLOG_WORD_LIMIT" id="txtWordLimit"  value="'.$this->ConfigurationManager->GetSetting('BLOG_WORD_LIMIT').'" maxlength="4" class="SmallInput" />
            </li>
            <li>
                <label for="txtBlogDate">'.$this->Context->GetDefinition("BlogDate").'</label>
		<input type="text" name="BLOG_DATE" id="txtBlogDate"  value="'.$this->ConfigurationManager->GetSetting('BLOG_DATE').'" maxlength="20" class="SmallInput" />
                <p class="Description" id="ExamplesLink"><a href="#" onclick="document.getElementById(\'Examples\').style.display = \'block\';document.getElementById(\'ExamplesLink\').style.display = \'none\';">'.$this->Context->GetDefinition("Examples").'</a></p>

                <table class="Description" id="Examples" style="display:none">
                <tr>
                    <td width="60px">F j, Y</td><td>'.date("F j, Y").'</td>
                </tr>
                <tr>
                    <td>m.d.y</td><td>'.date("m.d.y").'</td>
                </tr>
                <tr>
                    <td>j, n, Y</td><td>'.date("j, n, Y").'</td>
                </tr>
                <tr>
                    <td>Ymd</td><td>'.date("Ymd").'</td>
                </tr>
                <tr>
                    <td>D M j</td><td>'.date("D M j").'</td>
                </tr>
                <tr>
                    <td>j-m-y</td><td>'.date("j-m-y").'</td>
                </tr>
                <tr><td colspan="2"><a href="http://us.php.net/manual/en/function.date.php" target="_blank">'.$this->Context->GetDefinition("MoreExamples").'</a></td></tr>
                </table>

            </li>

            <li>
                <label for="txtBlogSocialBookmarks">'.$this->Context->GetDefinition("BlogSocialBookmarks").'</label>
                <p><span>'.GetDynamicCheckBox('BLOG_SOCIALBOOKMARKS_DIGG', 1, $this->ConfigurationManager->GetSetting('BLOG_SOCIALBOOKMARKS_DIGG'), '', $this->Context->GetDefinition('BlogSocialBookmarksDigg')).'</span></p>
                <p><span>'.GetDynamicCheckBox('BLOG_SOCIALBOOKMARKS_DELICIOUS', 1, $this->ConfigurationManager->GetSetting('BLOG_SOCIALBOOKMARKS_DELICIOUS'), '', $this->Context->GetDefinition('BlogSocialBookmarksDelicious')).'</span></p>
                <p><span>'.GetDynamicCheckBox('BLOG_SOCIALBOOKMARKS_STUMBLE', 1, $this->ConfigurationManager->GetSetting('BLOG_SOCIALBOOKMARKS_STUMBLE'), '', $this->Context->GetDefinition('BlogSocialBookmarksStumble')).'</span></p>
                <p><span>'.GetDynamicCheckBox('BLOG_SOCIALBOOKMARKS_TECHNORATI', 1, $this->ConfigurationManager->GetSetting('BLOG_SOCIALBOOKMARKS_TECHNORATI'), '', $this->Context->GetDefinition('BlogSocialBookmarksTechnorati')).'</span></p>
	    </li>
            <li>
                <label for="txtBlogAdsense">'.$this->Context->GetDefinition("BlogExtra").'</label>
                <p><span>'.GetDynamicCheckBox('BLOG_ADSENSE', 1, $this->ConfigurationManager->GetSetting('BLOG_ADSENSE'), '', $this->Context->GetDefinition('BlogAdsense')).'</span></p>
                <p><span>'.GetDynamicCheckBox('BLOG_PROFILETEXT', 1, $this->ConfigurationManager->GetSetting('BLOG_PROFILETEXT'), '', $this->Context->GetDefinition('BlogProfileText')).'</span></p>
                <p><span>'.GetDynamicCheckBox('BLOG_FULLNAME', 1, $this->ConfigurationManager->GetSetting('BLOG_FULLNAME'), '', $this->Context->GetDefinition('BlogFullName')).'</span></p>
	   </li>
            <li>
	    </li>
	</ul>
	<div class="Submit">
            <input type="submit" name="btnSave" value="'.$this->Context->GetDefinition('Save').'" class="Button SubmitButton" />
	    <a href="'.GetUrl($this->Context->Configuration, $this->Context->SelfUrl).'" class="CancelButton">'.$this->Context->GetDefinition('Cancel').'</a>
	</div>
    </form>
    </fieldset>
</div>';

?>
