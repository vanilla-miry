<?php
/*
Extension Name: BlogThis
Extension Url: http://lussumo.com/addons/index.php?PostBackAction=AddOn&AddOnID=364
Description: Blog page built from any comments from any discussions chosen.
Version: 1.14
Author: Andrew Miller (Spode)
Author Url: http://www.thinkabouttech.com
*/

/*
Based on the original "Blog" extension by Ziyad (MySchizoBuddy) Saeed.
*/

//Permissions
// Can blog/unblog any comment.
$Context->Configuration['PERMISSION_CANBLOGALL'] = '0';
// Can blog/unblog own comments.
$Context->Configuration['PERMISSION_CANBLOGTHIS'] = '0';
// Allow HTML in BlogThis profile?
$Context->Configuration['PERMISSION_BLOGPROFILEHTML'] = '0';

// Blog Path
$BlogPath = dirname(__FILE__);

// Libraries
include($BlogPath . '/library/Function.BlogThis.php');
include($BlogPath . '/library/Control.BlogThis.php');
include($BlogPath . '/library/PostBackControl.BlogForm.php');

// BlogThis Preferences
if( !array_key_exists('BLOG_TAB_NAME', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_TAB_NAME', 'Blog');}
if( !array_key_exists('BLOG_TAB_POSITION', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_TAB_POSITION', '1');}
if( !array_key_exists('BLOG_ADSENSE', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_ADSENSE', '0');}
if( !array_key_exists('BLOG_SOCIALBOOKMARKS_DIGG', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_SOCIALBOOKMARKS_DIGG', '0');}
if( !array_key_exists('BLOG_SOCIALBOOKMARKS_DELICIOUS', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_SOCIALBOOKMARKS_DELICIOUS', '0');}
if( !array_key_exists('BLOG_SOCIALBOOKMARKS_STUMBLE', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_SOCIALBOOKMARKS_STUMBLE', '0');}
if( !array_key_exists('BLOG_SOCIALBOOKMARKS_TECHNORATI', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_SOCIALBOOKMARKS_TECHNORATI', '0');}
if( !array_key_exists('BLOG_PER_PAGE', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_PER_PAGE', '10');}
if( !array_key_exists('BLOG_WORD_LIMIT', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_WORD_LIMIT', '0');}
if( !array_key_exists('BLOG_DATE', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_DATE', 'jS F, Y');}
if( !array_key_exists('BLOG_TITLE', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_TITLE', 'My Blog');}
if( !array_key_exists('BLOG_PROFILETEXT', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_PROFILETEXT', '1');}
if( !array_key_exists('BLOG_FULLNAME', $Configuration)) {AddConfigurationSetting($Context, 'BLOG_FULLNAME', '0');}

//Install BlogThis
if 	(!array_key_exists('BLOGTHIS', $Configuration) || @$_GET['UpgradeBlogThis'])
	{
	//check to see if the column exists in the database
	$query = "SHOW COLUMNS FROM `".$Configuration['DATABASE_TABLE_PREFIX']."Comment` LIKE 'BlogThis';";
	$result = $Context->Database->Execute($query,'','','An error occured checking for the existence of the BlogThis field.');
	$result = $Context->Database->GetRow($result);

	//BlogThis field doesn't exist!
	if 	(!$result)
		{
		$query = "ALTER TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."Comment` ADD `BlogThis` TINYINT(1) DEFAULT 0 NOT NULL ;";
                $result = $Context->Database->Execute($query,'','','An error occured adding the BlogThis field - does your database user have the correct priviliges?.');
		}

	$query = "SHOW COLUMNS FROM `".$Configuration['DATABASE_TABLE_PREFIX']."User` LIKE 'ProfileText';";
	$result = $Context->Database->Execute($query,'','','An error occured checking for the existence of the ProfileText field.');
	$result = $Context->Database->GetRow($result);

		
	if 	(!$result)
		{
		$query = "ALTER TABLE `".$Configuration['DATABASE_TABLE_PREFIX']."User` ADD `ProfileText` TEXT;";
                $result = $Context->Database->Execute($query,'','','An error occured adding the ProfileText field - does your database user have the correct priviliges?.');
		}

	//Add configurations
	AddConfigurationSetting($Context, 'BLOGTHIS', '1');
	}

$DatabaseColumns['Comment']['BlogThis'] = 'BlogThis';
$DatabaseColumns['User']['ProfileText'] = 'ProfileText';

//if the user has permission, add the blogthis buttons
if 	($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') || $Context->Session->User->Permission('PERMISSION_CANBLOGALL'))
	{
	$Context->AddToDelegate("CommentGrid", "PostCommentOptionsRender", "AddBlogThis");
	
	//As they can Blog, also enable ability to modify blog profile
	if ($Context->Configuration['BLOG_PROFILETEXT'])
		{	
		$Context->AddToDelegate("IdentityForm", "PreCustomInputsRender", "AddModifyProfile");
		$Context->AddToDelegate("UserManager", "PreIdentityUpdate", "AddProfileText");
		}
	}
	
//Add tab to main menu
if 	(isset($Menu))
	{
	$Menu->AddTab($Configuration['BLOG_TAB_NAME'], 'Blog', GetUrl($Configuration, 'extension.php', '', '', '', '',  'PostBackAction=Blog'), '', $Context->Configuration['BLOG_TAB_POSITION']);
	}

//Add stylesheet for the user account page
if 	($Context->SelfUrl == 'account.php')
	{
	$Head->AddStyleSheet( 'extensions/BlogThis/theme/accountstyle.css');
	}

//Show Blog User Profiles
if ($Context->Configuration['BLOG_PROFILETEXT'])
	{
	$Context->AddToDelegate("Account", "PostProfileRender", "AddProfileDetails");
	}

//add text link for viewing blog posts
$Context->AddToDelegate("Account", "PostProfileRender", "AddViewBlogPosts");

//UnBlog/Blog a post, assuming correct permissions and on the right page
if 	(in_array($Context->SelfUrl, array("comments.php")) && ($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') || $Context->Session->User->Permission('PERMISSION_CANBLOGALL')))
	{
	
	if (ForceIncomingString('PostBackAction', '') == 'BlogThis') {$blogvalue = 1;}
	if (ForceIncomingString('PostBackAction', '') == 'UnBlogThis') {$blogvalue = 0;}

	$cmid = ForceIncomingString('BlogCommentID', '');

	if 	(($cmid) && isset($blogvalue))
		{
		$sql = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
		$sql->SetMainTable('Comment','c');
		$sql->AddFieldNameValue('BlogThis', $blogvalue, 0);
		$sql->AddWhere('c', 'CommentID', '', FormatStringForDatabaseInput($cmid), '=');
		
		//stops people without permission blogging other people's posts
		if ($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') && !$Context->Session->User->Permission('PERMISSION_CANBLOGALL'))
			{
			$sql->AddWhere('c', 'AuthUserID', '', $Context->Session->UserID, '=');
			}
			
		$result = $Context->Database->Update ($sql, '', '' , 'Could not change the Blogged/UnBlogged status of the comment.');
		
		//if it returns 0, either the user doesn't have permission, or something odd is going on!
		if ($result)
			{
			$Context->AddToDelegate("CommentGrid", "PreRender", "DisplaySuccess");
			}
		else
			{
			$Context->AddToDelegate("CommentGrid", "PreRender", "DisplayFail");
			}
		}
	}

//If this is the "Blog" page, display the blog grid

if 	($Context->SelfUrl == 'extension.php' && ForceIncomingString('PostBackAction', '') == 'Blog')
	{
	$Head->AddStyleSheet( 'extensions/BlogThis/theme/style.css');
	$Head->BodyId = 'Blog';
	
	//changes current tab to Blog
	if (isset($Menu)) $Menu->CurrentTab = 'Blog';
	
	$Context->PageTitle = $Context->Configuration['BLOG_TITLE'];
	$Blog = $Context->ObjectFactory->NewContextObject($Context, 'Blog');
	$Page->AddRenderControl($Blog, $Configuration['CONTROL_POSITION_BODY_ITEM']);
	}

//Settings panel for BlogThis
if	(($Context->SelfUrl == 'settings.php') && $Context->Session->User->Permission('PERMISSION_CHANGE_APPLICATION_SETTINGS'))
	{
	$BlogForm = $Context->ObjectFactory->NewContextObject($Context, 'BlogForm');
	$Page->AddRenderControl($BlogForm, $Configuration["CONTROL_POSITION_BODY_ITEM"]);
	$ExtensionOptions = $Context->GetDefinition('ExtensionOptions');
	$Panel->AddList($ExtensionOptions);
	$Panel->AddListItem($ExtensionOptions, $Context->GetDefinition('BlogSettings'), GetUrl($Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=Blog'));
	}

//this was added in for compatibility with FeedPublisher and now FeedThis
//The url /search.php?PostBackAction=Search&Type=Comments&BlogSearch=1&Feed=RSS2 is used in FeedPublisher. FeedThis works automatically.
$Context->AddToDelegate("CommentManager", "SearchBuilder_PostWhere", "Search_AddQuery");

//Adds the "Start a blog post" link
$Context->AddToDelegate("Panel", "PostStartButtonRender", "AddStartBlog");

//Add extra bits to post page
if	(($Context->SelfUrl == 'post.php') && ($Context->Session->User->Permission('PERMISSION_CANBLOGALL') || $Context->Session->User->Permission('PERMISSION_CANBLOGTHIS')))
	{
	$Head->AddStyleSheet('extensions/BlogThis/theme/post.css');
	$Context->AddToDelegate("DiscussionForm", "DiscussionForm_PostButtonsRender", "AddBlogTick");
	$Context->AddToDelegate("DiscussionForm", "PostSaveDiscussion", "PostBlogThis");
	}

?>
