<?php
//Spanish definition files
$Context->SetDefinition('Blog', 'Blog');
$Context->SetDefinition('BlogThis', 'publicar en blog');
$Context->SetDefinition('UnBlogThis', 'despublicar de blog');
$Context->SetDefinition('BlogTitle', 'What is the blog page title?');
$Context->SetDefinition('BlogTabName', 'What is the text that should be displayed in the tab?');
$Context->SetDefinition('BlogExtra', 'Opciones adicionales');
$Context->SetDefinition('BlogAdsense', 'Add Adsense section targetting to blog posts and titles?');
$Context->SetDefinition('BlogSettings', 'Exportaci&oacute;n al blog');
$Context->SetDefinition('BlogNotes', 'These settings are specific to the BlogThis extension. Make sure that your user has permission to Blog/UnBlog posts. Be aware that there is a global character limit that will also affect BlogThis. ');
$Context->SetDefinition('BlogPostPerPage', 'How many blog posts should be displayed per page?');
$Context->SetDefinition('BlogSocialBookmarks', 'Which social bookmarking links should be added?');
$Context->SetDefinition('BlogSocialBookmarksDigg', 'Digg it');
$Context->SetDefinition('BlogSocialBookmarksDelicious', 'Del.icio.us');
$Context->SetDefinition('BlogSocialBookmarksStumble', 'Stumble Upon');
$Context->SetDefinition('BlogSocialBookmarksTechnorati', 'Technorati');
$Context->SetDefinition('AddComments', 'Añadir comentarios');
$Context->SetDefinition('BlogDate', 'Blog post date format?');
$Context->SetDefinition('More', 'Pulsa para leer más...');
$Context->SetDefinition('Examples', 'Pulsa para ver ejemplos');
$Context->SetDefinition('MoreExamples', 'M&aacute; ejemplos');
$Context->SetDefinition('WordLimit', 'Set the maximum number of words per blog post to show. Set to "0" to show the entire post.');
$Context->SetDefinition('ProfileText', 'Texto del perfil');
$Context->SetDefinition('ProfileTextDesc', 'Tell us a little about yourself, and feel free to use HTML links.');
$Context->SetDefinition('NoProfileText', 'hasn\'t written a profile yet.');
$Context->SetDefinition('ViewBlogPosts', 'View blog posts...');
$Context->SetDefinition('BlogProfileText', 'Allow bloggers to have a freetext profile?');
$Context->SetDefinition('BlogStatusChanged', 'BlogThis status successfully changed.');
$Context->SetDefinition('BlogStatusUnchanged', 'BlogThis status not changed.');
$Context->SetDefinition('BlogFullName', 'Display full name?');
$Context->SetDefinition('StartBlogPost', 'Nueva entrada en blog');
$Context->SetDefinition('BlogPostCheck', 'Blog this post?');

$Context->SetDefinition('PERMISSION_CANBLOGALL', 'Puede publicar y despublicar cualquier comentario en el blog.');
$Context->SetDefinition('PERMISSION_CANBLOGTHIS', 'Puede publicar y despublicar sus propios comentarios en el blog.');
$Context->SetDefinition('PERMISSION_BLOGPROFILEHTML', 'Se le permite usar HTML en su perfil del blog.');
?>