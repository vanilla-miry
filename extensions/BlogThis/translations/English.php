<?php
//English definition files
$Context->SetDefinition('Blog', 'Blog');
$Context->SetDefinition('BlogThis', 'blog this');
$Context->SetDefinition('UnBlogThis', 'unblog this');
$Context->SetDefinition('BlogTitle', 'What is the blog page title?');
$Context->SetDefinition('BlogTabName', 'What is the text that should be displayed in the tab?');
$Context->SetDefinition('BlogExtra', 'Extra Options');
$Context->SetDefinition('BlogAdsense', 'Add Adsense section targetting to blog posts and titles?');
$Context->SetDefinition('BlogSettings', 'BlogThis Settings');
$Context->SetDefinition('BlogNotes', 'These settings are specific to the BlogThis extension. Make sure that your user has permission to Blog/UnBlog posts. Be aware that there is a global character limit that will also affect BlogThis. ');
$Context->SetDefinition('BlogPostPerPage', 'How many blog posts should be displayed per page?');
$Context->SetDefinition('BlogSocialBookmarks', 'Which social bookmarking links should be added?');
$Context->SetDefinition('BlogSocialBookmarksDigg', 'Digg it');
$Context->SetDefinition('BlogSocialBookmarksDelicious', 'Del.icio.us');
$Context->SetDefinition('BlogSocialBookmarksStumble', 'Stumble Upon');
$Context->SetDefinition('BlogSocialBookmarksTechnorati', 'Technorati');
$Context->SetDefinition('AddComments', 'Add Comment');
$Context->SetDefinition('BlogDate', 'Blog post date format?');
$Context->SetDefinition('More', 'Click to read more...');
$Context->SetDefinition('Examples', 'Click to see examples');
$Context->SetDefinition('MoreExamples', 'More examples');
$Context->SetDefinition('WordLimit', 'Set the maximum number of words per blog post to show. Set to "0" to show the entire post.');
$Context->SetDefinition('ProfileText', 'Profile Text');
$Context->SetDefinition('ProfileTextDesc', 'Tell us a little about yourself, and feel free to use HTML links.');
$Context->SetDefinition('NoProfileText', 'hasn\'t written a profile yet.');
$Context->SetDefinition('ViewBlogPosts', 'View blog posts...');
$Context->SetDefinition('BlogProfileText', 'Allow bloggers to have a freetext profile?');
$Context->SetDefinition('BlogStatusChanged', 'BlogThis status successfully changed.');
$Context->SetDefinition('BlogStatusUnchanged', 'BlogThis status not changed.');
$Context->SetDefinition('BlogFullName', 'Display full name?');
$Context->SetDefinition('StartBlogPost', 'Start a new blog post');
$Context->SetDefinition('BlogPostCheck', 'Blog this post?');

$Context->SetDefinition('PERMISSION_CANBLOGALL', 'Can blog/unblog any comment.');
$Context->SetDefinition('PERMISSION_CANBLOGTHIS', 'Can blog/unblog own comments.');
$Context->SetDefinition('PERMISSION_BLOGPROFILEHTML', 'Allow HTML in BlogThis profile?');
?>