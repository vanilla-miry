<?php

//Makes sure the BlogThis setting is correct when saving/editing a discussion
function PostBlogThis($DiscussionForm)
	{
	global $Context;
	$blogvalue = ForceIncomingBool('BlogThisCheck', 0);
	$ResultDiscussion = $DiscussionForm->DelegateParameters['ResultDiscussion'];

	//Only change if they have permission and the discussion saved correctly
	if 	(($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') || $Context->Session->User->Permission('PERMISSION_CANBLOGALL')) && (@$ResultDiscussion->DiscussionID > 0))
		{
		//Get the CommentID
		$cmid = $ResultDiscussion->Comment->CommentID;

		//Let's set the Blog status
		$sql = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
		$sql->SetMainTable('Comment','c');
		$sql->AddFieldNameValue('BlogThis', $blogvalue, 0);
		$sql->AddWhere('c', 'CommentID', '', FormatStringForDatabaseInput($cmid), '=');
		
		//stops people without permission blogging other people's posts
		if ($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') && !$Context->Session->User->Permission('PERMISSION_CANBLOGALL'))
			{
			$sql->AddWhere('c', 'AuthUserID', '', $Context->Session->UserID, '=');
			}
			
		$result = $Context->Database->Update ($sql, '', '' , 'Could not change the Blogged/UnBlogged status of the comment.');
		}
	}

//Adds Blog Tick Box to Post Page
function AddBlogTick(&$DiscussionForm)
	{
	global $Context;

	//Was it submitted?
	$BlogCheck = ForceIncomingBool('BlogThisCheck', 0);

	//find out if it's blogged or not
	$sql = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
	$sql->SetMainTable('Comment','c');
	$sql->AddSelect('BlogThis', 'c');
	$sql->AddSelect('AuthUserID', 'c');
	$sql->AddWhere('c', 'CommentID', '', $DiscussionForm->Comment->CommentID, '=');
	
	$result = $Context->Database->Select ($sql, '', '' , 'Could not select blogged status from database.');
	$result = $Context->Database->GetRow($result);

	$BlogCheck2 = $result['BlogThis'];

	//Make it checked?
	if ($BlogCheck || $BlogCheck2) {$BlogCheck = "checked";}

	echo "<div class=\"BlogThisLabel\"><label for=\"BlogThisCheck\"><input type=\"checkbox\" id=\"BlogThisCheck\" name=\"BlogThisCheck\" value=\"1\" $BlogCheck/>".$Context->GetDefinition('BlogPostCheck')."</label><br/></div>";
	}

//Adds the link to start a Blog Post
function AddStartBlog()
	{
	global $Context;
	if	($Context->Session->UserID > 0 && ($Context->Session->User->Permission('PERMISSION_CANBLOGALL') || $Context->Session->User->Permission('PERMISSION_CANBLOGTHIS')))
		{
		$CategoryID = ForceIncomingInt('CategoryID', 0);
		if	($CategoryID == 0)
			{
			$CategoryID = '';
			}
		echo '<h1 style="margin-top:10px;"><a href="'.GetUrl($Context->Configuration, 'post.php', 'category/', 'CategoryID', $CategoryID, "","BlogThisCheck=1").'">'.$Context->GetDefinition('StartBlogPost').'</a></h1>';
		}
	}

//User Profile Stuff

function GetProfileText($UserID)
	{
	global $Context;

	$s = $Context->ObjectFactory->NewContextObject($Context, 'SqlBuilder');
	$s->SetMainTable('User', 'u');
	$s->AddSelect('ProfileText', 'u');
	$s->AddWhere('u', 'UserID', '', $UserID, '=');
	$result = $Context->Database->Select($s, 'AddModifyProfile', 'GetProfileText', 'A fatal error occurred while getting the profile text.');
	$row = $Context->Database->GetRow($result);
	return $row['ProfileText'];
	}


function AddProfileText(&$UserManager)
	{
	global $Context;

	if ($Context->Session->User->Permission('PERMISSION_BLOGPROFILEHTML'))
		{
		$ProfileText = FormatStringForDatabaseInput(ForceIncomingString('ProfileText', ''));
		}
	else
		{
		$ProfileText = FormatStringForDatabaseInput(ForceIncomingString('ProfileText', ''),1);
		}

	$UserManager->DelegateParameters['User']->ProfileText = $ProfileText;
	$UserManager->DelegateParameters['SqlBuilder']->AddFieldNameValue('ProfileText', $UserManager->DelegateParameters['User']->ProfileText);
	}

function AddModifyProfile($IdentityForm)
	{
	global $Context;

	$ProfileText = GetProfileText($IdentityForm->User->UserID);

	echo '
	<ul>
	<li>
		<label for="txtProfileText">'.$Context->GetDefinition('ProfileText').'</label>
		<textarea name="ProfileText" cols="80" rows="40" id="txtProfileText" />'.$ProfileText.'</textarea>
		<p class="Description">'.$Context->GetDefinition('ProfileTextDesc').'</p>
	</li>
	</ul>';
	}

function AddProfileDetails($Account)
	{
	global $Configuration;
	global $Context;

	if ((@$Account->User->Permissions['PERMISSION_CANBLOGALL']) || (@$Account->User->Permissions['PERMISSION_CANBLOGTHIS']))
		{
		$ProfileText = GetProfileText($Account->User->UserID);

		if (!$ProfileText) {$ProfileText = $Account->User->Name." ".$Context->GetDefinition('NoProfileText');}

		echo '<h2 id="AboutProfile">About '.$Account->User->Name.'</h2>';
		echo '<div id="ProfileText">'.nl2br($ProfileText).'</div>';
		}
	}

function AddViewBlogPosts($Account)
	{
	global $Configuration;
	global $Context;

	if ((@$Account->User->Permissions['PERMISSION_CANBLOGALL']) || (@$Account->User->Permissions['PERMISSION_CANBLOGTHIS']))
		{
		echo '<p id="UserBlog"><a href="'.GetUrl($Configuration, 'extension.php', '', '', '', '',  'PostBackAction=Blog&BlogUser='.$Account->User->UserID).'">'.$Context->GetDefinition('ViewBlogPosts').'</a></p>';
		}
	}

//Modifies the comment manager SQL for BlogThis
function CMBlogThis (&$CommentManager)
	{
	global $Context;
	$CommentManager->DelegateParameters['SqlBuilder']->AddJoin('Category', 'ca', 'CategoryID', 't', 'CategoryID', 'inner join');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('FirstName', 'a', 'FirstName');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('LastName', 'a', 'LastName');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('Name', 't', 'DiscussionName');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('Closed', 't', 'Closed');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('CountComments', 't', 'CountComments');
	$CommentManager->DelegateParameters['SqlBuilder']->AddSelect('Name', 'ca', 'CategoryName');

	//Allows deleted comments to be shown, assuming permission
	if	(!$Context->Session->User->Permission('PERMISSION_VIEW_HIDDEN_COMMENTS') || !$Context->Session->User->Preference('ShowDeletedComments'))
		{
		$CommentManager->DelegateParameters['SqlBuilder']->AddWhere('m', 'Deleted', '', 0, '=', 'and', '', 1, 1);
		$CommentManager->DelegateParameters['SqlBuilder']->AddWhere('m', 'Deleted', '', 0, '=', 'or', '' ,0);
		$CommentManager->DelegateParameters['SqlBuilder']->EndWhereGroup();
		}

	//Restrict to just one user?
	if	($BlogUser = ForceIncomingString('BlogUser', ''))
		{
		$CommentManager->DelegateParameters['SqlBuilder']->AddWhere('m', 'AuthUserID', '', FormatStringForDatabaseInput($BlogUser), '=', 'and');
		}

	//Finally, just blog posts please!
	$CommentManager->DelegateParameters['SqlBuilder']->AddWhere('m', 'BlogThis', '', '1', '=');

	//And a blog order please!
	$CommentManager->DelegateParameters['SqlBuilder']->AddOrderBy('DateCreated', 'm', $SortDirection = 'desc');
	}

//Sorts out the formatting of the comment body
function PostLimit($post)
	{
	global $Configuration;
	
	if  ($Configuration['BLOG_WORD_LIMIT'])
		{
		$out = explode(" ", $post);
		
		if (count($out) <= $Configuration['BLOG_WORD_LIMIT'])
			{
			$main = $post;
			$extended = '';
			}
		else
			{
			$main="";
		
			for ($i=0; $i < $Configuration['BLOG_WORD_LIMIT'];$i++)
				{
				$main .= $out[$i] . " ";
				}
			
			//take off the last space and add "..."
			$main = trim($main) . "...";
			$extended = 1;
			}
		}
	else
		{
		$main = $post;
		$extended = '';
		}
	
    	return array('main' => $main, 'extended' => $extended);
	}

// function that adds the blogthis/unblogthis text to the comment grid
function AddBlogThis(&$CommentGrid)
	{
	global $Context;
	global $Configuration;

	$Comment = $CommentGrid->DelegateParameters['Comment'];
	$CurrentPage = ForceIncomingInt("page", 1);

	//find out if it's blogged or not
	$sql = $CommentGrid->Context->ObjectFactory->NewContextObject($CommentGrid->Context, 'SqlBuilder');
	$sql->SetMainTable('Comment','c');
	$sql->AddSelect('BlogThis', 'c');
	$sql->AddSelect('AuthUserID', 'c');
	$sql->AddWhere('c', 'CommentID', '', $Comment->CommentID, '=');
	
	$result = $CommentGrid->Context->Database->Select ($sql, '', '' , 'Could not select blogged status from database.');
	$result = $CommentGrid->Context->Database->GetRow($result);
	
	if	($result['BlogThis'])
		{
		$blogged='UnBlogThis';
		}
	else	{
		$blogged='BlogThis';
		}
		
	if (($Context->Session->User->Permission('PERMISSION_CANBLOGTHIS') && ($result['AuthUserID'] == $Context->Session->UserID)) || ($Context->Session->User->Permission('PERMISSION_CANBLOGALL')))
		{
		$CommentList = &$CommentGrid->DelegateParameters["CommentList"];
		$CommentList .= '<a href="'.GetUrl($Configuration, 'comments.php', '', 'DiscussionID', $Comment->DiscussionID, $CurrentPage, 'PostBackAction='.$blogged).'&BlogCommentID='.$Comment->CommentID.'">'. $CommentGrid->Context->GetDefinition($blogged).'</a>';
		}
	}

//Added for FeedPublisher
function Search_AddQuery(&$CommentManager)
	{
	if 	(ForceIncomingInt('BlogSearch', 0) == 1)
		{
		$CommentManager->DelegateParameters['SqlBuilder']->AddWhere('c', 'BlogThis', '', '1', '=');
		$CommentManager->DelegateParameters['SqlBuilder']->AddOrderBy('DateCreated', 'c', $SortDirection = 'desc');
		}
	}
	
function DisplaySuccess ()
	{
	global $Context;
	echo  '<div class="Notice">'.$Context->GetDefinition('BlogStatusChanged').'</div>';
	}
	
function DisplayFail ()
	{
	global $Context;
	echo  '<div class="Notice">'.$Context->GetDefinition('BlogStatusUnChanged').'</div>';
	}
?>
