<?php

class Blog extends Control
	{
	var $PageJump;
	var $CurrentPage;
	var $BlogData;
	var $BlogDataCount;
        var $RowNumber;
        var $Body;
        var $FormatType;
        var $ShowHtml;

        function Blog(&$Context)
		{
		$this->Name = 'Blog';
		$this->Control($Context);

		//reset some data	    	
		$this->BlogData = false;
	    	$this->BlogDataCount = false;
            	$this->CurrentPage = ForceIncomingInt("page", 1);

		//Modified the comment manager to be our bitch
		$Context->AddToDelegate("CommentManager", "CommentBuilder_PreWhere", "CMBlogThis");

		//Create a CommentManager object
		$cm = $this->Context->ObjectFactory->NewContextObject($this->Context, 'CommentManager');

		//Retrieve the SQLBuilder
		$s = $cm->GetCommentBuilder();

		//Now limit to the amount we need and get just those select few
		$FirstRecord = (($this->CurrentPage - 1) * $this->Context->Configuration['BLOG_PER_PAGE']);
		$s->AddLimit($FirstRecord, $this->Context->Configuration['BLOG_PER_PAGE']);

		//to get the count
		$s->Fields = "SQL_CALC_FOUND_ROWS " . $s->Fields;

		//Get the blog posts
		$this->BlogData = $this->Context->Database->Select($s, $this->Name, 'BlogComments', 'An error occurred while attempting to retrieve the requested comments.');

		//Get the count
		$this->BlogDataCount = $this->Context->Database->Execute("select found_rows();", '', '', 'An error occurred counting the blog posts.');
		$this->BlogDataCount = ($this->Context->Database->GetRow($this->BlogDataCount));
		$this->BlogDataCount = $this->BlogDataCount[0];

            	$this->CallDelegate('Constructor');
		}

	function Render()
		{
		global $Context;
		$this->CallDelegate('PreRender');
                
		//make the page list - added category 0 to make it work with friendly urls
		$pl = $this->Context->ObjectFactory->NewContextObject($this->Context, 'PageList','CategoryID','0');
	    	$pl->CssClass = 'PageList';
	    	$pl->TotalRecords = $this->BlogDataCount;
	    	$pl->CurrentPage = $this->CurrentPage;
	    	$pl->RecordsPerPage = $this->Context->Configuration['BLOG_PER_PAGE'];
	    	$pl->PagesToDisplay = 10;
	    	$pl->PageParameterName = 'page';
	    	$pl->DefineProperties();

            	$this->RowNumber = 0;

	    	$PageDetails = $pl->GetPageDetails($this->Context);
	    	$PageList = $pl->GetNumericList();

	        $PERMISSION_EDIT_DISCUSSIONS = $this->Context->Session->User->Permission('PERMISSION_EDIT_DISCUSSIONS');
            	$PERMISSION_EDIT_COMMENTS = $this->Context->Session->User->Permission('PERMISSION_EDIT_COMMENTS');
            	$CurrentUserJumpToLastCommentPref = $this->Context->Session->User->Preference('JumpToLastReadComment');
            	$ThemeFilePath = $this->Context->Configuration['EXTENSIONS_PATH'].'BlogThis/theme/Theme.Blog.php';
 
		//top of blog list
           	echo '<div class="ContentInfo Top">
                	<div class="PageInfo">
                		<p>'.($PageDetails == '' ? $this->Context->GetDefinition('NoDiscussionsFound') : $PageDetails).'</p>
                	'.$PageList.'
                	</div></div>';
            	echo '<div id="ContentBody"><ol id="BlogList">';

            	$FirstRow = 1;
            	$BlogList = '';
            	$Alternate = 0;
            	$BlogPost = $this->Context->ObjectFactory->NewContextObject($this->Context, 'Discussion');            	

		//iterate through each blog post 
		while 	(($Row = $this->Context->Database->GetRow($this->BlogData)))
				{
				$this->RowNumber++;
				
				$BlogPost->Clear();
				$BlogPost->GetPropertiesFromDataSet($Row, $this->Context->Configuration);
				$BlogPost->FormatPropertiesForDisplay();

				//add some extra bits needed
				$BlogPost->CommentID = FormatStringForDisplay($Row['CommentID']);
				$BlogPost->DiscussionName = FormatStringForDisplay($Row['DiscussionName']);
				$BlogPost->CategoryName = FormatStringForDisplay($Row['CategoryName']);

				if 	($Row['LastName'] && $Row['FirstName'] && $Context->Configuration['BLOG_FULLNAME'])
					{
					$BlogPost->DisplayName = FormatStringForDisplay($Row['FirstName'] ." ". $Row['LastName']);
					}
				else
					{
					$BlogPost->DisplayName = FormatStringForDisplay($BlogPost->AuthUsername);
					}
				
				//format body comment appropiately
				$CommentBody = PostLimit($Row['Body']);
				$CommentBody['main'] = $this->Context->FormatString($CommentBody['main'], $this, $Row['FormatType'], FORMAT_STRING_FOR_DISPLAY);

				include($ThemeFilePath);
				$FirstRow = 0;             	
				$Alternate = FlipBool($Alternate);
				}

		//bottom of blog list
		echo $BlogList.'</ol></div>';

            	if 	($this->BlogDataCount > 0)
			{
                	echo '<div class="ContentInfo Bottom">
				<div class="PageInfo">
					<p>'.$pl->GetPageDetails($this->Context).'</p>
					'.$PageList.'
				</div>
				<a id="TopOfPage" href="'.GetRequestUri().'#pgtop">'.$this->Context->GetDefinition('TopOfPage').'</a>
                	</div>';
            		}
		}

    	}
?>
