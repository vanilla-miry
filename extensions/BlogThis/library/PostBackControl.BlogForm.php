<?php

//This is for the administration controls

class BlogForm extends PostBackControl
	{
	var $ConfigurationManager;

    	function BlogForm(&$Context)
    		{
        	$this->Name = 'BlogForm';
        	$this->ValidActions = array('Blog','ProcessBlog');
		$this->Constructor($Context);

        	if 	($this->IsPostBack)
        		{
            		$SettingsFile = $this->Context->Configuration['APPLICATION_PATH'].'conf/settings.php';
            		$this->ConfigurationManager = $this->Context->ObjectFactory->NewContextObject($this->Context, 'ConfigurationManager');
            		
			if 	($this->PostBackAction == 'ProcessBlog')
            			{
                		$this->ConfigurationManager->GetSettingsFromForm($SettingsFile);
                		$this->ConfigurationManager->DefineSetting('BLOG_TAB_NAME', ForceIncomingString('BLOG_TAB_NAME', 0), 0);
				$this->ConfigurationManager->DefineSetting('BLOG_TITLE', ForceIncomingString('BLOG_TITLE', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_SOCIALBOOKMARKS_DIGG', ForceIncomingBool('BLOG_SOCIALBOOKMARKS_DIGG', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_SOCIALBOOKMARKS_DELICIOUS', ForceIncomingBool('BLOG_SOCIALBOOKMARKS_DELICIOUS', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_SOCIALBOOKMARKS_STUMBLE', ForceIncomingBool('BLOG_SOCIALBOOKMARKS_STUMBLE', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_SOCIALBOOKMARKS_TECHNORATI', ForceIncomingBool('BLOG_SOCIALBOOKMARKS_TECHNORATI', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_ADSENSE', ForceIncomingBool('BLOG_ADSENSE', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_PROFILETEXT', ForceIncomingBool('BLOG_PROFILETEXT', 0), 0);
                		$this->ConfigurationManager->DefineSetting('BLOG_FULLNAME', ForceIncomingBool('BLOG_FULLNAME', 0), 0);
				$this->DelegateParameters['ConfigurationManager'] = &$this->ConfigurationManager;

		                // And save everything
				if 	($this->ConfigurationManager->SaveSettingsToFile($SettingsFile))
					{
					header('location: '.GetUrl($this->Context->Configuration, 'settings.php', '', '', '', '', 'PostBackAction=Blog&Success=1'));
					}
				else
					{
		    			$this->PostBackAction = 'Blog';
					}
            			}
        		}
    		}

    	function Render()
    		{
		if 	($this->IsPostBack)
			{
            		$this->PostBackParams->Clear();
	    		if 	($this->PostBackAction == 'Blog')
            			{
                		$this->PostBackParams->Set('PostBackAction', 'ProcessBlog');
                		$ThemeFilePath = $this->Context->Configuration['EXTENSIONS_PATH'].'BlogThis/theme/Theme.BlogForm.php';
				include($ThemeFilePath);
            			}
        		}
    		}
	}
?>
