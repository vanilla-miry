<?php
/*
Extension Name: Google Analytics
Extension Url: http://lussumo.com/community/discussion/3507/
Description: Adds google analytics code to your vanilla forum pages.
Version: 1.3
Author: dinoboff / ithcy
Author Url: http://lussumo.com/community/discussion/3507/
*/

//replace this with your google analytics account number, of course
$Configuration['GoogleAnalyticsAccountNumber'] = 'UA-xxxxx-x';

//add an array of pages to not analyse
if ( !in_array($Context->SelfUrl, array('settings.php')) && isset($Head) )
{
	class AddGoogleAnalytics extends Control
	{
		function Render()
		{
			global $Configuration;
			echo "\n" .
				"<script type=\"text/javascript\">\n" .
				"var _gaq = _gaq || [];\n" .
				"_gaq.push(['_setAccount', '".$Configuration['GoogleAnalyticsAccountNumber']."']);\n" .
				"_gaq.push(['_trackPageview']);\n" .
				"(function() {" .
				"var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" .
				"ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" .
				"(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);" .
				"})();\n" .
				"</script>\n";
		}
	}
	$AddGoogleAnalytics = $Context->ObjectFactory->NewContextObject($Context, "AddGoogleAnalytics");
	$Page->AddControl("Page_Render", $AddGoogleAnalytics, $Configuration["CONTROL_POSITION_FOOT"]);
}
?>