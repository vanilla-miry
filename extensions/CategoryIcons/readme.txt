CategoryIcons v1.0.1			September 11, 2006
Jim Wurster
jwurster@comcast.net
www.jwurster.us

Thanks to all the Vanilla users who I have learned a lot from over these past couple of months. DraganBabic at http://herbiv.org/forum developed the css included in this extension for adding Category Icons to your categories and discussion items. This extension hopefully makes using his idea a little easier to implement.

This extension comes with a set of images for use as your Category icons. These images are from nay-k designs at http://www.drition.org/nay-k/WordPress/. If you use any of them, please give attribution to nay-k.

Put the images you want to use in the images folder.

Then edit the style.css file and change the image file names to match your files.

Then upload the entire CategoryIcons folder.


************************************
Fixes in Version v1.1 by Ryan Taylor
************************************

1, Fixed image clipping on discussions page.

2, Fixed image clipping on categories page IE Only.


===================================
EXTENSION INSTALLATION INSTRUCTIONS
===================================
In order for Vanilla to recognize an extension, it must be contained within it's own directory within the extensions directory. So, once you have downloaded and unzipped the extension files, you can then place the folder containing the default.php file into your installation of Vanilla. The path to your extension's default.php file should look like this:

/path/to/vanilla/extensions/this_extension_name/default.php

Once this is complete, you can enable the extension through the "Manage Extensions" form on the settings tab in Vanilla.

