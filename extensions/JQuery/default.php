<?php
/*
Extension Name: JQuery
Extension Url: http://vanillaforums.org/addon/231
Description: jQuery JavaScript Library version 1.4.2. Must be enabled BEFORE all other "JQ Extensions". Requires Set List for Administration Extension Options.
Version: v19-1.4.2
Author: Luke Scammell aka [-Stash-]
Author Url: http://scammell.co.uk/
*/
 if(!isset($Head)) return;

 // a little security
 if (!defined('IN_VANILLA')) exit();
 define('JQUERY_EXTENSION', true);
 $JQPPath = 'extensions/JQuery/plugins/';
 // Please don't mess with these unless you know what you're doing, otherwise you can break update version checking
 $JQext = 'v19-1.4.2';
 $JQjs = '1.4.2';

 // Set the Extension and JavaScript version numbers.
 if (!array_key_exists('JQUERY_VERSION_EXT', $Configuration) || ($Configuration['JQUERY_VERSION_EXT'] != $JQext)) {
  AddConfigurationSetting($Context, 'JQUERY_VERSION_EXT', $JQext);
 }
 if (!array_key_exists('JQUERY_VERSION_JS', $Configuration) || ($Configuration['JQUERY_VERSION_JS'] != $JQjs)) {
  AddConfigurationSetting($Context, 'JQUERY_VERSION_JS', $JQjs);
 }
 // Enable inline images to be clickable for larger versions
 if(!array_key_exists('JQUERY_FILE_TYPE', $Configuration)) {
  AddConfigurationSetting($Context, 'JQUERY_FILE_TYPE', '0');
 }
 // Plugins
 if(!array_key_exists('JQUERY_PLUGIN_COLORBOX', $Configuration)) {
  AddConfigurationSetting($Context, 'JQUERY_PLUGIN_COLORBOX', '0');}
 if(!array_key_exists('JQUERY_PLUGIN_HIDESPOILER', $Configuration)) {
  AddConfigurationSetting($Context, 'JQUERY_PLUGIN_HIDESPOILER', '0');}
 if(!array_key_exists('JQUERY_PLUGIN_SMOOTHPAGESCROLL', $Configuration)) {
  AddConfigurationSetting($Context, 'JQUERY_PLUGIN_SMOOTHPAGESCROLL', '0');}
 if(!array_key_exists('JQUERY_PLUGIN_CKEDITOR', $Configuration)) {
  AddConfigurationSetting($Context, 'JQUERY_PLUGIN_CKEDITOR', '0');}
 function includeJQuery() {
  if(defined('JQUERY_INCLUDED')) return;
  global $Head, $Configuration, $Context;
   // Default - Current minified
   if ($Context->Configuration['JQUERY_FILE_TYPE'] == '0') {$Head->AddScript('extensions/JQuery/jquery-1.4.2.min.js');}
   // Development Mode - Current uncompressed
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '999') {$Head->AddScript('extensions/JQuery/jquery-1.4.2.js');}
   // Previously Released - Uncompressed
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '998') {$Head->AddScript('extensions/JQuery/old/jquery-1.4.1.js');}
   // Old Version - 1.0 minified
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '1') {$Head->AddScript('extensions/JQuery/old/jquery-1.0.4.min.js');}
   // Old Version - 1.1 minified
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '2') {$Head->AddScript('extensions/JQuery/old/jquery-1.1.4.min.js');}
   // Old Version - 1.2 minified
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '3') {$Head->AddScript('extensions/JQuery/old/jquery-1.2.6.min.js');}
   // Old Version - 1.3 minified
   elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '4') {$Head->AddScript('extensions/JQuery/old/jquery-1.3.2.min.js');}
   // Old Version - 1.4 minified
   // elseif ($Context->Configuration['JQUERY_FILE_TYPE'] == '5') {$Head->AddScript('extensions/JQuery/old/jquery-1.4.1.min.js');}
   define('JQUERY_INCLUDED', true);
 }

// ColorBox
// if (in_array($Context->SelfUrl, array('account.php','categories.php','comments.php','extension.php','index.php','people.php','post.php','search.php','settings.php','termsofservice.php'))) {
  if ($Context->Configuration['JQUERY_PLUGIN_COLORBOX'] == '1') {
   includeJQuery(); // call JQuery to ensure it's loaded first 
   $Head->AddStylesheet($JQPPath.'colorbox/colorbox.css');
   $Head->AddScript($JQPPath.'colorbox/jquery.colorbox-min.js');
  }
// }

// HideSpoiler
	if ($Context->Configuration['JQUERY_PLUGIN_HIDESPOILER'] == '1') {
		if(in_array($Context->SelfUrl, array("post.php", "comments.php"))) {
			includeJQuery(); // call JQuery to ensure it's loaded first 
			$Head->AddStylesheet($JQPPath.'HideSpoiler/HideSpoiler.css');
			$Head->AddScript($JQPPath.'HideSpoiler/HideSpoiler.js');
			if(!defined('IN_VANILLA')) exit();
			class HideSpoiler extends StringFormatter {
			function Parse ($String, $Object, $FormatPurpose) {
				global $Configuration;
				$CommentList = &$CommentGrid->DelegateParameters["CommentList"];
					$sReturn = $String;
					if($FormatPurpose  == FORMAT_STRING_FOR_DISPLAY) { // This is what you type in the comment box
						$Patterns = array("/\[hide\](.+?)\[\/hide\]/is");
							$Replacements = array('<span class="hidden">$1</span>');
						$sReturn = preg_replace($Patterns, $Replacements, $sReturn);
					}
					return $sReturn;
				}
			}
			// Global StringFormatter
			$HideSpoiler = $Context->ObjectFactory->NewObject($Context, "HideSpoiler");
			$Context->StringManipulator->AddGlobalManipulator("HideSpoiler", $HideSpoiler);
		}
	}

// SmoothPageScroll
// if (in_array($Context->SelfUrl, array('account.php','categories.php','comments.php','extension.php','index.php','people.php','post.php','search.php','settings.php','termsofservice.php'))) {
	if ($Context->Configuration['JQUERY_PLUGIN_SMOOTHPAGESCROLL'] == '1') {
		includeJQuery(); // call JQuery to ensure it's loaded first 
		$Head->AddScript($JQPPath.'SmoothPageScroll/SmoothPageScroll.js');
	}
// }

// CKEditor
// if (in_array($Context->SelfUrl, array('account.php','categories.php','comments.php','extension.php','index.php','people.php','post.php','search.php','settings.php','termsofservice.php'))) {
	if ($Context->Configuration['JQUERY_PLUGIN_CKEDITOR'] == '1') {
		includeJQuery(); // call JQuery to ensure it's loaded first 
		$Head->AddScript($JQPPath.'ckeditor/ckeditor.js');
		$Head->AddScript($JQPPath.'ckeditor/adapters/jquery.js');
		$Head->AddScript($JQPPath.'ckeditor/ckeditor.config.js');
	}
// }
?>