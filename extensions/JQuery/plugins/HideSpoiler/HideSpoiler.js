jQuery(document).ready(function($){ // allows compatibility with prototype and other libraries
 $(function(){
  // create link and hide spoiler
  $(".Hidden,.hidden").before('<span class="HiddenLabel" title="Click to view Spoiler">[Hide/Show]</span>').addClass('HiddenHide');
  // toggle the spoilers
  $('span.HiddenLabel').toggle(
   function(){ $(this).next(".Hidden,.hidden").removeClass('HiddenHide').addClass('HiddenShown'); },
   function(){ $(this).next(".Hidden,.hidden").addClass('HiddenHide').removeClass('HiddenShown'); }
   )
 });
}); // allows compatibility with prototype and other libraries