﻿jQuery(document).ready(function($){ // allows compatibility with prototype and other libraries

	/*	2010-02-19 11:29:10 LS
	 *	Hide the native Vanilla 1 comment box resizer since it doesn't work with CKEditor 3.1
	 */
	$('#CommentBoxController').hide();

	/*	http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar */
	var config = {
		extraPlugins : 'uicolor',
		uiColor: '#eaeaea',
		toolbar:
		[
			['Source'],
			['Bold', 'Italic','Underline','Strike'],
			['Format'],
			['NumberedList', 'BulletedList'],
			['Link', 'Unlink','Anchor'],
			['Scayt','Undo','Redo'],
			'/',
			['Find','Replace','-','SelectAll','RemoveFormat'],
			['Image','MediaEmbed','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
			['Outdent','Indent','Blockquote'],
			['Maximize','About'],
			['SelectAll','RemoveFormat']
		]
	};

	// Initialize the editor.
	// Callback function can be passed and executed after full instance creation.
	$('#CommentBox').ckeditor(config);

	/*	2010-02-19 11:50:30 LS
	 *	Stop CKEditor from butchering the sourcecode with random carriage returns.
	 *	http://cksource.com/forums/viewtopic.php?f=6&t=14493&p=38352&hilit=ckeditor#p38352
	 */
	CKEDITOR.on('instanceReady', function(ev)
	{	var TagsBlock = ['br','caption','div','h1','h2','h3','h4','h5','h6','li','ol','p','ul','table','tbody','td','th','thead','tr']; // etc.

		for (var key in TagsBlock) {
		ev.editor.dataProcessor.writer.setRules(TagsBlock[key],
			{	indent : true,
				breakBeforeOpen : true,
				breakAfterOpen : false,
				breakBeforeClose : false,
				breakAfterClose : true
			});
		}

		/*	2010-02-24 17:10:34 LS
		 *	Slightly over the top since this is the same as default, but hey :D
		 */
		var TagsInline = ['a','span']; // etc.

		for (var key in TagsInline) {
		ev.editor.dataProcessor.writer.setRules(TagsInline[key],
			{	indent : false,
				breakBeforeOpen : false,
				breakAfterOpen : false,
				breakBeforeClose : false,
				breakAfterClose : false
			});
		}
	});

	/*	2010-02-19 11:51:53 LS
	 *	Enable the MediaEmbed plugin for CKEditor 3
	 *	http://www.fluidbyte.net/index.php?view=embed-youtube-vimeo-etc-into-ckeditor
	 *	http://jesox.com/posts/how-add-youtube-vimeo-or-any-other-embed-content-ckeditor
	 */
	config.extraPlugins = 'MediaEmbed';
	CKEDITOR.plugins.addExternal('mediaembed','extensions/JQuery/plugins/ckeditor/plugins/mediaembed/');
}); // allows compatibility with prototype and other libraries